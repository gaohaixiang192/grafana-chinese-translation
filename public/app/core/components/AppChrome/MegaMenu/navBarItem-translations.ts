import { t } from 'app/core/internationalization';

// Maps the ID of the nav item to a translated phrase to later pass to <Trans />
// Because the navigation content is dynamic (defined in the backend), we can not use
// the normal inline message definition method.

// see pkg/api/index.go
export function getNavTitle(navId: string | undefined) {
  // the switch cases must match the ID of the navigation item, as defined in the backend nav model
  switch (navId) {
    case 'home':
      return t('nav.home.title', '家');
    case 'new':
      return t('nav.new.title', '新');
    case 'create':
      return t('nav.create.title', '创建');
    case 'create-dashboard':
      return t('nav.create-dashboard.title', '仪表板');
    case 'folder':
      return t('nav.create-folder.title', '文件夹');
    case 'import':
      return t('nav.create-import.title', '导入仪表板');
    case 'alert':
      return t('nav.create-alert.title', '创建警报规则');
    case 'starred':
      return t('nav.starred.title', '星标');
    case 'starred-empty':
      return t('nav.starred-empty.title', '您的星形仪表板将显示在此处');
    case 'dashboards':
      return t('nav.dashboards.title', '仪表板');
    case 'dashboards/browse':
      return t('nav.dashboards.title', '仪表板');
    case 'dashboards/playlists':
      return t('nav.playlists.title', '播放列表');
    case 'dashboards/snapshots':
      return t('nav.snapshots.title', '快照');
    case 'dashboards/library-panels':
      return t('nav.library-panels.title', '面板库');
    case 'dashboards/public':
      return t('nav.public.title', '公共仪表板');
    case 'dashboards/new':
      return t('nav.new-dashboard.title', '新建仪表板');
    case 'dashboards/folder/new':
      return t('nav.new-folder.title', '新增文件夹');
    case 'dashboards/import':
      return t('nav.create-import.title', '导入仪表板');
    case 'scenes':
      return t('nav.scenes.title', '场景');
    case 'explore':
      return t('nav.explore.title', '探索');
    case 'alerting':
      return t('nav.alerting.title', '警报');
    case 'alerting-legacy':
      return t('nav.alerting-legacy.title', '警报（旧版）');
    case 'alert-home':
      return t('nav.alerting-home.title', '家');
    case 'alert-list':
      return t('nav.alerting-list.title', '警报规则');
    case 'receivers':
      return t('nav.alerting-receivers.title', '接触点');
    case 'am-routes':
      return t('nav.alerting-am-routes.title', '通知策略');
    case 'channels':
      return t('nav.alerting-channels.title', '通知渠道');
    case 'silences':
      return t('nav.alerting-silences.title', '静默');
    case 'groups':
      return t('nav.alerting-groups.title', '组');
    case 'alerting-admin':
      return t('nav.alerting-admin.title', '管理');
    case 'cfg':
      return t('nav.config.title', '管理');
    case 'datasources':
      return t('nav.datasources.title', '数据源');
    case 'correlations':
      return t('nav.correlations.title', '相关性');
    case 'users':
      return t('nav.users.title', '用户');
    case 'teams':
      return t('nav.teams.title', '团队');
    case 'plugins':
      return t('nav.plugins.title', '插件');
    case 'org-settings':
      return t('nav.org-settings.title', '默认首选项');
    case 'apikeys':
      return t('nav.api-keys.title', 'API密钥');
    case 'serviceaccounts':
      return t('nav.service-accounts.title', '服务帐户');
    case 'admin':
      return t('nav.admin.title', '服务器管理员');
    case 'support-bundles':
      return t('nav.support-bundles.title', '支持捆绑包');
    case 'global-users':
      return t('nav.global-users.title', '用户');
    case 'global-orgs':
      return t('nav.global-orgs.title', '组织机构');
    case 'server-settings':
      return t('nav.server-settings.title', '设置');
    case 'storage':
      return t('nav.storage.title', '存储');
    case 'upgrading':
      return t('nav.upgrading.title', '统计数据和许可证');
    case 'monitoring':
      return t('nav.monitoring.title', '可观测性');
    case 'apps':
      return t('nav.apps.title', '应用程序');
    case 'alerts-and-incidents':
      return t('nav.alerts-and-incidents.title', '警报和IRM');
    case 'help':
      return t('nav.help.title', '帮助');
    case 'profile/settings':
      return t('nav.profile/settings.title', '轮廓');
    case 'profile/notifications':
      return t('nav.profile/notifications.title', '通知历史记录');
    case 'profile/password':
      return t('nav.profile/password.title', '更改密码');
    case 'sign-out':
      return t('nav.sign-out.title', '注销');
    case 'search':
      return t('nav.search-dashboards.title', '搜索仪表板');
    default:
      return undefined;
  }
}

export function getNavSubTitle(navId: string | undefined) {
  switch (navId) {
    case 'dashboards':
      return t('nav.dashboards.subtitle', '创建和管理仪表板以可视化您的数据');
    case 'dashboards/browse':
      return t('nav.dashboards.subtitle', '创建和管理仪表板以可视化您的数据');
    case 'manage-folder':
      return t('nav.manage-folder.subtitle', '管理文件夹面板和权限');
    case 'dashboards/playlists':
      return t('nav.playlists.subtitle', '按顺序显示的仪表板组');
    case 'dashboards/snapshots':
      return t(
        'nav.snapshots.subtitle',
        'Interactive, publically available, 仪表板的时间点表示'
      );
    case 'dashboards/library-panels':
      return t('nav.library-panels.subtitle', '可添加到多个仪表板的可重复使用的面板');
    case 'alerting':
      return t('nav.alerting.subtitle', '在系统出现问题后立即了解这些问题');
    case 'alert-list':
      return t('nav.alerting-list.subtitle', '确定警报是否会触发的规则');
    case 'receivers':
      return t(
        'nav.alerting-receivers.subtitle',
        '选择警报实例触发时通知联系人的方式'
      );
    case 'am-routes':
      return t('nav.alerting-am-routes.subtitle', '确定如何将警报发送到联络点');
    case 'silences':
      return t('nav.alerting-silences.subtitle', '停止来自一个或多个警报规则的通知');
    case 'groups':
      return t('nav.alerting-groups.subtitle', '查看来自Alertmanager实例的分组警报');
    case 'datasources':
      return t('nav.datasources.subtitle', '添加和配置数据源');
    case 'correlations':
      return t('nav.correlations.subtitle', '添加和配置相关性');
    case 'users':
      return t('nav.users.subtitle', '邀请用户并为其分配角色');
    case 'teams':
      return t('nav.teams.subtitle', '具有通用仪表板和权限需求的用户组');
    case 'plugins':
      return t('nav.plugins.subtitle', '使用插件扩展Grafana体验');
    case 'org-settings':
      return t('nav.org-settings.subtitle', '管理整个组织的首选项');
    case 'apikeys':
      return t('nav.api-keys.subtitle', '管理和创建用于与Grafana HTTP API交互的API密钥');
    case 'serviceaccounts':
      return t('nav.service-accounts.subtitle', '使用服务帐户在Grafana中运行自动化工作负载');
    case 'global-users':
      return t('nav.global-users.subtitle', '管理Grafana中的用户');
    case 'global-orgs':
      return t('nav.global-orgs.subtitle', 'Grafana在同一服务器上运行的独立实例');
    case 'server-settings':
      return t('nav.server-settings.subtitle', '查看Grafana配置中定义的设置');
    case 'storage':
      return t('nav.storage.subtitle', '管理文件存储');
    case 'support-bundles':
      return t('nav.support-bundles.subtitle', '下载支持捆绑包');
    case 'admin':
      return t(
        'nav.admin.subtitle',
        '管理服务器范围的设置和对资源（如组织、用户和许可证）的访问'
      );
    case 'apps':
      return t('nav.apps.subtitle', '扩展Grafana体验的应用程序插件');
    case 'monitoring':
      return t('nav.monitoring.subtitle', '监控和基础设施应用程序');
    case 'alerts-and-incidents':
      return t('nav.alerts-and-incidents.subtitle', '警报和事件管理应用程序');
    default:
      return undefined;
  }
}
