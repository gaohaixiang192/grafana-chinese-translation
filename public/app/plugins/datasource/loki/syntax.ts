import { Grammar } from 'prismjs';

import { CompletionItem } from '@grafana/ui';

export const AGGREGATION_OPERATORS: CompletionItem[] = [
  {
    label: 'avg',
    insertText: 'avg',
    documentation: '计算尺寸的平均值',
  },
  {
    label: 'bottomk',
    insertText: 'bottomk',
    documentation: '按样本值排列的最小k个元素',
  },
  {
    label: 'count',
    insertText: 'count',
    documentation: '计算矢量中的元素数',
  },
  {
    label: 'max',
    insertText: 'max',
    documentation: '选择尺寸上的最大值',
  },
  {
    label: 'min',
    insertText: 'min',
    documentation: '在尺寸上选择最小值',
  },
  {
    label: 'stddev',
    insertText: 'stddev',
    documentation: '计算尺寸上的总体标准偏差',
  },
  {
    label: 'stdvar',
    insertText: 'stdvar',
    documentation: '计算维度上的总体标准方差',
  },
  {
    label: 'sum',
    insertText: 'sum',
    documentation: '计算尺寸总和',
  },
  {
    label: 'topk',
    insertText: 'topk',
    documentation: '按样本值排列的最大k个元素',
  },
];

export const PIPE_PARSERS: CompletionItem[] = [
  {
    label: 'json',
    insertText: 'json',
    documentation: '使用json解析器从日志行提取标签.',
  },
  {
    label: 'regexp',
    insertText: 'regexp ""',
    documentation: '使用regexp解析器从日志行提取标签.',
    move: -1,
  },
  {
    label: 'logfmt',
    insertText: 'logfmt',
    documentation: '使用logfmt解析器从日志行提取标签.',
  },
  {
    label: 'pattern',
    insertText: 'pattern',
    documentation: '使用模式解析器从日志行提取标签。仅在Loki 2.3中提供+.',
  },
  {
    label: 'unpack',
    insertText: 'unpack',
    detail: 'unpack identifier',
    documentation:
      '解析JSON日志行，在打包阶段拆包所有嵌入的标签。还将使用一个特殊属性“_entry”来替换原始日志行。仅在Loki 2.2中提供+.',
  },
];

export const PIPE_OPERATORS: CompletionItem[] = [
  {
    label: 'unwrap',
    insertText: 'unwrap',
    detail: 'unwrap identifier',
    documentation: '获取标签并将这些值用作度量聚合的示例数据.',
  },
  {
    label: 'label_format',
    insertText: 'label_format',
    documentation: '用于重命名、修改或添加标签。例如，|label_format foo=bar .',
  },
  {
    label: 'line_format',
    insertText: 'line_format',
    documentation: '重写日志行内容。例如, | line_format "{{.query}} {{.duration}}" .',
  },
];

export const RANGE_VEC_FUNCTIONS = [
  {
    insertText: 'avg_over_time',
    label: 'avg_over_time',
    detail: 'avg_over_time(range-vector)',
    documentation: '指定间隔内所有值的平均值.',
  },
  {
    insertText: 'bytes_over_time',
    label: 'bytes_over_time',
    detail: 'bytes_over_time(range-vector)',
    documentation: '统计给定范围内每个日志流使用的字节数',
  },
  {
    insertText: 'bytes_rate',
    label: 'bytes_rate',
    detail: 'bytes_rate(range-vector)',
    documentation: '计算每个流每秒的字节数.',
  },
  {
    insertText: 'first_over_time',
    label: 'first_over_time',
    detail: 'first_over_time(range-vector)',
    documentation: '指定间隔中所有值中的第一个值。仅在Loki 2.3中提供+.',
  },
  {
    insertText: 'last_over_time',
    label: 'last_over_time',
    detail: 'last_over_time(range-vector)',
    documentation: '指定间隔中所有值的最后一个。仅在Loki 2.3中提供+.',
  },
  {
    insertText: 'sum_over_time',
    label: 'sum_over_time',
    detail: 'sum_over_time(range-vector)',
    documentation: '指定间隔内所有值的总和.',
  },
  {
    insertText: 'count_over_time',
    label: 'count_over_time',
    detail: 'count_over_time(range-vector)',
    documentation: '指定间隔内所有值的计数.',
  },
  {
    insertText: 'max_over_time',
    label: 'max_over_time',
    detail: 'max_over_time(range-vector)',
    documentation: '指定间隔内所有值的最大值.',
  },
  {
    insertText: 'min_over_time',
    label: 'min_over_time',
    detail: 'min_over_time(range-vector)',
    documentation: '指定间隔内所有值的最小值.',
  },
  {
    insertText: 'quantile_over_time',
    label: 'quantile_over_time',
    detail: 'quantile_over_time(scalar, range-vector)',
    documentation: 'The φ-quantile (0 ≤ φ ≤ 1) 指定间隔中的值的.',
  },
  {
    insertText: 'rate',
    label: 'rate',
    detail: 'rate(v range-vector)',
    documentation: '计算每秒的条目数.',
  },
  {
    insertText: 'stddev_over_time',
    label: 'stddev_over_time',
    detail: 'stddev_over_time(range-vector)',
    documentation: '指定区间内数值的总体标准偏差.',
  },
  {
    insertText: 'stdvar_over_time',
    label: 'stdvar_over_time',
    detail: 'stdvar_over_time(range-vector)',
    documentation: '指定区间内数值的总体标准方差.',
  },
];

export const BUILT_IN_FUNCTIONS = [
  {
    insertText: 'vector',
    label: 'vector',
    detail: 'vector(scalar)',
    documentation: '以不带标签的矢量形式返回标量.',
  },
];

export const FUNCTIONS = [...AGGREGATION_OPERATORS, ...RANGE_VEC_FUNCTIONS, ...BUILT_IN_FUNCTIONS];
export const LOKI_KEYWORDS = [...FUNCTIONS, ...PIPE_OPERATORS, ...PIPE_PARSERS].map((keyword) => keyword.label);

export const lokiGrammar: Grammar = {
  comment: {
    pattern: /#.*/,
  },
  'context-aggregation': {
    pattern: /((without|by)\s*)\([^)]*\)/, // by ()
    lookbehind: true,
    inside: {
      'label-key': {
        pattern: /[^(),\s][^,)]*[^),\s]*/,
        alias: 'attr-name',
      },
      punctuation: /[()]/,
    },
  },
  'context-labels': {
    pattern: /\{[^}]*(?=}?)/,
    greedy: true,
    inside: {
      comment: {
        pattern: /#.*/,
      },
      'label-key': {
        pattern: /[a-zA-Z_]\w*(?=\s*(=|!=|=~|!~))/,
        alias: 'attr-name',
        greedy: true,
      },
      'label-value': {
        pattern: /"(?:\\.|[^\\"])*"/,
        greedy: true,
        alias: 'attr-value',
      },
      punctuation: /[{]/,
    },
  },
  'context-pipe': {
    pattern: /\s\|[^=~]\s?\w*/i,
    inside: {
      'pipe-operator': {
        pattern: /\|/i,
        alias: 'operator',
      },
      'pipe-operations': {
        pattern: new RegExp(`${[...PIPE_PARSERS, ...PIPE_OPERATORS].map((f) => f.label).join('|')}`, 'i'),
        alias: 'keyword',
      },
    },
  },
  function: new RegExp(`\\b(?:${FUNCTIONS.map((f) => f.label).join('|')})(?=\\s*\\()`, 'i'),
  'context-range': [
    {
      pattern: /\[[^\]]*(?=\])/, // [1m]
      inside: {
        'range-duration': {
          pattern: /\b\d+[smhdwy]\b/i,
          alias: 'number',
        },
      },
    },
    {
      pattern: /(offset\s+)\w+/, // offset 1m
      lookbehind: true,
      inside: {
        'range-duration': {
          pattern: /\b\d+[smhdwy]\b/i,
          alias: 'number',
        },
      },
    },
  ],
  quote: {
    pattern: /"(?:\\.|[^\\"])*"/,
    alias: 'string',
    greedy: true,
  },
  backticks: {
    pattern: /`(?:\\.|[^\\`])*`/,
    alias: 'string',
    greedy: true,
  },
  number: /\b-?\d+((\.\d*)?([eE][+-]?\d+)?)?\b/,
  operator: /\s?(\|[=~]?|!=?|<(?:=>?|<|>)?|>[>=]?)\s?/i,
  punctuation: /[{}(),.]/,
};

export default lokiGrammar;
