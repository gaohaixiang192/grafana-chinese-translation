import { DataFrame, QueryHint } from '@grafana/data';

import {
  isQueryWithLabelFilter,
  isQueryPipelineErrorFiltering,
  isQueryWithLabelFormat,
  isQueryWithParser,
  isQueryWithLineFilter,
} from './queryUtils';
import {
  dataFrameHasLevelLabel,
  extractHasErrorLabelFromDataFrame,
  extractLevelLikeLabelFromDataFrame,
  extractLogParserFromDataFrame,
} from './responseUtils';

export function getQueryHints(query: string, series: DataFrame[]): QueryHint[] {
  if (series.length === 0) {
    return [];
  }

  const hints: QueryHint[] = [];
  const { queryWithParser, parserCount } = isQueryWithParser(query);

  if (!queryWithParser) {
    const { hasLogfmt, hasJSON, hasPack } = extractLogParserFromDataFrame(series[0]);
    if (hasJSON) {
      if (hasPack) {
        hints.push({
          type: 'ADD_UNPACK_PARSER',
          label: '所选日志流选择器已打包日志.',
          fix: {
            title: '添加解包解析器',
            label: '考虑使用解包解析器.',
            action: {
              type: 'ADD_UNPACK_PARSER',
              query,
            },
          },
        });
      } else {
        hints.push({
          type: 'ADD_JSON_PARSER',
          label: '所选日志流选择器具有JSON格式的日志.',
          fix: {
            title: '添加json解析器',
            label: '考虑使用JSON解析器.',
            action: {
              type: 'ADD_JSON_PARSER',
              query,
            },
          },
        });
      }
    }

    if (hasLogfmt) {
      hints.push({
        type: 'ADD_LOGFMT_PARSER',
        label: '所选日志流选择器具有logfmt格式的日志.',
        fix: {
          title: '添加logfmt解析器',
          label: '考虑使用logfmt解析器将日志行中的键值对转换为标签.',
          action: {
            type: 'ADD_LOGFMT_PARSER',
            query,
          },
        },
      });
    }
  }

  if (queryWithParser) {
    // To keep this simple, we consider pipeline error filtering hint only is query has up to 1 parser
    if (parserCount === 1) {
      const hasPipelineErrorFiltering = isQueryPipelineErrorFiltering(query);
      const hasError = extractHasErrorLabelFromDataFrame(series[0]);
      if (hasError && !hasPipelineErrorFiltering) {
        hints.push({
          type: 'ADD_NO_PIPELINE_ERROR',
          label: '所选日志流中的某些日志存在解析错误.',
          fix: {
            title: '删除管道错误',
            label: '考虑筛选出包含解析错误的日志.',
            action: {
              type: 'ADD_NO_PIPELINE_ERROR',
              query,
            },
          },
        });
      }
    }

    const hasLabelFilter = isQueryWithLabelFilter(query);

    if (!hasLabelFilter) {
      hints.push({
        type: 'ADD_LABEL_FILTER',
        label: '考虑按日志的标签和值筛选日志.',
        fix: {
          title: '添加标签过滤器',
          label: '',
          action: {
            type: 'ADD_LABEL_FILTER',
            query,
          },
        },
      });
    }
  }

  const queryWithLabelFormat = isQueryWithLabelFormat(query);
  if (!queryWithLabelFormat) {
    const hasLevel = dataFrameHasLevelLabel(series[0]);
    const levelLikeLabel = extractLevelLikeLabelFromDataFrame(series[0]);

    // Add hint only if we don't have "level" label and have level-like label
    if (!hasLevel && levelLikeLabel) {
      hints.push({
        type: 'ADD_LEVEL_LABEL_FORMAT',
        label: `所选日志流中的某些日志具有 "${levelLikeLabel}" label.`,
        fix: {
          title: '添加标签级格式',
          label: `If ${levelLikeLabel} 标签具有级别值，请考虑使用label_format将其重命名为“级别”。然后可以在日志卷中显示级别标签.`,
          action: {
            type: 'ADD_LEVEL_LABEL_FORMAT',
            query,
            options: {
              renameTo: 'level',
              originalLabel: levelLikeLabel,
            },
          },
        },
      });
    }
  }

  const hasLineFilter = isQueryWithLineFilter(query);

  if (!hasLineFilter) {
    hints.push({
      type: 'ADD_LINE_FILTER',
      label: '考虑筛选特定字符串的日志.',
      fix: {
        title: 'add line filter',
        label: '',
        action: {
          type: 'ADD_LINE_FILTER',
          query,
        },
      },
    });
  }

  return hints;
}
