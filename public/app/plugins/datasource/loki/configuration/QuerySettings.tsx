import React from 'react';

import { config } from '@grafana/runtime';
import { Badge, LegacyForms } from '@grafana/ui';

const { FormField } = LegacyForms;

type Props = {
  maxLines: string;
  onMaxLinedChange: (value: string) => void;
  predefinedOperations: string;
  onPredefinedOperationsChange: (value: string) => void;
};

export const QuerySettings = (props: Props) => {
  const { maxLines, onMaxLinedChange, predefinedOperations, onPredefinedOperationsChange } = props;
  return (
    <>
      <h3 className="page-heading">Queries</h3>
      <div className="gf-form-group">
        <div className="gf-form-inline">
          <div className="gf-form">
            <FormField
              label="Maximum lines"
              labelWidth={11}
              inputWidth={20}
              inputEl={
                <input
                  type="number"
                  className="gf-form-input width-8 gf-form-input--has-help-icon"
                  value={maxLines}
                  onChange={(event) => onMaxLinedChange(event.currentTarget.value)}
                  spellCheck={false}
                  placeholder="1000"
                />
              }
              tooltip={
                <>
                  Loki查询必须包含返回的最大行数限制（默认值：1000）。增长
                  这一限制使特设分析的结果集更大。如果您的浏览器
                  显示日志结果时变得迟缓.
                </>
              }
            />
          </div>
        </div>
        {config.featureToggles.lokiPredefinedOperations && (
          <div className="gf-form-inline">
            <div className="gf-form">
              <FormField
                label="Predefined operations"
                labelWidth={11}
                inputEl={
                  <input
                    type="string"
                    className="gf-form-input width-20 gf-form-input--has-help-icon"
                    value={predefinedOperations}
                    onChange={(event) => onPredefinedOperationsChange(event.currentTarget.value)}
                    spellCheck={false}
                    placeholder="| unpack | line_format"
                  />
                }
                tooltip={
                  <div>
                    {
                      '预定义的操作用作查询的初始状态。如果您想解包、解析或格式化所有日志行，它们非常有用。目前，我们只支持以|开头的日志操作。例如: | unpack | line_format "{{.message}}".'
                    }
                  </div>
                }
              />
              <Badge
                text="Experimental"
                color="orange"
                icon="exclamation-triangle"
                tooltip="预定义操作是一种实验性功能，将来可能会发生变化."
              />
            </div>
          </div>
        )}
      </div>
    </>
  );
};
