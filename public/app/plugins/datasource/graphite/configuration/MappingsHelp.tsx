import React from 'react';

import { Alert } from '@grafana/ui';

type Props = {
  onDismiss: () => void;
};

export default function MappingsHelp(props: Props): JSX.Element {
  return (
    <Alert severity="info" title="如何将Graphite指标映射到标签?" onRemove={props.onDismiss}>
      <p>当前仅支持Graphite和Loki查询之间的映射.</p>
      <p>
        当您将数据源从Graphite切换到Loki时，您的查询将根据映射进行映射
        在下面的示例中定义。要定义映射，请编写度量的完整路径并替换所需的节点
        映射到带有括号中标签名称的标签。标签的值是从Graphite查询中提取的
        切换数据源时.
      </p>
      <p>
        无论映射配置如何，所有标签都会自动映射到标签。石墨匹配图案
        （使用&#123；&#125；）转换为Loki&apos；s正则表达式匹配模式。当您使用函数时
        在查询中，将提取度量和标记，以便将它们与定义的映射相匹配.
      </p>
      <p>
        Example: 用于映射 = <code>servers.(cluster).(server).*</code>:
      </p>
      <table>
        <thead>
          <tr>
            <th>石墨查询</th>
            <th>映射到Loki查询</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <code>
                alias(servers.<u>west</u>.<u>001</u>.cpu,1,2)
              </code>
            </td>
            <td>
              <code>
                &#123;cluster=&quot;<u>west</u>&quot;, server=&quot;<u>001</u>&quot;&#125;
              </code>
            </td>
          </tr>
          <tr>
            <td>
              <code>
                alias(servers.*.<u>&#123;001,002&#125;</u>.*,1,2)
              </code>
            </td>
            <td>
              <code>
                &#123;server=~&quot;<u>(001|002)</u>&quot;&#125;
              </code>
            </td>
          </tr>
          <tr>
            <td>
              <code>interpolate(seriesByTag(&apos;foo=bar&apos;, &apos;server=002&apos;), inf))</code>
            </td>
            <td>
              <code>&#123;foo=&quot;bar&quot;, server=&quot;002&quot;&#125;</code>
            </td>
          </tr>
        </tbody>
      </table>
    </Alert>
  );
}
