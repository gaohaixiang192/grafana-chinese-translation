export function invalidNamespaceError() {
  return {
    status: 404,
    statusText: 'Not Found',
    data: {
      error: {
        code: 'InvalidResourceNamespace',
        message: "资源命名空间“grafanadev”无效.",
      },
    },
    config: {
      url: 'api/datasources/proxy/31/azuremonitor/subscriptions/44693801-6ee6-49de-9b2d-9106972f9572/resourceGroups/grafanadev/providers/grafanadev/select/providers/microsoft.insights/metricdefinitions?api-version=2018-01-01&metricnamespace=select',
      method: 'GET',
      retry: 0,
      headers: {
        'X-Grafana-Org-Id': 1,
      },
      hideFromInspector: false,
    },
  };
}

export function invalidSubscriptionError() {
  return {
    status: 400,
    data: {
      error: {
        code: 'InvalidSubscriptionId',
        message: "提供的订阅标识符“abc”格式错误或无效.",
      },
    },
  };
}
