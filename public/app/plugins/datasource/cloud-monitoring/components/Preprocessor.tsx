import React, { useMemo } from 'react';

import { SelectableValue } from '@grafana/data';
import { EditorField } from '@grafana/experimental';
import { RadioButtonGroup } from '@grafana/ui';

import { getAlignmentPickerData } from '../functions';
import { PreprocessorType, TimeSeriesList, MetricKind, ValueTypes } from '../types/query';
import { MetricDescriptor } from '../types/types';

const NONE_OPTION = { label: 'None', value: PreprocessorType.None };

export interface Props {
  metricDescriptor?: MetricDescriptor;
  onChange: (query: TimeSeriesList) => void;
  query: TimeSeriesList;
}

export const Preprocessor = ({ query, metricDescriptor, onChange }: Props) => {
  const options = useOptions(metricDescriptor);

  return (
    <EditorField
      label="Pre-processing"
      tooltip="当所选度量具有增量或累积度量时，将显示预处理选项。可用的特定选项由度量的值类型决定。如果选择“速率”，数据点将对齐并转换为每个时间序列的速率。如果选择“增量”，则数据点按每个时间序列的增量（差）对齐"
    >
      <RadioButtonGroup
        onChange={(value: PreprocessorType) => {
          const { perSeriesAligner: psa } = query;
          const { valueType, metricKind } = metricDescriptor ?? {};
          const { perSeriesAligner } = getAlignmentPickerData(valueType, metricKind, psa, value);
          onChange({ ...query, preprocessor: value, perSeriesAligner });
        }}
        value={query.preprocessor ?? PreprocessorType.None}
        options={options}
      />
    </EditorField>
  );
};

const useOptions = (metricDescriptor?: MetricDescriptor): Array<SelectableValue<PreprocessorType>> => {
  const metricKind = metricDescriptor?.metricKind;
  const valueType = metricDescriptor?.valueType;

  return useMemo(() => {
    if (!metricKind || metricKind === MetricKind.GAUGE || valueType === ValueTypes.DISTRIBUTION) {
      return [NONE_OPTION];
    }

    const options = [
      NONE_OPTION,
      {
        label: 'Rate',
        value: PreprocessorType.Rate,
        description: '数据点对齐并转换为每个时间序列的速率',
      },
    ];

    return metricKind === MetricKind.CUMULATIVE
      ? [
          ...options,
          {
            label: 'Delta',
            value: PreprocessorType.Delta,
            description: '数据点按时间序列的增量（差）排列',
          },
        ]
      : options;
  }, [metricKind, valueType]);
};
