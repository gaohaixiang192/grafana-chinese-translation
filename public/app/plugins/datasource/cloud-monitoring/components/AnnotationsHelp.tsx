import React from 'react';

export const AnnotationsHelp = () => {
  return (
    <div className="gf-form grafana-info-box alert-info">
      <div>
        <h5>注释查询格式</h5>
        <p>
          注释是覆盖在图形顶部的事件。注释呈现非常昂贵，因此限制返回的行数非常重要.{' '}
        </p>
        <p>
          “标题”和“文本”字段支持模板化，并且可以使用查询返回的数据。例如，“标题”字段可以包含以下文本:
        </p>
        <code>
          {`${'{{metric.type}}'}`} 具有值: {`${'{{metric.value}}'}`}
        </code>
        <p>
          示例结果: <code>monitoring.googleapis.com/uptime_check/http_status 具有此值: 502</code>
        </p>
        <span>模式:</span>
        <p>
          <code>{`${'{{metric.value}}'}`}</code> = 度量/点的值
        </p>
        <p>
          <code>{`${'{{metric.type}}'}`}</code> = 公制类型，例如 compute.googleapis.com/instance/cpu/usage_time
        </p>
        <p>
          <code>{`${'{{metric.name}}'}`}</code> = 度量的名称部分，例如 instance/cpu/usage_time
        </p>
        <p>
          <code>{`${'{{metric.service}}'}`}</code> = 度量的服务部分，例如计算
        </p>
        <p>
          <code>{`${'{{metric.label.label_name}}'}`}</code> = 度量标签元数据，例如 metric.label.instance_name
        </p>
        <p>
          <code>{`${'{{resource.label.label_name}}'}`}</code> = 资源标签元数据，例如 resource.label.zone
        </p>
      </div>
    </div>
  );
};
