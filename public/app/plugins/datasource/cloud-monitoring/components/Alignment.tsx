import React, { useMemo } from 'react';

import { SelectableValue } from '@grafana/data';
import { EditorField, EditorFieldGroup } from '@grafana/experimental';

import { ALIGNMENT_PERIODS } from '../constants';
import CloudMonitoringDatasource from '../datasource';
import { alignmentPeriodLabel } from '../functions';
import { PreprocessorType, TimeSeriesList } from '../types/query';
import { CustomMetaData, MetricDescriptor } from '../types/types';

import { AlignmentFunction } from './AlignmentFunction';
import { PeriodSelect } from './PeriodSelect';

export interface Props {
  refId: string;
  onChange: (query: TimeSeriesList) => void;
  query: TimeSeriesList;
  templateVariableOptions: Array<SelectableValue<string>>;
  customMetaData: CustomMetaData;
  datasource: CloudMonitoringDatasource;
  metricDescriptor?: MetricDescriptor;
  preprocessor?: PreprocessorType;
}

export const Alignment = ({
  refId,
  templateVariableOptions,
  onChange,
  query,
  customMetaData,
  datasource,
  metricDescriptor,
  preprocessor,
}: Props) => {
  const alignmentLabel = useMemo(() => alignmentPeriodLabel(customMetaData, datasource), [customMetaData, datasource]);
  return (
    <EditorFieldGroup>
      <EditorField
        label="对准功能"
        tooltip="对齐过程包括收集在固定时间长度内接收到的所有数据点，应用函数组合这些数据点，并为结果分配时间戳."
      >
        <AlignmentFunction
          inputId={`${refId}-alignment-function`}
          templateVariableOptions={templateVariableOptions}
          query={query}
          onChange={(q) => onChange({ ...query, ...q })}
          metricDescriptor={metricDescriptor}
          preprocessor={preprocessor}
        />
      </EditorField>
      <EditorField label="Alignment period" tooltip={alignmentLabel}>
        <PeriodSelect
          inputId={`${refId}-alignment-period`}
          templateVariableOptions={templateVariableOptions}
          current={query.alignmentPeriod}
          onChange={(period) => onChange({ ...query, alignmentPeriod: period })}
          aligmentPeriods={ALIGNMENT_PERIODS}
        />
      </EditorField>
    </EditorFieldGroup>
  );
};
