import { css } from '@emotion/css';
import React, { PureComponent } from 'react';

import { QueryEditorHelpProps } from '@grafana/data';

import { CloudMonitoringQuery } from '../types/query';

export default class CloudMonitoringCheatSheet extends PureComponent<
  QueryEditorHelpProps<CloudMonitoringQuery>,
  { userExamples: string[] }
> {
  render() {
    return (
      <div>
        <h2>云监控别名模式</h2>
        <div>
          <p>
            使用别名模式以任何方式设置图例键的格式。使用别名模式以任意方式设置图例键的格式.
          </p>
          例子:
          <code>{`${'{{metric.name}} - {{metric.label.instance_name}}'}`}</code>
          <br />
          结果: &nbsp;&nbsp;<code>cpu/usage_time - server1-europe-west-1</code>
          <br />
          <br />
          <span>模式:</span>
          <br />
          <ul
            className={css`
              list-style: none;
            `}
          >
            <li>
              <code>{`${'{{metric.type}}'}`}</code> = 公制类型，例如 compute.googleapis.com/instance/cpu/usage_time
            </li>
            <li>
              <code>{`${'{{metric.name}}'}`}</code> = 度量的名称部分，例如 instance/cpu/usage_time
            </li>
            <li>
              <code>{`${'{{metric.service}}'}`}</code> = 度量的服务部分，例如计算
            </li>
            <li>
              <code>{`${'{{metric.label.label_name}}'}`}</code> = 度量标签元数据，例如 metric.label.instance_name
            </li>
            <li>
              <code>{`${'{{resource.label.label_name}}'}`}</code> = 资源标签元数据，例如 resource.label.zone
            </li>
            <li>
              <code>{`${'{{metadata.system_labels.name}}'}`}</code> = 元数据系统标签，例如
              metadata.system_labels.name. 为此，需要将
            </li>
            <li>
              <code>{`${'{{metadata.user_labels.name}}'}`}</code> = 元数据用户标签，例如
              metadata.user_labels.name. 为此，需要将
            </li>
            <li>
              <code>{`${'{{bucket}}'}`}</code> = 在中使用热图时分布度量的桶边界Grafana
            </li>
            <li>
              <code>{`${'{{project}}'}`}</code> = 在查询编辑器中指定的项目名称
            </li>
            <li>
              <code>{`${'{{service}}'}`}</code> = 在SLO查询编辑器中指定的服务id
            </li>
            <li>
              <code>{`${'{{slo}}'}`}</code> = 在SLO查询编辑器中指定的SLO id
            </li>
            <li>
              <code>{`${'{{selector}}'}`}</code> = 在SLO查询编辑器中指定的Selector函数
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
