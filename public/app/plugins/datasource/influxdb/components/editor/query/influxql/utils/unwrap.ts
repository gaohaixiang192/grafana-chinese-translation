export function unwrap<T>(value: T | null | undefined): T {
  if (value == null) {
    throw new Error('值不能为零');
  }
  return value;
}
