import React, { useState } from 'react';

import { QueryEditorProps } from '@grafana/data/src';
import { InlineFormLabel, Input } from '@grafana/ui/src';

import InfluxDatasource from '../../../datasource';
import { InfluxOptions, InfluxQuery } from '../../../types';

export const AnnotationEditor = (props: QueryEditorProps<InfluxDatasource, InfluxQuery, InfluxOptions>) => {
  const { query, onChange } = props;
  const [eventQuery, setEventQuery] = useState<string>(query.query ?? '');

  const [textColumn, setTextColumn] = useState<string>(query.textColumn ?? '');
  const [tagsColumn, setTagsColumn] = useState<string>(query.tagsColumn ?? '');
  const [timeEndColumn, setTimeEndColumn] = useState<string>(query?.timeEndColumn ?? '');
  const [titleColumn] = useState<string>(query?.titleColumn ?? '');
  const updateValue = <K extends keyof InfluxQuery, V extends InfluxQuery[K]>(key: K, val: V) => {
    onChange({
      ...query,
      [key]: val,
      fromAnnotations: true,
      textEditor: true,
    });
  };
  return (
    <div className="gf-form-group">
      <div className="gf-form">
        <InlineFormLabel width={12}>InfluxQL Query</InlineFormLabel>
        <Input
          value={eventQuery}
          onChange={(e) => setEventQuery(e.currentTarget.value ?? '')}
          onBlur={() => updateValue('query', eventQuery)}
          placeholder="从$timeFilter限制为1000的事件中选择文本"
        />
      </div>
      <InlineFormLabel
        width={12}
        tooltip={
          <div>
            如果您的influxdb查询返回多个字段，则需要在下面指定列名。注释
            事件由一个标题、标记和一个附加的文本字段组成。也可以映射timeEnd列
            用于区域注释.
          </div>
        }
      >
        字段映射
      </InlineFormLabel>
      <div className="gf-form-group">
        <div className="gf-form-inline">
          <div className="gf-form">
            <InlineFormLabel width={12}>文本</InlineFormLabel>
            <Input
              value={textColumn}
              onChange={(e) => setTextColumn(e.currentTarget.value ?? '')}
              onBlur={() => updateValue('textColumn', textColumn)}
            />
          </div>
          <div className="gf-form">
            <InlineFormLabel width={12}>标签</InlineFormLabel>
            <Input
              value={tagsColumn}
              onChange={(e) => setTagsColumn(e.currentTarget.value ?? '')}
              onBlur={() => updateValue('tagsColumn', tagsColumn)}
            />
          </div>
          <div className="gf-form">
            <InlineFormLabel width={12}>时间结束</InlineFormLabel>
            <Input
              value={timeEndColumn}
              onChange={(e) => setTimeEndColumn(e.currentTarget.value ?? '')}
              onBlur={() => updateValue('timeEndColumn', timeEndColumn)}
            />
          </div>
          <div className="gf-form ng-hide">
            <InlineFormLabel width={12}>标题</InlineFormLabel>
            <Input defaultValue={titleColumn} />
          </div>
        </div>
      </div>
    </div>
  );
};
