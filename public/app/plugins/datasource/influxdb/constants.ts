export const BROWSER_MODE_DISABLED_MESSAGE =
  'InfluxDB数据源中的直接浏览器访问不再可用。切换到服务器访问模式.';
