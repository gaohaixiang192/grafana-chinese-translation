import React, { ChangeEvent } from 'react';

import { InlineField, InlineFieldRow, Input } from '@grafana/ui';

import { EditorProps } from '../QueryEditor';
import { PulseWaveQuery } from '../dataquery.gen';

const fields: Array<{
  label: string;
  id: keyof PulseWaveQuery;
  placeholder: string;
  tooltip: string;
}> = [
  { label: 'Step', id: 'timeStep', placeholder: '60', tooltip: '数据点之间的秒数.' },
  {
    label: 'On Count',
    id: 'onCount',
    placeholder: '3',
    tooltip: '循环开始时，循环内应具有onValue的值的数目.',
  },
  { label: 'Off Count', id: 'offCount', placeholder: '6', tooltip: '循环内offValues的数量.' },
  {
    label: 'On Value',
    id: 'onValue',
    placeholder: '1',
    tooltip: '“on values”的值可以是int、float或null.',
  },
  {
    label: 'Off Value',
    id: 'offValue',
    placeholder: '1',
    tooltip: '“off-values”的值可以是int、float或null.',
  },
];

export const PredictablePulseEditor = ({ onChange, query }: EditorProps) => {
  // Convert values to numbers before saving
  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;

    onChange({ target: { name, value: Number(value) } });
  };

  return (
    <InlineFieldRow>
      {fields.map(({ label, id, placeholder, tooltip }) => {
        return (
          <InlineField label={label} labelWidth={14} key={id} tooltip={tooltip}>
            <Input
              width={32}
              type="number"
              name={id}
              id={`pulseWave.${id}-${query.refId}`}
              value={query.pulseWave?.[id]}
              placeholder={placeholder}
              onChange={onInputChange}
            />
          </InlineField>
        );
      })}
    </InlineFieldRow>
  );
};
