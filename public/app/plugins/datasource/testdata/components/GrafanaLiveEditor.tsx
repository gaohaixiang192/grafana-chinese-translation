import React from 'react';

import { SelectableValue } from '@grafana/data';
import { InlineField, InlineFieldRow, Select } from '@grafana/ui';

import { EditorProps } from '../QueryEditor';

const liveTestDataChannels = [
  {
    label: 'random-2s-stream',
    value: 'random-2s-stream',
    description: '随机流，每2s点',
  },
  {
    label: 'random-flakey-stream',
    value: 'random-flakey-stream',
    description: '以随机间隔返回数据的流',
  },
  {
    label: 'random-labeled-stream',
    value: 'random-labeled-stream',
    description: '带移动标签的值',
  },
  {
    label: 'random-20Hz-stream',
    value: 'random-20Hz-stream',
    description: '20Hz内点的随机流',
  },
];

export const GrafanaLiveEditor = ({ onChange, query }: EditorProps) => {
  const onChannelChange = ({ value }: SelectableValue<string>) => {
    onChange({ ...query, channel: value });
  };

  return (
    <InlineFieldRow>
      <InlineField label="Channel" labelWidth={14}>
        <Select
          width={32}
          onChange={onChannelChange}
          placeholder="Select channel"
          options={liveTestDataChannels}
          value={liveTestDataChannels.find((f) => f.value === query.channel)}
        />
      </InlineField>
    </InlineFieldRow>
  );
};
