import { TestDataQueryType } from '../dataquery.gen';

export const scenarios = [
  {
    description: '',
    id: TestDataQueryType.Annotations,
    name: 'Annotations',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.Arrow,
    name: 'Load Apache Arrow Data',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.CSVMetricValues,
    name: 'CSV Metric Values',
    stringInput: '1,20,90,30,5,0',
  },
  {
    description: '',
    id: TestDataQueryType.DataPointsOutsideRange,
    name: 'Datapoints Outside Range',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.ExponentialHeatmapBucketData,
    name: 'Exponential heatmap bucket data',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.GrafanaAPI,
    name: 'Grafana API',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.LinearHeatmapBucketData,
    name: 'Linear heatmap bucket data',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.Logs,
    name: 'Logs',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.ManualEntry,
    name: 'Manual Entry',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.NoDataPoints,
    name: 'No Data Points',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.PredictableCSVWave,
    name: 'Predictable CSV Wave',
    stringInput: '',
  },
  {
    description:
      '可预测脉冲每次返回一个数据点StepSeconds的脉冲波。\n' +
      '\n' +
      '波形以时间StepSeconds*（onCount+offCount）循环。\n' +
      '\n' +
      '波浪的周期是基于绝对时间（从纪元开始），这使得它是可预测的。\n' +
      '\n' +
      '时间戳将在timeStepSeconds上均匀排列（例如，60秒意味着时间将全部以：00秒结束）.',
    id: TestDataQueryType.PredictablePulse,
    name: 'Predictable Pulse',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.RandomWalk,
    name: 'Random Walk',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.RandomWalkTable,
    name: 'Random Walk Table',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.RandomWalkWithError,
    name: 'Random Walk (with error)',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.ServerError500,
    name: 'Server Error (500)',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.SlowQuery,
    name: 'Slow Query',
    stringInput: '5s',
  },
  {
    description: '',
    id: TestDataQueryType.StreamingClient,
    name: 'Streaming Client',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.TableStatic,
    name: 'Table Static',
    stringInput: '',
  },
  {
    description: '',
    id: TestDataQueryType.FlameGraph,
    name: 'Flame Graph',
    stringInput: '',
  },
];
