import React from 'react';

import { AnnotationQuery } from '@grafana/data';
import { EditorField, EditorRow, EditorRows, EditorSwitch, Space } from '@grafana/experimental';
import { AutoSizeInput, Input } from '@grafana/ui';

import { PromQueryCodeEditor } from '../querybuilder/components/PromQueryCodeEditor';
import { PromQuery } from '../types';

import { PromQueryEditorProps } from './types';

type Props = PromQueryEditorProps & {
  annotation?: AnnotationQuery<PromQuery>;
  onAnnotationChange?: (annotation: AnnotationQuery<PromQuery>) => void;
};

export function AnnotationQueryEditor(props: Props) {
  // This is because of problematic typing. See AnnotationQueryEditorProps in grafana-data/annotations.ts.
  const annotation = props.annotation!;
  const onAnnotationChange = props.onAnnotationChange!;
  const query = { expr: annotation.expr, refId: annotation.name, interval: annotation.step };

  return (
    <>
      <EditorRows>
        <PromQueryCodeEditor
          {...props}
          query={query}
          showExplain={false}
          onChange={(query) => {
            onAnnotationChange({
              ...annotation,
              expr: query.expr,
            });
          }}
        />
        <EditorRow>
          <EditorField
            label="Min step"
            tooltip={
              <>
                Prometheus查询的step参数和{' '}
                <code>$__interval</code> and <code>$__rate_interval</code> variables.
              </>
            }
          >
            <AutoSizeInput
              type="text"
              aria-label="设置阶跃参数的下限"
              placeholder={'auto'}
              minWidth={10}
              onCommitChange={(ev) => {
                onAnnotationChange({
                  ...annotation,
                  step: ev.currentTarget.value,
                });
              }}
              defaultValue={query.interval}
            />
          </EditorField>
        </EditorRow>
      </EditorRows>
      <Space v={0.5} />
      <EditorRow>
        <EditorField
          label="Title"
          tooltip={
            '请使用名称或模式。例如， {{instance}} 被替换为标签实例的标签值.'
          }
        >
          <Input
            type="text"
            placeholder="{{alertname}}"
            value={annotation.titleFormat}
            onChange={(event) => {
              onAnnotationChange({
                ...annotation,
                titleFormat: event.currentTarget.value,
              });
            }}
          />
        </EditorField>
        <EditorField label="Tags">
          <Input
            type="text"
            placeholder="label1,label2"
            value={annotation.tagKeys}
            onChange={(event) => {
              onAnnotationChange({
                ...annotation,
                tagKeys: event.currentTarget.value,
              });
            }}
          />
        </EditorField>
        <EditorField
          label="Text"
          tooltip={
            '请使用名称或模式。例如， {{instance}} 被替换为标签实例的标签值.'
          }
        >
          <Input
            type="text"
            placeholder="{{instance}}"
            value={annotation.textFormat}
            onChange={(event) => {
              onAnnotationChange({
                ...annotation,
                textFormat: event.currentTarget.value,
              });
            }}
          />
        </EditorField>
        <EditorField
          label="Series value as timestamp"
          tooltip={
            '时间戳的单位是毫秒。如果序列值的单位是秒，则将其范围向量乘以1000.'
          }
        >
          <EditorSwitch
            value={annotation.useValueForTime}
            onChange={(event) => {
              onAnnotationChange({
                ...annotation,
                useValueForTime: event.currentTarget.value,
              });
            }}
          />
        </EditorField>
      </EditorRow>
    </>
  );
}
