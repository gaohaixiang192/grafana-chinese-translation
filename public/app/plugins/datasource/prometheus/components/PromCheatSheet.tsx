import React from 'react';

import { QueryEditorHelpProps } from '@grafana/data';

import { PromQuery } from '../types';

const CHEAT_SHEET_ITEMS = [
  {
    title: 'Request Rate',
    expression: 'rate(http_request_total[5m])',
    label:
      '给定HTTP请求计数器，此查询计算过去5分钟内每秒的平均请求速率.',
  },
  {
    title: '请求延迟的第95个百分比',
    expression: 'histogram_quantile(0.95, sum(rate(prometheus_http_request_duration_seconds_bucket[5m])) by (le))',
    label: '计算5分钟窗口内HTTP请求率的95%.',
  },
  {
    title: '警报启动',
    expression: 'sort_desc(sum(sum_over_time(ALERTS{alertstate="firing"}[24h])) by (alertname))',
    label: '总结过去24小时内发出的警报.',
  },
  {
    title: '步骤',
    label:
      '使用持续时间格式（15s，1m，3h，…）定义图形分辨率。小步骤创建高分辨率图形，但在较大的时间范围内可能较慢。使用较长的步长会降低分辨率，并通过生成较少的数据点来平滑图形。如果没有给出步长，则自动计算分辨率.',
  },
];

const PromCheatSheet = (props: QueryEditorHelpProps<PromQuery>) => (
  <div>
    <h2>PromQL Cheat Sheet</h2>
    {CHEAT_SHEET_ITEMS.map((item, index) => (
      <div className="cheat-sheet-item" key={index}>
        <div className="cheat-sheet-item__title">{item.title}</div>
        {item.expression ? (
          <button
            type="button"
            className="cheat-sheet-item__example"
            onClick={(e) => props.onClickExample({ refId: 'A', expr: item.expression })}
          >
            <code>{item.expression}</code>
          </button>
        ) : null}
        <div className="cheat-sheet-item__label">{item.label}</div>
      </div>
    ))}
  </div>
);

export default PromCheatSheet;
