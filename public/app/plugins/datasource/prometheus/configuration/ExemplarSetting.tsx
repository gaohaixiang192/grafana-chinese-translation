import React, { useState } from 'react';

import { selectors } from '@grafana/e2e-selectors';
import { DataSourcePicker } from '@grafana/runtime';
import { Button, InlineField, Input, Switch, useTheme2 } from '@grafana/ui';

import { ExemplarTraceIdDestination } from '../types';

import { docsTip, overhaulStyles, PROM_CONFIG_LABEL_WIDTH } from './ConfigEditor';

type Props = {
  value: ExemplarTraceIdDestination;
  onChange: (value: ExemplarTraceIdDestination) => void;
  onDelete: () => void;
  disabled?: boolean;
};

export default function ExemplarSetting({ value, onChange, onDelete, disabled }: Props) {
  const [isInternalLink, setIsInternalLink] = useState(Boolean(value.datasourceUid));

  const theme = useTheme2();
  const styles = overhaulStyles(theme);

  return (
    <div className="gf-form-group">
      <InlineField
        label="Internal link"
        labelWidth={PROM_CONFIG_LABEL_WIDTH}
        disabled={disabled}
        tooltip={
          <>
            如果您有内部链接，请启用此选项。启用后，将显示数据源选择器。为示例数据选择后端跟踪数据存储. {docsTip()}
          </>
        }
        interactive={true}
        className={styles.switchField}
      >
        <>
          <Switch
            value={isInternalLink}
            aria-label={selectors.components.DataSource.Prometheus.configPage.internalLinkSwitch}
            onChange={(ev) => setIsInternalLink(ev.currentTarget.checked)}
          />
        </>
      </InlineField>

      {isInternalLink ? (
        <InlineField
          label="Data source"
          labelWidth={PROM_CONFIG_LABEL_WIDTH}
          tooltip={<>示例要导航到的数据源. {docsTip()}</>}
          disabled={disabled}
          interactive={true}
        >
          <DataSourcePicker
            tracing={true}
            current={value.datasourceUid}
            noDefault={true}
            width={40}
            onChange={(ds) =>
              onChange({
                ...value,
                datasourceUid: ds.uid,
                url: undefined,
              })
            }
          />
        </InlineField>
      ) : (
        <InlineField
          label="URL"
          labelWidth={PROM_CONFIG_LABEL_WIDTH}
          tooltip={<>用户要查看其跟踪的跟踪后端的URL. {docsTip()}</>}
          disabled={disabled}
          interactive={true}
        >
          <Input
            placeholder="https://example.com/${__value.raw}"
            spellCheck={false}
            width={40}
            value={value.url}
            onChange={(event) =>
              onChange({
                ...value,
                datasourceUid: undefined,
                url: event.currentTarget.value,
              })
            }
          />
        </InlineField>
      )}

      <InlineField
        label="URL Label"
        labelWidth={PROM_CONFIG_LABEL_WIDTH}
        tooltip={<>用于覆盖示例traceID字段上的按钮标签. {docsTip()}</>}
        disabled={disabled}
        interactive={true}
      >
        <Input
          placeholder="Go to example.com"
          spellCheck={false}
          width={40}
          value={value.urlDisplayLabel}
          onChange={(event) =>
            onChange({
              ...value,
              urlDisplayLabel: event.currentTarget.value,
            })
          }
        />
      </InlineField>
      <InlineField
        label="Label name"
        labelWidth={PROM_CONFIG_LABEL_WIDTH}
        tooltip={<>标签对象中应用于获取traceID的字段的名称. {docsTip()}</>}
        disabled={disabled}
        interactive={true}
      >
        <Input
          placeholder="traceID"
          spellCheck={false}
          width={40}
          value={value.name}
          onChange={(event) =>
            onChange({
              ...value,
              name: event.currentTarget.value,
            })
          }
        />
      </InlineField>
      {!disabled && (
        <InlineField label="Remove exemplar link" labelWidth={PROM_CONFIG_LABEL_WIDTH} disabled={disabled}>
          <Button
            variant="destructive"
            title="Remove exemplar link"
            icon="times"
            onClick={(event) => {
              event.preventDefault();
              onDelete();
            }}
          />
        </InlineField>
      )}
    </div>
  );
}
