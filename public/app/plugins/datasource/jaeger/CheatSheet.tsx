import { css } from '@emotion/css';
import React from 'react';

import { GrafanaTheme2 } from '@grafana/data';
import { useStyles2 } from '@grafana/ui';

export default function CheatSheet() {
  const styles = useStyles2(getStyles);
  return (
    <>
      <h2 id="jaeger-cheat-sheet">Jaeger Cheat Sheet</h2>
      <p>
        此备忘单提供了可用查询类型的快速概述。有关Jaeger数据源的更多详细信息，请查看{' '}
        <a
          href="https://grafana.com/docs/grafana/latest/datasources/jaeger"
          target="_blank"
          rel="noreferrer"
          className={styles.anchorTag}
        >
          文件
        </a>
        .
      </p>

      <hr />
      <ul className={styles.unorderedList}>
        <li>
          搜索-按服务名称筛选跟踪。另外，您可以通过标记或最小/最大持续时间进行筛选，并限制返回的跟踪数.
        </li>
        <li>TraceID-如果您有跟踪ID，只需输入跟踪ID即可查看跟踪.</li>
        <li>
          JSON文件-您可以上传一个包含单个跟踪的JSON文件来可视化它
          跟踪，则第一个跟踪用于可视化。可以在中找到有效JSON文件的示例{' '}
          <a
            href="https://grafana.com/docs/grafana/latest/datasources/jaeger/#upload-json-trace-file"
            target="_blank"
            rel="noreferrer"
            className={styles.anchorTag}
          >
            本节
          </a>{' '}
          文件的.
        </li>
      </ul>
    </>
  );
}

const getStyles = (theme: GrafanaTheme2) => ({
  anchorTag: css`
    color: ${theme.colors.text.link};
  `,
  unorderedList: css`
    list-style-type: none;
  `,
});
