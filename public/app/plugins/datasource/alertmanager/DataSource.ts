import { lastValueFrom, Observable, of } from 'rxjs';

import { DataQuery, DataQueryResponse, DataSourceApi, DataSourceInstanceSettings } from '@grafana/data';
import { BackendSrvRequest, getBackendSrv, isFetchError } from '@grafana/runtime';

import { discoverAlertmanagerFeaturesByUrl } from '../../../features/alerting/unified/api/buildInfo';
import { messageFromError } from '../../../features/alerting/unified/utils/redux';
import { AlertmanagerApiFeatures } from '../../../types/unified-alerting-dto';

import { AlertManagerDataSourceJsonData, AlertManagerImplementation } from './types';

export type AlertManagerQuery = {
  query: string;
} & DataQuery;

export class AlertManagerDatasource extends DataSourceApi<AlertManagerQuery, AlertManagerDataSourceJsonData> {
  constructor(public instanceSettings: DataSourceInstanceSettings<AlertManagerDataSourceJsonData>) {
    super(instanceSettings);
  }

  // `query()` has to be implemented but we actually don't use it, just need this
  // data source to proxy requests.
  // @ts-ignore
  query(): Observable<DataQueryResponse> {
    return of({
      data: [],
    });
  }

  _request(url: string) {
    const options: BackendSrvRequest = {
      headers: {},
      method: 'GET',
      url: this.instanceSettings.url + url,
    };

    if (this.instanceSettings.basicAuth || this.instanceSettings.withCredentials) {
      this.instanceSettings.withCredentials = true;
    }

    if (this.instanceSettings.basicAuth) {
      options.headers!.Authorization = this.instanceSettings.basicAuth;
    }

    return lastValueFrom(getBackendSrv().fetch<any>(options));
  }

  async testDatasource() {
    let alertmanagerResponse;
    const amUrl = this.instanceSettings.url;

    const amFeatures: AlertmanagerApiFeatures = amUrl
      ? await discoverAlertmanagerFeaturesByUrl(amUrl)
      : { lazyConfigInit: false };

    if (this.instanceSettings.jsonData.implementation === AlertManagerImplementation.prometheus) {
      try {
        alertmanagerResponse = await this._request('/alertmanager/api/v2/status');
        if (alertmanagerResponse && alertmanagerResponse?.status === 200) {
          return {
            status: 'error',
            message:
              '看起来您选择了Prometheus实现，但检测到了Mimir或Cortex端点。请更新实施选择，然后重试.',
          };
        }
      } catch (e) {}
      try {
        alertmanagerResponse = await this._request('/api/v2/status');
      } catch (e) {}
    } else {
      try {
        alertmanagerResponse = await this._request('/api/v2/status');
        if (alertmanagerResponse && alertmanagerResponse?.status === 200) {
          return {
            status: 'error',
            message:
              '看起来您选择了Mimir或Cortex实现，但检测到了Prometheus端点。请更新实施选择，然后重试.',
          };
        }
      } catch (e) {}
      try {
        alertmanagerResponse = await this._request('/alertmanager/api/v2/status');
      } catch (e) {
        if (
          isFetchError(e) &&
          amFeatures.lazyConfigInit &&
          messageFromError(e)?.includes('未配置Alertmanager')
        ) {
          return {
            status: 'success',
            message: '健康检查通过.',
            details: { message: '已发现没有回退配置的Mimir Alertmanager.' },
          };
        }
      }
    }

    return alertmanagerResponse?.status === 200
      ? {
          status: 'success',
          message: '健康检查通过.',
        }
      : {
          status: 'error',
          message: '健康检查失败.',
        };
  }
}
