import produce from 'immer';
import React from 'react';
import { Link } from 'react-router-dom';

import { SIGV4ConnectionConfig } from '@grafana/aws-sdk';
import { DataSourcePluginOptionsEditorProps, SelectableValue } from '@grafana/data';
import { DataSourceHttpSettings, InlineField, InlineFormLabel, InlineSwitch, Select } from '@grafana/ui';
import { Span } from '@grafana/ui/src/unstable';
import { config } from 'app/core/config';

import { AlertManagerDataSourceJsonData, AlertManagerImplementation } from './types';

export type Props = DataSourcePluginOptionsEditorProps<AlertManagerDataSourceJsonData>;

const IMPL_OPTIONS: Array<SelectableValue<AlertManagerImplementation>> = [
  {
    value: AlertManagerImplementation.mimir,
    label: 'Mimir',
    description: `https://grafana.com/oss/mimir/. Prometheus的开源、水平可扩展、高可用、多租户的长期存储.`,
  },
  {
    value: AlertManagerImplementation.cortex,
    label: 'Cortex',
    description: `https://cortexmetrics.io/`,
  },
  {
    value: AlertManagerImplementation.prometheus,
    label: 'Prometheus',
    description:
      'https://prometheus.io/.不支持通过API编辑配置，因此联系人和通知策略是只读的.',
  },
];

export const ConfigEditor = (props: Props) => {
  const { options, onOptionsChange } = props;

  return (
    <>
      <h3 className="page-heading">Alertmanager</h3>
      <div className="gf-form-group">
        <div className="gf-form-inline">
          <div className="gf-form">
            <InlineFormLabel width={13}>Implementation</InlineFormLabel>
            <Select
              width={40}
              options={IMPL_OPTIONS}
              value={options.jsonData.implementation || AlertManagerImplementation.mimir}
              onChange={(value) =>
                onOptionsChange({
                  ...options,
                  jsonData: {
                    ...options.jsonData,
                    implementation: value.value,
                  },
                })
              }
            />
          </div>
        </div>
        <div className="gf-form-inline">
          <InlineField
            label="接收Grafana警报"
            tooltip="启用后，Grafana管理的警报将发送到此警报管理器."
            labelWidth={26}
          >
            <InlineSwitch
              value={options.jsonData.handleGrafanaManagedAlerts ?? false}
              onChange={(e) => {
                onOptionsChange(
                  produce(options, (draft) => {
                    draft.jsonData.handleGrafanaManagedAlerts = e.currentTarget.checked;
                  })
                );
              }}
            />
          </InlineField>
        </div>
        {options.jsonData.handleGrafanaManagedAlerts && (
          <Span variant="bodySmall" color="secondary">
            Make sure to enable the alert forwarding on the <Link to="/alerting/admin">admin page</Link>.
          </Span>
        )}
      </div>
      <DataSourceHttpSettings
        defaultUrl={''}
        dataSourceConfig={options}
        showAccessOptions={true}
        onChange={onOptionsChange}
        sigV4AuthToggleEnabled={config.sigV4AuthEnabled}
        renderSigV4Editor={<SIGV4ConnectionConfig {...props}></SIGV4ConnectionConfig>}
        secureSocksDSProxyEnabled={false} // the proxy is not implemented to work with the alertmanager
      />
    </>
  );
};
