import { DataFrame, DataFrameJSON, dataFrameToJSON } from '@grafana/data';
import appEvents from 'app/core/app_events';
import { GRAFANA_DATASOURCE_NAME } from 'app/features/alerting/unified/utils/datasource';
import { PanelModel } from 'app/features/dashboard/state';
import { ShowConfirmModalEvent } from 'app/types/events';

import { GrafanaQuery, GrafanaQueryType } from './types';

/**
 * Will show a confirm modal if the current panel does not have a snapshot query.
 * If the confirm modal is shown, and the user aborts the promise will resolve with a false value,
 * otherwise it will resolve with a true value.
 */
export function onUpdatePanelSnapshotData(panel: PanelModel, frames: DataFrame[]): Promise<boolean> {
  return new Promise<boolean>((resolve) => {
    if (panel.datasource?.uid === GRAFANA_DATASOURCE_NAME) {
      updateSnapshotData(frames, panel);
      resolve(true);
      return;
    }

    appEvents.publish(
      new ShowConfirmModalEvent({
        title: '更改为面板嵌入数据',
        text: '如果您想更改此面板中显示的数据，Grafana需要删除面板当前查询，并用当前数据的快照替换它。这使您能够编辑数据.',
        yesText: 'Continue',
        icon: 'pen',
        onConfirm: () => {
          updateSnapshotData(frames, panel);
          resolve(true);
        },
        onDismiss: () => {
          resolve(false);
        },
      })
    );
  });
}

function updateSnapshotData(frames: DataFrame[], panel: PanelModel) {
  const snapshot: DataFrameJSON[] = frames.map((f) => dataFrameToJSON(f));

  const query: GrafanaQuery = {
    refId: 'A',
    queryType: GrafanaQueryType.Snapshot,
    snapshot,
    datasource: { uid: GRAFANA_DATASOURCE_NAME },
  };

  panel.updateQueries({
    dataSource: { uid: GRAFANA_DATASOURCE_NAME },
    queries: [query],
  });

  panel.refresh();
}
