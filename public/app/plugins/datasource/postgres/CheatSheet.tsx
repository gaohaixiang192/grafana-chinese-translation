import { css } from '@emotion/css';
import React from 'react';

import { GrafanaTheme2 } from '@grafana/data';
import { useStyles2 } from '@grafana/ui';

export function CheatSheet() {
  const styles = useStyles2(getStyles);

  return (
    <div>
      <h2>PostgreSQL备忘单</h2>
      时间序列:
      <ul className={styles.ulPadding}>
        <li>
          已命名的返回列 <i>time</i> （UTC以秒或时间戳表示）
        </li>
        <li>返回以数字数据类型为值的列</li>
      </ul>
      随意的:
      <ul className={styles.ulPadding}>
        <li>
          已命名的返回列 <i>metric</i> 表示系列名称.
        </li>
        <li>如果返回多个值列，则使用度量列作为前缀.</li>
        <li>如果未找到列命名度量，则值列的列名将用作序列名</li>
      </ul>
      <p>时间序列查询的结果集需要按时间排序.</p>
      表:
      <ul className={styles.ulPadding}>
        <li>返回任意一组列</li>
      </ul>
      宏:
      <ul className={styles.ulPadding}>
        <li>$__time(column) -&gt; column as &quot;time&quot;</li>
        <li>$__timeEpoch -&gt; extract(epoch from column) as &quot;time&quot;</li>
        <li>
          $__timeFilter(column) -&gt; column BETWEEN &apos;2017-04-21T05:01:17Z&apos; AND
          &apos;2017-04-21T05:01:17Z&apos;
        </li>
        <li>$__unixEpochFilter(column) -&gt; column &gt;= 1492750877 AND column &lt;= 1492750877</li>
        <li>
          $__unixEpochNanoFilter(column) -&gt; column &gt;= 1494410783152415214 AND column &lt;= 1494497183142514872
        </li>
        <li>
          $__timeGroup(column,&apos;5m&apos;[, fillvalue]) -&gt; (extract(epoch from column)/300)::bigint*300 通过设置
          fillwalue grafana将根据时间间隔填充缺失的值fillvalue可以是文字
          值，NULL或以前的值；previor将填充以前看到的值，如果还没有看到，则为NULL
        </li>
        <li>
          $__timeGroupAlias(column,&apos;5m&apos;) -&gt; (extract(epoch from column)/300)::bigint*300 AS
          &quot;time&quot;
        </li>
        <li>$__unixEpochGroup(column,&apos;5m&apos;) -&gt; floor(column/300)*300</li>
        <li>$__unixEpochGroupAlias(column,&apos;5m&apos;) -&gt; floor(column/300)*300 AS &quot;time&quot;</li>
      </ul>
      <p>Example of group by and order by with $__timeGroup:</p>
      <pre>
        <code>
          SELECT $__timeGroup(date_time_col, &apos;1h&apos;), sum(value) as value <br />
          FROM yourtable
          <br />
          GROUP BY time
          <br />
          ORDER BY time
          <br />
        </code>
      </pre>
      或者使用这些宏构建自己的条件，这些宏只返回值:
      <ul className={styles.ulPadding}>
        <li>$__timeFrom() -&gt; &apos;2017-04-21T05:01:17Z&apos;</li>
        <li>$__timeTo() -&gt; &apos;2017-04-21T05:01:17Z&apos;</li>
        <li>$__unixEpochFrom() -&gt; 1492750877</li>
        <li>$__unixEpochTo() -&gt; 1492750877</li>
        <li>$__unixEpochNanoFrom() -&gt; 1494410783152415214</li>
        <li>$__unixEpochNanoTo() -&gt; 1494497183142514872</li>
      </ul>
    </div>
  );
}

function getStyles(theme: GrafanaTheme2) {
  return {
    ulPadding: css({
      margin: theme.spacing(1, 0),
      paddingLeft: theme.spacing(5),
    }),
  };
}
