import { css, cx } from '@emotion/css';
import { stripIndent, stripIndents } from 'common-tags';
import Prism from 'prismjs';
import React, { useState } from 'react';

import { Collapse } from '@grafana/ui';
import { flattenTokens } from '@grafana/ui/src/slate-plugins/slate-prism';

import tokenizer from '../language/cloudwatch-logs/syntax';
import { CloudWatchQuery } from '../types';

interface QueryExample {
  category: string;
  examples: Array<{
    title?: string;
    description?: string;
    expr: string;
  }>;
}

const QUERIES: QueryExample[] = [
  {
    category: 'Lambda',
    examples: [
      {
        title: '查看5分钟间隔的延迟统计信息',
        expr: stripIndents`filter @type = "REPORT" |
                           stats avg(@duration), max(@duration), min(@duration) by bin(5m)`,
      },
      {
        title: '确定预留过多的内存量',
        expr: stripIndent`filter @type = "REPORT"
        | stats max(@memorySize / 1000 / 1000) as provisionedMemoryMB,
          min(@maxMemoryUsed / 1000 / 1000) as smallestMemoryRequestMB,
          avg(@maxMemoryUsed / 1000 / 1000) as avgMemoryUsedMB,
          max(@maxMemoryUsed / 1000 / 1000) as maxMemoryUsedMB,
          provisionedMemoryMB - maxMemoryUsedMB as overProvisionedMB
        `,
      },
      {
        title: '查找最昂贵的请求',
        expr: stripIndents`filter @type = "REPORT"
        | fields @requestId, @billedDuration
        | sort by @billedDuration desc`,
      },
    ],
  },

  {
    category: 'VPC Flow Logs',
    examples: [
      {
        title: '按源和目标IP地址划分的平均、最小和最大字节传输',
        expr: `stats avg(bytes), min(bytes), max(bytes) by srcAddr, dstAddr`,
      },
      {
        title: '使用UDP传输协议的IP地址',
        expr: 'filter protocol=17 | stats count(*) by srcAddr',
      },
      {
        title: '按源和目标IP地址排列的前10个字节传输',
        expr: stripIndents`stats sum(bytes) as bytesTransferred by srcAddr, dstAddr |
                           sort bytesTransferred desc |
                           limit 10`,
      },
      {
        title: '拒绝请求数最多的前20个源IP地址',
        expr: stripIndents`filter action="REJECT" |
                           stats count(*) as numRejections by srcAddr |
                           sort numRejections desc |
                           limit 20`,
      },
      {
        title: '查找主机间的前15个数据包传输',
        expr: stripIndents`stats sum(packets) as packetsTransferred by srcAddr, dstAddr
        | sort packetsTransferred  desc
        | limit 15`,
      },
      {
        title: '查找在捕获窗口期间跳过流记录的IP地址',
        expr: stripIndents`filter logStatus="SKIPDATA"
        | stats count(*) by bin(1h) as t
        | sort t`,
      },
    ],
  },
  {
    category: 'CloudTrail',
    examples: [
      {
        title: '按服务、事件类型和地区列出的日志条目数',
        expr: 'stats count(*) by eventSource, eventName, awsRegion',
      },

      {
        title: '按区域和EC2事件类型列出的日志条目数',
        expr: stripIndents`filter eventSource="ec2.amazonaws.com" |
                           stats count(*) as eventCount by eventName, awsRegion |
                           sort eventCount desc`,
      },

      {
        title: '新建IAM用户的地区、用户名和ARN',
        expr: stripIndents`filter eventName="CreateUser" |
                           fields awsRegion, requestParameters.userName, responseElements.user.arn`,
      },
      {
        title: '查找在给定AWS区域中启动或停止的EC2主机',
        expr: stripIndents`filter (eventName="StartInstances" or eventName="StopInstances") and region="us-east-2"`,
      },
      {
        title: '查找调用UpdateTrail API时发生异常的记录数',
        expr: stripIndents`filter eventName="UpdateTrail" and ispresent(errorCode) | stats count(*) by errorCode, errorMessage`,
      },
      {
        title: '查找使用TLS 1.0或1.1的日志项',
        expr: stripIndents`filter tlsDetails.tlsVersion in [ "TLSv1", "TLSv1.1" ]
        | stats count(*) as numOutdatedTlsCalls by userIdentity.accountId, recipientAccountId, eventSource, eventName, awsRegion, tlsDetails.tlsVersion, tlsDetails.cipherSuite, userAgent
        | sort eventSource, eventName, awsRegion, tlsDetails.tlsVersion`,
      },
      {
        title: '查找使用TLS版本1.0或1.1的每个服务的调用数',
        expr: stripIndents`filter tlsDetails.tlsVersion in [ "TLSv1", "TLSv1.1" ]
        | stats count(*) as numOutdatedTlsCalls by eventSource
        | sort numOutdatedTlsCalls desc`,
      },
    ],
  },
  {
    category: 'Common Queries',
    examples: [
      {
        title: '25个最近添加的日志事件',
        expr: stripIndents`fields @timestamp, @message |
                           sort @timestamp desc |
                           limit 25`,
      },
      {
        title: '每5分钟记录的异常数',
        expr: stripIndents`filter @message like /Exception/ |
                           stats count(*) as exceptionCount by bin(5m) |
                           sort exceptionCount desc`,
      },
      {
        title: '非异常的日志事件列表',
        expr: 'fields @message | filter @message not like /Exception/',
      },
      {
        title: '解析和计数字段',
        expr: stripIndents`fields @timestamp, @message
        | filter @message like /User ID/
        | parse @message "User ID: *" as @userId
        | stats count(*) by @userId`,
      },
      {
        title: '识别任何API调用的错误',
        expr: stripIndents`filter Operation = <operation> AND Fault > 0
        | fields @timestamp, @logStream as instanceId, ExceptionMessage`,
      },
      {
        title:
          '使用regex获取每5分钟记录的异常数，其中异常不区分大小写',
        expr: stripIndents`filter @message like /(?i)Exception/
        | stats count(*) as exceptionCount by bin(5m)
        | sort exceptionCount desc`,
      },
      {
        title: '使用glob表达式解析临时字段',
        expr: stripIndents`parse @message "user=*, method:*, latency := *" as @user, @method, @latency
        | stats avg(@latency) by @method, @user`,
      },
      {
        title: '使用正则表达式使用glob表达式解析临时字段',
        expr: stripIndents`parse @message /user=(?<user2>.*?), method:(?<method2>.*?), latency := (?<latency2>.*?)/
        | stats avg(latency2) by @method2, @user2`,
      },
      {
        title: '为包含ERROR字符串的事件提取临时字段并显示字段',
        expr: stripIndents`fields @message
        | parse @message "* [*] *" as loggingTime, loggingType, loggingMessage
        | filter loggingType IN ["ERROR"]
        | display loggingMessage, loggingType = "ERROR" as isError`,
      },
      {
        title: '从查询结果中修剪空白',
        expr: stripIndents`fields trim(@message) as trimmedMessage
        | parse trimmedMessage "[*] * * Retrieving CloudWatch Metrics for AccountID : *, CloudWatch Metric : *, Resource Type : *, ResourceID : *" as level, time, logId, accountId, metric, type, resourceId
        | display level, time, logId, accountId, metric, type, resourceId
        | filter level like "INFO"`,
      },
    ],
  },
  {
    category: 'Route 53',
    examples: [
      {
        title: '边缘位置每10分钟接收的请求数',
        expr: 'stats count(*) by queryType, bin(10m)',
      },
      {
        title: '按域划分的未成功请求数',
        expr: 'filter responseCode="SERVFAIL" | stats count(*) by queryName',
      },
      {
        title: '请求数量最多的前10个DNS解析程序IP',
        expr: 'stats count(*) as numRequests by resolverIp | sort numRequests desc | limit 10',
      },
    ],
  },
  {
    category: 'AWS AppSync',
    examples: [
      {
        title: '唯一HTTP状态代码的数目',
        expr: stripIndents`fields ispresent(graphQLAPIId) as isApi |
                           filter isApi |
                           filter logType = "RequestSummary" |
                           stats count() as statusCount by statusCode |
                           sort statusCount desc`,
      },
      {
        title: '具有最大延迟的前10个解析器',
        expr: stripIndents`fields resolverArn, duration |
                           filter logType = "Tracing" |
                           sort duration desc |
                           limit 10`,
      },
      {
        title: '最常调用的解析器',
        expr: stripIndents`fields ispresent(resolverArn) as isRes |
                           stats count() as invocationCount by resolverArn |
                           filter isRes |
                           filter logType = "Tracing" |
                           sort invocationCount desc |
                           limit 10`,
      },
      {
        title: '映射模板中错误最多的解析程序',
        expr: stripIndents`fields ispresent(resolverArn) as isRes |
                           stats count() as errorCount by resolverArn, logType |
                           filter isRes and (logType = "RequestMapping" or logType = "ResponseMapping") and fieldInError |
                           sort errorCount desc |
                           limit 10`,
      },
      {
        title: '字段延迟统计信息',
        expr: stripIndents`fields requestId, latency |
                           filter logType = "RequestSummary" |
                           sort latency desc |
                           limit 10`,
      },
      {
        title: '解析程序延迟统计信息',
        expr: stripIndents`fields ispresent(resolverArn) as isRes |
                           filter isRes |
                           filter logType = "Tracing" |
                           stats min(duration), max(duration), avg(duration) as avgDur by resolverArn |
                           sort avgDur desc |
                           limit 10`,
      },
      {
        title: '具有最大延迟的前10个请求',
        expr: stripIndents`fields requestId, latency |
                           filter logType = "RequestSummary" |
                           sort latency desc |
                           limit 10`,
      },
    ],
  },
];

const COMMANDS: QueryExample[] = [
  {
    category: 'fields',
    examples: [
      {
        description:
          '检索一个或多个日志字段。您还可以在该命令中使用诸如abs（a+b）、sqrt（a/b）、log（a）+log（b）、strlen（trim（））、datefloor（）、isPresent（）等函数和操作.',
        expr: 'fields @log, @logStream, @message, @timestamp',
      },
    ],
  },
  {
    category: 'filter',
    examples: [
      {
        description:
          '基于一个或多个条件检索日志字段。您可以使用比较运算符，如=、！=、>、>=、<、<=、，该命令中的布尔运算符，如and、or、and not和正则表达式.',
        expr: 'filter @message like /(?i)(Exception|error|fail|5dd)/',
      },
    ],
  },
  {
    category: 'stats',
    examples: [
      {
        description: '计算日志字段的聚合统计信息，如sum（）、avg（）、count（）、min（）和max（）.',
        expr: 'stats count() by bin(5m)',
      },
    ],
  },
  {
    category: 'sort',
    examples: [
      {
        description: '按升序或降序对日志字段进行排序',
        expr: 'sort @timestamp asc',
      },
    ],
  },
  {
    category: 'limit',
    examples: [
      {
        description: '限制查询返回的日志事件数.',
        expr: 'limit 10',
      },
    ],
  },
  {
    category: 'parse',
    examples: [
      {
        description:
          '创建一个或多个临时字段，查询可以进一步处理这些字段。以下示例将从@message中提取临时字段host、identity、dateTimeString、httpVerb、url、protocol、statusCode、bytes，并返回按最大值（字节）降序排列的url、max（字节）和avg（字节）字段.',
        expr: stripIndents`parse '* - * [*] "* * *" * *' as host, identity, dateTimeString, httpVerb, url, protocol, statusCode, bytes
        | stats max(bytes) as maxBytes, avg(bytes) by url
        | sort maxBytes desc`,
      },
    ],
  },
];

function renderHighlightedMarkup(code: string, keyPrefix: string) {
  const grammar = tokenizer;
  const tokens = flattenTokens(Prism.tokenize(code, grammar));
  const spans = tokens
    .filter((token) => typeof token !== 'string')
    .map((token, i) => {
      return (
        <span
          className={`prism-token token ${token.types.join(' ')} ${token.aliases.join(' ')}`}
          key={`${keyPrefix}-token-${i}`}
        >
          {token.content}
        </span>
      );
    });

  return <div className="slate-query-field">{spans}</div>;
}

const exampleCategory = css`
  margin-top: 5px;
`;

const link = css`
  text-decoration: underline;
`;

type Props = {
  onClickExample: (query: CloudWatchQuery) => void;
  query: CloudWatchQuery;
};

const LogsCheatSheet = (props: Props) => {
  const [isCommandsOpen, setIsCommandsOpen] = useState(false);
  const [isQueriesOpen, setIsQueriesOpen] = useState(false);

  return (
    <div>
      <h3>CloudWatch Logs cheat sheet</h3>
      <Collapse
        label="Commands"
        collapsible={true}
        isOpen={isCommandsOpen}
        onToggle={(isOpen) => setIsCommandsOpen(isOpen)}
      >
        <>
          {COMMANDS.map((cat, i) => (
            <div key={`cat-${i}`}>
              <h5>{cat.category}</h5>
              {cat.examples.map((item, j) => (
                <div key={`item-${j}`}>
                  <p>{item.description}</p>
                  <button
                    type="button"
                    className="cheat-sheet-item__example"
                    key={item.expr}
                    onClick={() =>
                      props.onClickExample({
                        refId: props.query.refId ?? 'A',
                        expression: item.expr,
                        queryMode: 'Logs',
                        region: props.query.region,
                        id: props.query.refId ?? 'A',
                        logGroupNames: 'logGroupNames' in props.query ? props.query.logGroupNames : [],
                        logGroups: 'logGroups' in props.query ? props.query.logGroups : [],
                      })
                    }
                  >
                    <pre>{renderHighlightedMarkup(item.expr, `item-${j}`)}</pre>
                  </button>
                </div>
              ))}
            </div>
          ))}
        </>
      </Collapse>
      <Collapse
        label="Queries"
        collapsible={true}
        isOpen={isQueriesOpen}
        onToggle={(isOpen) => setIsQueriesOpen(isOpen)}
      >
        {QUERIES.map((cat, i) => (
          <div key={`cat-${i}`}>
            <div className={`cheat-sheet-item__title ${cx(exampleCategory)}`}>{cat.category}</div>
            {cat.examples.map((item, j) => (
              <div className="cheat-sheet-item" key={`item-${j}`}>
                <h4>{item.title}</h4>
                <button
                  type="button"
                  className="cheat-sheet-item__example"
                  key={item.expr}
                  onClick={() =>
                    props.onClickExample({
                      refId: props.query.refId ?? 'A',
                      expression: item.expr,
                      queryMode: 'Logs',
                      region: props.query.region,
                      id: props.query.refId ?? 'A',
                      logGroupNames: 'logGroupNames' in props.query ? props.query.logGroupNames : [],
                      logGroups: 'logGroups' in props.query ? props.query.logGroups : [],
                    })
                  }
                >
                  <pre>{renderHighlightedMarkup(item.expr, `item-${j}`)}</pre>
                </button>
              </div>
            ))}
          </div>
        ))}
      </Collapse>
      <div>
        Note: If you are seeing masked data, you may have CloudWatch logs data protection enabled.{' '}
        <a
          className={cx(link)}
          href="https://grafana.com/docs/grafana/latest/datasources/aws-cloudwatch/#cloudwatch-logs-data-protection"
          target="_blank"
          rel="noreferrer"
        >
          See documentation for details
        </a>
        .
      </div>
    </div>
  );
};

export default LogsCheatSheet;
