import React, { useMemo } from 'react';

import { SelectableValue } from '@grafana/data';
import { EditorField } from '@grafana/experimental';
import { Select } from '@grafana/ui';

export interface Props {
  onChange: (accountId?: string) => void;
  accountOptions: Array<SelectableValue<string>>;
  accountId?: string;
}

export const ALL_ACCOUNTS_OPTION = {
  label: 'All',
  value: 'all',
  description: '以所有链接帐户为目标',
};

export function Account({ accountId, onChange, accountOptions }: Props) {
  const selectedAccountExistsInOptions = useMemo(
    () =>
      accountOptions.find((a) => {
        if (a.options) {
          const matchingTemplateVar = a.options.find((tempVar: SelectableValue<string>) => {
            return tempVar.value === accountId;
          });
          return matchingTemplateVar;
        }
        return a.value === accountId;
      }),
    [accountOptions, accountId]
  );

  if (accountOptions.length === 0) {
    return null;
  }

  return (
    <EditorField
      label="Account"
      width={26}
      tooltip="CloudWatch监控帐户可查看源帐户的数据，因此您可以跨多个帐户集中监控和故障排除活动。转到AWS控制台中的CloudWatch设置页面以了解更多详细信息."
    >
      <Select
        aria-label="Account Selection"
        value={selectedAccountExistsInOptions ? accountId : ALL_ACCOUNTS_OPTION.value}
        options={[ALL_ACCOUNTS_OPTION, ...accountOptions]}
        onChange={({ value }) => {
          onChange(value);
        }}
      />
    </EditorField>
  );
}
