import { css } from '@emotion/css';
import React from 'react';

import { GrafanaTheme2 } from '@grafana/data';
import { DataSourcePicker } from '@grafana/runtime';
import { Alert, InlineField, useStyles2 } from '@grafana/ui';
import { getDatasourceSrv } from 'app/features/plugins/datasource_srv';

const getStyles = (theme: GrafanaTheme2) => ({
  infoText: css`
    padding-bottom: ${theme.spacing(2)};
    color: ${theme.colors.text.secondary};
  `,
});

interface Props {
  datasourceUid?: string;
  onChange: (uid: string) => void;
}

const xRayDsId = 'grafana-x-ray-datasource';

export function XrayLinkConfig({ datasourceUid, onChange }: Props) {
  const hasXrayDatasource = Boolean(getDatasourceSrv().getList({ pluginId: xRayDsId }).length);

  const styles = useStyles2(getStyles);

  return (
    <>
      <h3 className="page-heading">X-ray trace link</h3>

      <div className={styles.infoText}>
        如果日志包含@xrayTraceId字段，Grafana将自动创建到X射线数据源中跟踪的链接
      </div>

      {!hasXrayDatasource && (
        <Alert
          title={
            '没有可链接的X射线数据源。首先添加X射线数据来源，然后将其链接到Cloud Watch. '
          }
          severity="info"
        />
      )}

      <div className="gf-form-group">
        <InlineField
          htmlFor="data-source-picker"
          label="Data source"
          labelWidth={28}
          tooltip="X-ray data source containing traces"
        >
          <DataSourcePicker
            pluginId={xRayDsId}
            onChange={(ds) => onChange(ds.uid)}
            current={datasourceUid}
            noDefault={true}
          />
        </InlineField>
      </div>
    </>
  );
}
