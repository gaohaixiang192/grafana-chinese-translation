import React, { useEffect, useState } from 'react';
import { useDebounce } from 'react-use';

import { ConnectionConfig } from '@grafana/aws-sdk';
import {
  rangeUtil,
  DataSourcePluginOptionsEditorProps,
  onUpdateDatasourceJsonDataOption,
  updateDatasourcePluginJsonDataOption,
} from '@grafana/data';
import { Input, InlineField, FieldProps, SecureSocksProxySettings } from '@grafana/ui';
import { notifyApp } from 'app/core/actions';
import { config } from 'app/core/config';
import { createWarningNotification } from 'app/core/copy/appNotification';
import { getDatasourceSrv } from 'app/features/plugins/datasource_srv';
import { store } from 'app/store/store';

import { CloudWatchDatasource } from '../datasource';
import { SelectableResourceValue } from '../resources/types';
import { CloudWatchJsonData, CloudWatchSecureJsonData } from '../types';

import { LogGroupsField } from './LogGroups/LogGroupsField';
import { XrayLinkConfig } from './XrayLinkConfig';

export type Props = DataSourcePluginOptionsEditorProps<CloudWatchJsonData, CloudWatchSecureJsonData>;

type LogGroupFieldState = Pick<FieldProps, 'invalid'> & { error?: string | null };

export const ConfigEditor = (props: Props) => {
  const { options, onOptionsChange } = props;
  const { defaultLogGroups, logsTimeout, defaultRegion, logGroups } = options.jsonData;
  const datasource = useDatasource(props);
  useAuthenticationWarning(options.jsonData);
  const logsTimeoutError = useTimoutValidation(logsTimeout);
  const saved = useDataSourceSavedState(props);
  const [logGroupFieldState, setLogGroupFieldState] = useState<LogGroupFieldState>({
    invalid: false,
  });
  useEffect(() => setLogGroupFieldState({ invalid: false }), [props.options]);

  return (
    <>
      <ConnectionConfig
        {...props}
        labelWidth={29}
        loadRegions={
          datasource &&
          (async () => {
            return datasource.resources
              .getRegions()
              .then((regions) =>
                regions.reduce(
                  (acc: string[], curr: SelectableResourceValue) => (curr.value ? [...acc, curr.value] : acc),
                  []
                )
              );
          })
        }
      >
        <InlineField label="Namespaces of Custom Metrics" labelWidth={29} tooltip="Namespaces of Custom Metrics.">
          <Input
            width={60}
            placeholder="Namespace1,Namespace2"
            value={options.jsonData.customMetricsNamespaces || ''}
            onChange={onUpdateDatasourceJsonDataOption(props, 'customMetricsNamespaces')}
          />
        </InlineField>
      </ConnectionConfig>

      {config.secureSocksDSProxyEnabled && (
        <SecureSocksProxySettings options={options} onOptionsChange={onOptionsChange} />
      )}

      <h3 className="page-heading">CloudWatch Logs</h3>
      <div className="gf-form-group">
        <InlineField
          label="Retry Timeout"
          labelWidth={28}
          tooltip='Cloudwatch日志最多允许30个并发查询。如果Grafana在Cloudwatch日志中遇到并发最大查询错误，它将自动重试请求查询长达30分钟。此重试超时策略是可配置的。必须是有效的持续时间字符串，如“15m”“30s”“2000ms”等.'
          invalid={Boolean(logsTimeoutError)}
        >
          <Input
            width={60}
            placeholder="30m"
            value={options.jsonData.logsTimeout || ''}
            onChange={onUpdateDatasourceJsonDataOption(props, 'logsTimeout')}
            title={'超时必须是有效的持续时间字符串，如“15米”“30秒”“2000米”等.'}
          />
        </InlineField>
        <InlineField
          label="默认日志组"
          labelWidth={28}
          tooltip="（可选）为CloudWatch日志查询指定默认日志组."
          shrink={true}
          {...logGroupFieldState}
        >
          <LogGroupsField
            region={defaultRegion ?? ''}
            datasource={datasource}
            onBeforeOpen={() => {
              if (saved) {
                return;
              }

              let error = '添加日志组之前需要保存数据源.';
              if (props.options.version && props.options.version > 1) {
                error =
                  '您有未保存的连接详细信息更改。添加日志组之前需要保存数据源.';
              }
              setLogGroupFieldState({
                invalid: true,
                error,
              });
              throw new Error(error);
            }}
            legacyLogGroupNames={defaultLogGroups}
            logGroups={logGroups}
            onChange={(updatedLogGroups) => {
              onOptionsChange({
                ...props.options,
                jsonData: {
                  ...props.options.jsonData,
                  logGroups: updatedLogGroups,
                  defaultLogGroups: undefined,
                },
              });
            }}
            maxNoOfVisibleLogGroups={2}
          />
        </InlineField>
      </div>
      <XrayLinkConfig
        onChange={(uid) => updateDatasourcePluginJsonDataOption(props, 'tracingDatasourceUid', uid)}
        datasourceUid={options.jsonData.tracingDatasourceUid}
      />
    </>
  );
};

function useAuthenticationWarning(jsonData: CloudWatchJsonData) {
  const addWarning = (message: string) => {
    store.dispatch(notifyApp(createWarningNotification('CloudWatch Authentication', message)));
  };

  useEffect(() => {
    if (jsonData.authType === 'arn') {
      addWarning('由于grafana 7.3身份验证类型“arn”已弃用，因此返回默认SDK提供程序');
    } else if (jsonData.authType === 'credentials' && !jsonData.profile && !jsonData.database) {
      addWarning(
        '从grafana 7.3起，身份验证类型“凭据”应仅用于共享文件凭据。\
        如果您没有凭据文件，请切换到用于提取凭据的默认SDK提供程序 \
        来自环境变量或IAM角色'
      );
    }
  }, [jsonData.authType, jsonData.database, jsonData.profile]);
}

function useDatasource(props: Props) {
  const [datasource, setDatasource] = useState<CloudWatchDatasource>();

  useEffect(() => {
    if (props.options.version) {
      getDatasourceSrv()
        .loadDatasource(props.options.name)
        .then((datasource) => {
          if (datasource instanceof CloudWatchDatasource) {
            setDatasource(datasource);
          }
        });
    }
  }, [props.options.version, props.options.name]);

  return datasource;
}

function useTimoutValidation(value: string | undefined) {
  const [err, setErr] = useState<undefined | string>(undefined);
  useDebounce(
    () => {
      if (value) {
        try {
          rangeUtil.describeInterval(value);
          setErr(undefined);
        } catch (e) {
          if (e instanceof Error) {
            setErr(e.toString());
          }
        }
      } else {
        setErr(undefined);
      }
    },
    350,
    [value]
  );
  return err;
}

function useDataSourceSavedState(props: Props) {
  const [saved, setSaved] = useState(!!props.options.version && props.options.version > 1);
  useEffect(() => {
    setSaved(false);
  }, [
    props.options.jsonData.assumeRoleArn,
    props.options.jsonData.authType,
    props.options.jsonData.defaultRegion,
    props.options.jsonData.endpoint,
    props.options.jsonData.externalId,
    props.options.jsonData.profile,
    props.options.secureJsonData?.accessKey,
    props.options.secureJsonData?.secretKey,
  ]);

  useEffect(() => {
    props.options.version && setSaved(true);
  }, [props.options.version]);

  return saved;
}
