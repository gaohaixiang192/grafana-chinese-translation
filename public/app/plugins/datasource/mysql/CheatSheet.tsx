import { css } from '@emotion/css';
import React from 'react';

import { GrafanaTheme2 } from '@grafana/data';
import { useStyles2 } from '@grafana/ui';

export function CheatSheet() {
  const styles = useStyles2(getStyles);

  return (
    <div>
      <h2>MySQL备忘单</h2>
      时间序列:
      <ul className={styles.ulPadding}>
        <li>
          返回名为time或time_sec（UTC）的列，作为unix时间戳或任何sql本机日期数据类型。您可以使用以下宏.
        </li>
        <li>返回以数字数据类型为值的列</li>
      </ul>
      随意的:
      <ul className={styles.ulPadding}>
        <li>
          已命名的返回列 <i>metric</i> 表示系列名称.
        </li>
        <li>如果返回多个值列，则使用度量列作为前缀.</li>
        <li>如果未找到列命名度量，则值列的列名将用作序列名</li>
      </ul>
      <p>时间序列查询的结果集需要按时间排序.</p>
      表:
      <ul className={styles.ulPadding}>
        <li>返回任意一组列</li>
      </ul>
      Macros:
      <ul className={styles.ulPadding}>
        <li>$__time(column) -&gt; UNIX_TIMESTAMP(column) as time_sec</li>
        <li>$__timeEpoch(column) -&gt; UNIX_TIMESTAMP(column) as time_sec</li>
        <li>$__timeFilter(column) -&gt; column BETWEEN FROM_UNIXTIME(1492750877) AND FROM_UNIXTIME(1492750877)</li>
        <li>$__unixEpochFilter(column) -&gt; time_unix_epoch &gt; 1492750877 AND time_unix_epoch &lt; 1492750877</li>
        <li>
          $__unixEpochNanoFilter(column) -&gt; column &gt;= 1494410783152415214 AND column &lt;= 1494497183142514872
        </li>
        <li>
          $__timeGroup(column,&apos;5m&apos;[, fillvalue]) -&gt; cast(cast(UNIX_TIMESTAMP(column)/(300) as signed)*300
          as signed) 通过设置填充值grafana将根据间隔填充缺失的值可以
          文字值，NULL或以前的值；previor将填充以前看到的值，如果没有，则为NULL
          还没被看见
        </li>
        <li>
          $__timeGroupAlias(column,&apos;5m&apos;) -&gt; cast(cast(UNIX_TIMESTAMP(column)/(300) as signed)*300 as
          signed) AS &quot;time&quot;
        </li>
        <li>$__unixEpochGroup(column,&apos;5m&apos;) -&gt; column DIV 300 * 300</li>
        <li>$__unixEpochGroupAlias(column,&apos;5m&apos;) -&gt; column DIV 300 * 300 AS &quot;time&quot;</li>
      </ul>
      <p>分组依据和排序依据示例 $__timeGroup:</p>
      <pre>
        <code>
          $__timeGroupAlias(timestamp_col, &apos;1h&apos;), sum(value_double) as value
          <br />
          FROM yourtable
          <br />
          GROUP BY 1<br />
          ORDER BY 1
          <br />
        </code>
      </pre>
      或者使用这些宏构建自己的条件，这些宏只返回值:
      <ul className={styles.ulPadding}>
        <li>$__timeFrom() -&gt; FROM_UNIXTIME(1492750877)</li>
        <li>$__timeTo() -&gt; FROM_UNIXTIME(1492750877)</li>
        <li>$__unixEpochFrom() -&gt; 1492750877</li>
        <li>$__unixEpochTo() -&gt; 1492750877</li>
        <li>$__unixEpochNanoFrom() -&gt; 1494410783152415214</li>
        <li>$__unixEpochNanoTo() -&gt; 1494497183142514872</li>
      </ul>
    </div>
  );
}

function getStyles(theme: GrafanaTheme2) {
  return {
    ulPadding: css({
      margin: theme.spacing(1, 0),
      paddingLeft: theme.spacing(5),
    }),
  };
}
