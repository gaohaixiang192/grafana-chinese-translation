import { DataSourceApi } from '@grafana/data';
import { getDataSourceSrv } from '@grafana/runtime';

export async function getDS(uid?: string): Promise<DataSourceApi | undefined> {
  if (!uid) {
    return undefined;
  }

  const dsSrv = getDataSourceSrv();
  try {
    return await dsSrv.get(uid);
  } catch (error) {
    console.error('未能加载数据源', error);
    return undefined;
  }
}
