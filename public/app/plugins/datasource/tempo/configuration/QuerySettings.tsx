import { css } from '@emotion/css';
import React from 'react';

import { DataSourcePluginOptionsEditorProps, GrafanaTheme2, updateDatasourcePluginJsonDataOption } from '@grafana/data';
import { InlineField, InlineFieldRow, InlineSwitch, Input, useStyles2 } from '@grafana/ui';

import { TempoJsonData } from '../types';

interface Props extends DataSourcePluginOptionsEditorProps<TempoJsonData> {}

export function QuerySettings({ options, onOptionsChange }: Props) {
  const styles = useStyles2(getStyles);

  return (
    <div className={styles.container}>
      <InlineField
        label="在查询中使用时间范围"
        tooltip="当出现性能问题或超时时，可以使用时间范围，因为它会将搜索范围缩小到定义的范围。默认：禁用"
        labelWidth={26}
      >
        <InlineSwitch
          id="enable-time-shift"
          value={options.jsonData.traceQuery?.timeShiftEnabled || false}
          onChange={(event) => {
            updateDatasourcePluginJsonDataOption({ onOptionsChange, options }, 'traceQuery', {
              ...options.jsonData.traceQuery,
              timeShiftEnabled: event.currentTarget.checked,
            });
          }}
        />
      </InlineField>
      <InlineFieldRow>
        <InlineField
          label="开始搜索的时间偏移"
          labelWidth={26}
          disabled={!options.jsonData.traceQuery?.timeShiftEnabled}
          grow
          tooltip="按TraceID搜索时，移动时间范围的开始。搜索可能会返回未完全落入搜索时间范围的跟踪，因此我们建议对较长的跟踪使用更高的时间偏移。默认值：30m（此处可以使用时间单位，例如：5s、1m、3h）"
        >
          <Input
            type="text"
            placeholder="30m"
            width={40}
            onChange={(v) =>
              updateDatasourcePluginJsonDataOption({ onOptionsChange, options }, 'traceQuery', {
                ...options.jsonData.traceQuery,
                spanStartTimeShift: v.currentTarget.value,
              })
            }
            value={options.jsonData.traceQuery?.spanStartTimeShift || ''}
          />
        </InlineField>
      </InlineFieldRow>
      <InlineFieldRow>
        <InlineField
          label="搜索结束的时间偏移"
          labelWidth={26}
          disabled={!options.jsonData.traceQuery?.timeShiftEnabled}
          grow
          tooltip="按TraceID搜索时移动时间范围的末尾。搜索可能会返回未完全落入搜索时间范围的跟踪，因此我们建议对较长的跟踪使用更高的时间偏移。默认值：30m（此处可以使用时间单位，例如：5s、1m、3h）"
        >
          <Input
            type="text"
            placeholder="30m"
            width={40}
            onChange={(v) =>
              updateDatasourcePluginJsonDataOption({ onOptionsChange, options }, 'traceQuery', {
                ...options.jsonData.traceQuery,
                spanEndTimeShift: v.currentTarget.value,
              })
            }
            value={options.jsonData.traceQuery?.spanEndTimeShift || ''}
          />
        </InlineField>
      </InlineFieldRow>
    </div>
  );
}

export const getStyles = (theme: GrafanaTheme2) => ({
  infoText: css`
    padding-bottom: ${theme.spacing(2)};
    color: ${theme.colors.text.secondary};
  `,
  container: css`
    width: 100%;
  `,
  row: css`
    align-items: baseline;
  `,
});
