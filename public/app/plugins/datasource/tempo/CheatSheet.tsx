import React from 'react';

import { config, reportInteraction } from '@grafana/runtime';

export default function CheatSheet() {
  reportInteraction('grafana_traces_cheatsheet_clicked', {
    datasourceType: 'tempo',
    grafana_version: config.buildInfo.version,
  });

  return (
    <div>
      <h2 id="tempo-cheat-sheet">Tempo Cheat Sheet</h2>
      <p>
        Tempo是一个跟踪id查找存储。在上面的字段中输入跟踪id，然后点击“运行查询”以检索您的跟踪.
        Tempo通常与其他数据源（如Loki或Prometheus）配对以查找痕迹.
      </p>
      <p>
        这里有一些{' '}
        <a href="https://grafana.com/docs/tempo/latest/guides/instrumentation/" target="blank">
          仪器仪表示例
        </a>{' '}
        通过日志和度量开始跟踪发现（示例）.
      </p>
    </div>
  );
}
