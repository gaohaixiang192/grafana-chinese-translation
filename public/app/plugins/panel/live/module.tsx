import { PanelPlugin } from '@grafana/data';

import { LiveChannelEditor } from './LiveChannelEditor';
import { LivePanel } from './LivePanel';
import { LivePanelOptions, MessageDisplayMode } from './types';

export const plugin = new PanelPlugin<LivePanelOptions>(LivePanel).setPanelOptions((builder) => {
  builder.addCustomEditor({
    category: ['Channel'],
    id: 'channel',
    path: 'channel',
    name: 'Channel',
    editor: LiveChannelEditor,
    defaultValue: {},
  });

  builder
    .addRadio({
      path: 'message',
      name: '显示消息',
      description: '显示此频道上收到的最后一条消息',
      settings: {
        options: [
          { value: MessageDisplayMode.Raw, label: 'Raw Text' },
          { value: MessageDisplayMode.JSON, label: 'JSON' },
          { value: MessageDisplayMode.Auto, label: 'Auto' },
          { value: MessageDisplayMode.None, label: 'None' },
        ],
      },
      defaultValue: MessageDisplayMode.JSON,
    })
    .addBooleanSwitch({
      path: 'publish',
      name: '显示发布',
      description: '显示用于发布值的窗体',
      defaultValue: false,
    });
});
