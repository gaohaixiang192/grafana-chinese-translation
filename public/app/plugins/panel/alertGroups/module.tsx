import React, { useMemo } from 'react';

import { PanelPlugin } from '@grafana/data';
import { AlertManagerPicker } from 'app/features/alerting/unified/components/AlertManagerPicker';
import {
  getAllAlertManagerDataSources,
  GRAFANA_RULES_SOURCE_NAME,
} from 'app/features/alerting/unified/utils/datasource';

import { AlertGroupsPanel } from './AlertGroupsPanel';
import { Options } from './panelcfg.gen';

export const plugin = new PanelPlugin<Options>(AlertGroupsPanel).setPanelOptions((builder) => {
  return builder
    .addCustomEditor({
      name: 'Alertmanager',
      path: 'alertmanager',
      id: 'alertmanager',
      defaultValue: GRAFANA_RULES_SOURCE_NAME,
      category: ['Options'],
      editor: function RenderAlertmanagerPicker(props) {
        const alertManagers = useMemo(getAllAlertManagerDataSources, []);

        return (
          <AlertManagerPicker
            current={props.value}
            onChange={(alertManagerSourceName) => {
              return props.onChange(alertManagerSourceName);
            }}
            dataSources={alertManagers}
          />
        );
      },
    })
    .addBooleanSwitch({
      name: '默认情况下全部展开',
      path: 'expandAll',
      defaultValue: false,
      category: ['Options'],
    })
    .addTextInput({
      description: '按匹配标签筛选结果，例如：env=生产，严重性=~严重|警告',
      name: 'Labels',
      path: 'labels',
      category: ['Filter'],
    });
});
