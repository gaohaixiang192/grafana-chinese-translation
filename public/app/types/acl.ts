export enum TeamPermissionLevel {
  Admin = 4,
  Editor = 2,
  Member = 0,
  Viewer = 1,
}

export enum OrgRole {
  Viewer = 'Viewer',
  Editor = 'Editor',
  Admin = 'Admin',
}

export interface DashboardAclDTO {
  id?: number;
  dashboardId?: number;
  userId?: number;
  userLogin?: string;
  userEmail?: string;
  teamId?: number;
  team?: string;
  permission?: PermissionLevel;
  role?: OrgRole;
  icon?: string;
  inherited?: boolean;
}

export interface DashboardAclUpdateDTO {
  userId?: number;
  teamId?: number;
  role?: OrgRole;
  permission?: PermissionLevel;
}

export interface DashboardAcl {
  id?: number;
  dashboardId?: number;
  userId?: number;
  userLogin?: string;
  userEmail?: string;
  teamId?: number;
  team?: string;
  permission?: PermissionLevel;
  role?: OrgRole;
  icon?: string;
  name?: string;
  inherited?: boolean;
  sortRank?: number;
  userAvatarUrl?: string;
  teamAvatarUrl?: string;
}

export interface DashboardPermissionInfo {
  value: PermissionLevel;
  label: string;
  description: string;
}

export interface NewDashboardAclItem {
  teamId: number;
  userId: number;
  role?: OrgRole;
  permission: PermissionLevel;
  type: AclTarget;
}

export enum PermissionLevel {
  View = 1,
  Edit = 2,
  Admin = 4,
}

export enum PermissionLevelString {
  View = 'View',
  Edit = 'Edit',
  Admin = 'Admin',
}

export enum SearchQueryType {
  Folder = 'dash-folder',
  Dashboard = 'dash-db',
  AlertFolder = 'dash-folder-alerting',
}

export enum DataSourcePermissionLevel {
  Query = 1,
  Admin = 2,
}

export enum AclTarget {
  Team = 'Team',
  User = 'User',
  Viewer = 'Viewer',
  Editor = 'Editor',
}

export interface AclTargetInfo {
  value: AclTarget;
  label: string;
}

export const dataSourceAclLevels = [
  { value: DataSourcePermissionLevel.Query, label: 'Query', description: '可以查询数据源.' },
];

export const dashboardAclTargets: AclTargetInfo[] = [
  { value: AclTarget.Team, label: '团队' },
  { value: AclTarget.User, label: '用户' },
  { value: AclTarget.Viewer, label: '具有查看器角色的所有人' },
  { value: AclTarget.Editor, label: '具有编辑角色的每个人' },
];

export const dashboardPermissionLevels: DashboardPermissionInfo[] = [
  { value: PermissionLevel.View, label: PermissionLevelString.View, description: '可以查看仪表板.' },
  {
    value: PermissionLevel.Edit,
    label: PermissionLevelString.Edit,
    description: '可以添加、编辑和删除仪表板.',
  },
  {
    value: PermissionLevel.Admin,
    label: 'Admin',
    description: '可以添加/删除权限，还可以添加、编辑和删除仪表板.',
  },
];

export interface TeamPermissionInfo {
  value: TeamPermissionLevel;
  label: string;
  description: string;
}

export const teamsPermissionLevels: TeamPermissionInfo[] = [
  { value: TeamPermissionLevel.Member, label: 'Member', description: '是团队成员' },
  {
    value: TeamPermissionLevel.Admin,
    label: 'Admin',
    description: '可以添加/删除权限、成员和删除团队.',
  },
];
