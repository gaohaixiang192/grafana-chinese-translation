import { css } from '@emotion/css';
import React from 'react';

import { GrafanaTheme2 } from '@grafana/data/src';
import { ConfirmModal, useStyles2 } from '@grafana/ui/src';

const Body = ({ title }: { title?: string }) => {
  const styles = useStyles2(getStyles);

  return (
    <p className={styles.description}>
      {title
        ? '是否确实要吊销此URL？仪表板将不再是公共的.'
        : '孤立的公共仪表板将不再是公共的.'}
    </p>
  );
};

export const DeletePublicDashboardModal = ({
  dashboardTitle,
  onConfirm,
  onDismiss,
}: {
  dashboardTitle?: string;
  onConfirm: () => void;
  onDismiss: () => void;
}) => (
  <ConfirmModal
    isOpen
    body={<Body title={dashboardTitle} />}
    onConfirm={onConfirm}
    onDismiss={onDismiss}
    title="吊销公共URL"
    icon="trash-alt"
    confirmText="吊销公共URL"
  />
);

const getStyles = (theme: GrafanaTheme2) => ({
  title: css`
    margin-bottom: ${theme.spacing(1)};
  `,
  description: css`
    font-size: ${theme.typography.body.fontSize};
  `,
});
