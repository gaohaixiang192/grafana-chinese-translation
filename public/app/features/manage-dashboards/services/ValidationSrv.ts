import { backendSrv } from 'app/core/services/backend_srv';

const hitTypes = {
  FOLDER: 'dash-folder',
  DASHBOARD: 'dash-db',
};

class ValidationError extends Error {
  type: string;

  constructor(type: string, message: string) {
    super(message);
    this.type = type;
  }
}

export class ValidationSrv {
  rootName = 'general';

  validateNewDashboardName(folderUid: any, name: string) {
    return this.validate(folderUid, name, '具有相同名称的仪表板或文件夹已存在');
  }

  validateNewFolderName(name?: string) {
    return this.validate(0, name, '常规文件夹中已存在同名的文件夹或仪表板');
  }

  private async validate(folderId: any, name: string | undefined, existingErrorMessage: string) {
    name = (name || '').trim();
    const nameLowerCased = name.toLowerCase();

    if (name.length === 0) {
      throw new ValidationError('REQUIRED', '姓名是必填项');
    }

    if (folderId === 0 && nameLowerCased === this.rootName) {
      throw new ValidationError('EXISTING', '这是一个保留名称，不能用于文件夹.');
    }

    const promises = [];
    promises.push(backendSrv.search({ type: hitTypes.FOLDER, folderIds: [folderId], query: name }));
    promises.push(backendSrv.search({ type: hitTypes.DASHBOARD, folderIds: [folderId], query: name }));

    const res = await Promise.all(promises);
    let hits: any[] = [];

    if (res.length > 0 && res[0].length > 0) {
      hits = res[0];
    }

    if (res.length > 1 && res[1].length > 0) {
      hits = hits.concat(res[1]);
    }

    for (const hit of hits) {
      if (nameLowerCased === hit.title.toLowerCase()) {
        throw new ValidationError('EXISTING', existingErrorMessage);
      }
    }

    return;
  }
}

export const validationSrv = new ValidationSrv();
