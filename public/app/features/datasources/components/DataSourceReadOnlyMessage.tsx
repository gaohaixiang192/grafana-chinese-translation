import React from 'react';

import { selectors as e2eSelectors } from '@grafana/e2e-selectors';
import { Alert } from '@grafana/ui';

export const readOnlyMessage =
  '数据源是通过配置添加的，无法使用UI进行修改。请与您的服务器管理员联系以更新此数据源.';

export function DataSourceReadOnlyMessage() {
  return (
    <Alert aria-label={e2eSelectors.pages.DataSource.readOnly} severity="info" title="已设置的数据源">
      {readOnlyMessage}
    </Alert>
  );
}
