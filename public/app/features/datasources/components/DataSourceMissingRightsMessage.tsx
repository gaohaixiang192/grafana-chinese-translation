import React from 'react';

import { Alert } from '@grafana/ui';

export const missingRightsMessage =
  '不允许您修改此数据源。请与您的服务器管理员联系以更新此数据源.';

export function DataSourceMissingRightsMessage() {
  return (
    <Alert severity="info" title="缺少权利">
      {missingRightsMessage}
    </Alert>
  );
}
