import React from 'react';

import { Card } from '@grafana/ui';
import EmptyListCTA from 'app/core/components/EmptyListCTA/EmptyListCTA';

interface Props {
  onClick?: () => void;
  canWriteCorrelations: boolean;
}
export const EmptyCorrelationsCTA = ({ onClick, canWriteCorrelations }: Props) => {
  // TODO: if there are no datasources show a different message

  return canWriteCorrelations ? (
    <EmptyListCTA
      title="你还没有定义任何相关性."
      buttonIcon="gf-glue"
      onClick={onClick}
      buttonTitle="添加相关性"
      proTip="还可以通过数据源配置定义相关性"
    />
  ) : (
    <Card>
      <Card.Heading>尚未配置相关性.</Card.Heading>
      <Card.Description>请与管理员联系以创建新的相关性.</Card.Description>
    </Card>
  );
};
