import { css, cx } from '@emotion/css';
import React from 'react';
import { useFormContext } from 'react-hook-form';

import { GrafanaTheme2 } from '@grafana/data';
import { Field, FieldSet, Input, TextArea, useStyles2 } from '@grafana/ui';

import { useCorrelationsFormContext } from './correlationsFormContext';
import { FormDTO } from './types';
import { getInputId } from './utils';

const getStyles = (theme: GrafanaTheme2) => ({
  label: css`
    max-width: ${theme.spacing(80)};
  `,
  description: css`
    max-width: ${theme.spacing(80)};
  `,
});

export const ConfigureCorrelationBasicInfoForm = () => {
  const { register, formState } = useFormContext<FormDTO>();
  const styles = useStyles2(getStyles);
  const { correlation, readOnly } = useCorrelationsFormContext();

  return (
    <>
      <FieldSet label="定义相关性标签（第1步，共3步）">
        <p>Define text that will describe the correlation.</p>
        <input type="hidden" {...register('config.type')} />
        <Field
          label="Label"
          description="此名称将用作相关性的标签。这将显示为按钮文本、菜单项或链接上的悬停文本."
          className={styles.label}
          invalid={!!formState.errors.label}
          error={formState.errors.label?.message}
        >
          <Input
            id={getInputId('label', correlation)}
            {...register('label', { required: { value: true, message: '此字段是必需的。.' } })}
            readOnly={readOnly}
            placeholder="e.g. Tempo traces"
          />
        </Field>

        <Field
          label="Description"
          description="可选说明以及有关链接的详细信息"
          // the Field component automatically adds margin to itself, so we are forced to workaround it by overriding  its styles
          className={cx(styles.description)}
        >
          <TextArea id={getInputId('description', correlation)} {...register('description')} readOnly={readOnly} />
        </Field>
      </FieldSet>
    </>
  );
};
