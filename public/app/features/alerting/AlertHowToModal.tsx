import React from 'react';

import { Modal, VerticalGroup } from '@grafana/ui';

export interface AlertHowToModalProps {
  onDismiss: () => void;
}

export function AlertHowToModal({ onDismiss }: AlertHowToModalProps): JSX.Element {
  return (
    <Modal title="Adding an Alert" isOpen onDismiss={onDismiss} onClickBackdrop={onDismiss}>
      <VerticalGroup spacing="sm">
        <img src="public/img/alert_howto_new.png" alt="" />
        <p>
          在任何仪表板图形面板的“警报”选项卡中添加和配置警报，使您能够构建和可视化
          使用现有查询的警报。
        </p>
        <p>请记住保存仪表板以保持您的警报规则更改。</p>
      </VerticalGroup>
    </Modal>
  );
}
