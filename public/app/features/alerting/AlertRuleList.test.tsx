import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { openMenu } from 'react-select-event';
import { mockToolkitActionCreator } from 'test/core/redux/mocks';
import { TestProvider } from 'test/helpers/TestProvider';

import { locationService } from '@grafana/runtime';
import { getRouteComponentProps } from 'app/core/navigation/__mocks__/routeProps';

import appEvents from '../../core/app_events';
import { ShowModalReactEvent } from '../../types/events';

import { AlertHowToModal } from './AlertHowToModal';
import { AlertRuleListUnconnected, Props } from './AlertRuleList';
import { setSearchQuery } from './state/reducers';

jest.mock('../../core/app_events', () => ({
  publish: jest.fn(),
}));

const defaultProps: Props = {
  ...getRouteComponentProps({}),
  search: '',
  isLoading: false,
  alertRules: [],
  getAlertRulesAsync: jest.fn().mockResolvedValue([]),
  setSearchQuery: mockToolkitActionCreator(setSearchQuery),
  togglePauseAlertRule: jest.fn(),
};

const setup = (propOverrides?: object) => {
  const props: Props = {
    ...defaultProps,
    ...propOverrides,
  };

  const { rerender } = render(
    <TestProvider>
      <AlertRuleListUnconnected {...props} />
    </TestProvider>
  );

  return {
    rerender: (element: JSX.Element) => rerender(<TestProvider>{element}</TestProvider>),
  };
};

afterEach(() => {
  jest.clearAllMocks();
});

describe('AlertRuleList', () => {
  it('装载时应调用fetchrules', () => {
    jest.spyOn(AlertRuleListUnconnected.prototype, 'fetchRules');

    expect(AlertRuleListUnconnected.prototype.fetchRules).not.toHaveBeenCalled();
    setup();
    expect(AlertRuleListUnconnected.prototype.fetchRules).toHaveBeenCalled();
  });

  it('应在道具更改时调用fetchrules', () => {
    const fetchRulesSpy = jest.spyOn(AlertRuleListUnconnected.prototype, 'fetchRules');
    expect(AlertRuleListUnconnected.prototype.fetchRules).not.toHaveBeenCalled();
    const { rerender } = setup();
    expect(AlertRuleListUnconnected.prototype.fetchRules).toHaveBeenCalled();

    fetchRulesSpy.mockReset();
    rerender(<AlertRuleListUnconnected {...defaultProps} queryParams={{ state: 'ok' }} />);
    expect(AlertRuleListUnconnected.prototype.fetchRules).toHaveBeenCalled();
  });

  describe('获取状态筛选器', () => {
    it('如果道具没有设置，应该是全部', () => {
      setup();
      expect(screen.getByText('All')).toBeInTheDocument();
    });

    it('如果已设置，则应返回状态筛选器', () => {
      setup({
        queryParams: { state: 'not_ok' },
      });
      expect(screen.getByText('Not OK')).toBeInTheDocument();
    });
  });

  describe('状态筛选器已更改', () => {
    it('应该更新位置', async () => {
      setup();
      const stateFilterSelect = screen.getByLabelText('States');
      openMenu(stateFilterSelect);
      await userEvent.click(screen.getByText('Not OK'));
      expect(locationService.getSearchObject().state).toBe('not_ok');
    });
  });

  describe('打开如何', () => {
    it('应发出show模态事件', async () => {
      setup();

      await userEvent.click(screen.getByRole('button', { name: 'How to add an alert' }));
      expect(appEvents.publish).toHaveBeenCalledWith(new ShowModalReactEvent({ component: AlertHowToModal }));
    });
  });

  describe('搜索查询更改', () => {
    it('应设置搜索查询', async () => {
      setup();

      await userEvent.click(screen.getByPlaceholderText('Search alerts'));
      await userEvent.paste('dashboard');
      expect(defaultProps.setSearchQuery).toHaveBeenCalledWith('dashboard');
    });
  });
});
