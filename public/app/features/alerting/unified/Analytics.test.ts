import { dateTime } from '@grafana/data';
import { getBackendSrv } from '@grafana/runtime';

import { isNewUser, USER_CREATION_MIN_DAYS } from './Analytics';

jest.mock('@grafana/runtime', () => ({
  ...jest.requireActual('@grafana/runtime'),
  getBackendSrv: jest.fn().mockReturnValue({
    get: jest.fn(),
  }),
}));

describe('isNewUser', function () {
  it('如果用户是在上周内创建的，则应返回true', async () => {
    const newUser = {
      id: 1,
      createdAt: dateTime().subtract(6, 'days'),
    };

    getBackendSrv().get = jest.fn().mockResolvedValue(newUser);

    const isNew = await isNewUser();
    expect(isNew).toBe(true);
    expect(getBackendSrv().get).toHaveBeenCalledTimes(1);
    expect(getBackendSrv().get).toHaveBeenCalledWith('/api/user');
  });

  it('如果用户是在过去两周之前创建的，则应返回false', async () => {
    const oldUser = {
      id: 2,
      createdAt: dateTime().subtract(USER_CREATION_MIN_DAYS, 'days'),
    };

    getBackendSrv().get = jest.fn().mockResolvedValue(oldUser);

    const isNew = await isNewUser();
    expect(isNew).toBe(false);
    expect(getBackendSrv().get).toHaveBeenCalledTimes(1);
    expect(getBackendSrv().get).toHaveBeenCalledWith('/api/user');
  });
});
