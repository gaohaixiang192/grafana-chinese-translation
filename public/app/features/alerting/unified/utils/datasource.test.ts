import { mockDataSource } from '../mocks';

import { isDataSourceManagingAlerts } from './datasource';

describe('isDataSourceManagingAlerts', () => {
  it('当道具设置为true时应返回true', () => {
    expect(
      isDataSourceManagingAlerts(
        mockDataSource({
          jsonData: {
            manageAlerts: true,
          },
        })
      )
    ).toBe(true);
  });

  it('未定义道具时应返回true', () => {
    expect(
      isDataSourceManagingAlerts(
        mockDataSource({
          jsonData: {},
        })
      )
    ).toBe(true);
  });
});

it('当道具设置为false时应返回false', () => {
  expect(
    isDataSourceManagingAlerts(
      mockDataSource({
        jsonData: {
          manageAlerts: false,
        },
      })
    )
  ).toBe(false);
});
