import { CloudNotifierType, NotificationChannelOption, NotifierDTO } from 'app/types';

function option(
  propertyName: string,
  label: string,
  description: string,
  rest: Partial<NotificationChannelOption> = {}
): NotificationChannelOption {
  return {
    propertyName,
    label,
    description,
    element: 'input',
    inputType: '',
    required: false,
    secure: false,
    placeholder: '',
    validationRule: '',
    showWhen: { field: '', is: '' },
    dependsOn: '',
    ...rest,
  };
}

const basicAuthOption: NotificationChannelOption = option(
  'basic_auth',
  'Basic auth',
  '使用配置的用户名和密码设置“Authorization”标头。密码和密码文件互斥.',
  {
    element: 'subform',
    subformOptions: [
      option('username', 'Username', ''),
      option('password', 'Password', ''),
      option('password_file', 'Password file', ''),
    ],
  }
);

const tlsConfigOption: NotificationChannelOption = option('tls_config', 'TLS config', '配置TLS设置.', {
  element: 'subform',
  subformOptions: [
    option('ca_file', 'CA file', '用于验证服务器证书的CA证书.'),
    option('cert_file', 'Cert file', '用于对服务器进行客户端证书身份验证的证书.'),
    option('key_file', 'Key file', '用于向服务器进行客户端证书身份验证的密钥文件.'),
    option('server_name', 'Server name', 'ServerName扩展名，用于指示服务器的名称.'),
    option('insecure_skip_verify', 'Skip verify', '禁用服务器证书验证.', {
      element: 'checkbox',
    }),
  ],
});

const httpConfigOption: NotificationChannelOption = option(
  'http_config',
  'HTTP Config',
  '请注意，“basic_auth”、“bearer_token”和“bearer_token_file”选项互斥.',
  {
    element: 'subform',
    subformOptions: [
      option('bearer_token', 'Bearer token', '使用配置的承载令牌设置“Authorization”标头.'),
      option(
        'bearer_token_file',
        'Bearer token file',
        '使用从配置文件中读取的承载令牌设置“Authorization”标头.'
      ),
      option('proxy_url', 'Proxy URL', '可选代理URL.'),
      basicAuthOption,
      tlsConfigOption,
    ],
  }
);

export const cloudNotifierTypes: Array<NotifierDTO<CloudNotifierType>> = [
  {
    name: 'Email',
    description: 'Send notification over SMTP',
    type: 'email',
    info: '',
    heading: 'Email settings',
    options: [
      option(
        'to',
        'To',
        '要向其发送通知的电子邮件地址。您可以使用“，”分隔符输入多个地址',
        { required: true }
      ),
      option('from', 'From', '发件人地址.'),
      option('smarthost', 'SMTP host', '发送电子邮件的SMTP主机.'),
      option('hello', 'Hello', '要标识给SMTP服务器的主机名.'),
      option('auth_username', 'Username', 'SMTP身份验证信息'),
      option('auth_password', 'Password', 'SMTP身份验证信息'),
      option('auth_secret', 'Secret', 'SMTP身份验证信息'),
      option('auth_identity', 'Identity', 'SMTP身份验证信息'),
      option('require_tls', 'Require TLS', 'SMTP TLS要求', { element: 'checkbox' }),
      option('html', 'Email HTML body', '电子邮件通知的HTML正文.', {
        placeholder: '{{ template "email.default.html" . }}',
        element: 'textarea',
      }),
      option('text', 'Email text body', '电子邮件通知的文本正文.', { element: 'textarea' }),
      option(
        'headers',
        'Headers',
        '其他邮件头电子邮件头键/值对。覆盖以前由通知实现设置的任何标头.',
        { element: 'key_value_map' }
      ),
      tlsConfigOption,
    ],
  },
  {
    name: 'PagerDuty',
    description: '向PagerDuty发送通知',
    type: 'pagerduty',
    info: '',
    heading: 'PagerDuty settings',
    options: [
      option(
        'routing_key',
        'Routing key',
        'PagerDuty集成键（使用PagerDuy集成类型“Events API v2`”时）'
      ),
      option(
        'service_key',
        'Service key',
        'PagerDuty集成密钥（当使用PagerDuy集成类型“Prometheus”时）.'
      ),
      option('url', 'URL', '发送API请求的URL'),
      option('client', 'Client', '警报管理员的客户标识.', {
        placeholder: '{{ template "pagerduty.default.client" . }}',
      }),
      option('client_url', 'Client URL', '通知发送者的反向链接.', {
        placeholder: '{{ template "pagerduty.default.clientURL" . }}',
      }),
      option('description', 'Description', '事件描述.', {
        placeholder: '{{ template "pagerduty.default.description" .}}',
      }),
      option('severity', 'Severity', '事件的严重性.', { placeholder: 'error' }),
      option(
        'details',
        'Details',
        '一组任意的键/值对，提供有关事件的进一步详细信息.',
        {
          element: 'key_value_map',
        }
      ),
      option('images', 'Images', '要附加到事件的图像.', {
        element: 'subform_array',
        subformOptions: [
          option('href', 'URL', '', { required: true }),
          option('source', 'Source', '', { required: true }),
          option('alt', 'Alt', '', { required: true }),
        ],
      }),
      option('links', 'Links', '要附加到事件的链接.', {
        element: 'subform_array',
        subformOptions: [option('href', 'URL', '', { required: true }), option('text', 'Text', '', { required: true })],
      }),
      httpConfigOption,
    ],
  },
  {
    name: 'Pushover',
    description: '向Pushover发送通知',
    type: 'pushover',
    info: '',
    heading: 'Pushover settings',
    options: [
      option('user_key', 'User key', '收件人用户的用户密钥.', { required: true }),
      option('token', 'Token', '您注册的应用程序的API令牌, see https://pushover.net/app', {
        required: true,
      }),
      option('title', 'Title', '通知标题.', {
        placeholder: '{{ template "pushover.default.title" . }}',
      }),
      option('message', 'Message', '通知消息.', {
        placeholder: '{{ template "pushover.default.message" . }}',
      }),
      option('url', 'URL', '消息旁边显示的补充URL.', {
        placeholder: '{{ template "pushover.default.url" . }}',
      }),
      option('priority', 'Priority', 'Priority, see https://pushover.net/api#priority', {
        placeholder: '{{ if eq .Status "firing" }}2{{ else }}0{{ end }}',
      }),
      option(
        'retry',
        'Retry',
        'Pushover服务器向用户发送相同通知的频率。必须至少为30秒.',
        {
          placeholder: '1m',
        }
      ),
      option(
        'expire',
        'Expire',
        '除非用户确认通知，否则将继续重试您的通知多长时间.',
        {
          placeholder: '1h',
        }
      ),
      httpConfigOption,
    ],
  },
  {
    name: 'Slack',
    description: 'Send notifications to Slack',
    type: 'slack',
    info: '',
    heading: 'Slack settings',
    options: [
      option('api_url', 'Webhook URL', 'Slack网络挂钩URL.'),
      option('channel', 'Channel', '要向其发送通知的#频道或@用户.', { required: true }),
      option('icon_emoji', 'Emoji icon', ''),
      option('icon_url', 'Icon URL', ''),
      option('link_names', 'Names link', '', { element: 'checkbox' }),
      option('username', 'Username', '', { placeholder: '{{ template "slack.default.username" . }}' }),
      option('callback_id', 'Callback ID', '', { placeholder: '{{ template "slack.default.callbackid" . }}' }),
      option('color', 'Color', '', { placeholder: '{{ if eq .Status "firing" }}danger{{ else }}good{{ end }}' }),
      option('fallback', 'Fallback', '', { placeholder: '{{ template "slack.default.fallback" . }}' }),
      option('footer', 'Footer', '', { placeholder: '{{ template "slack.default.footer" . }}' }),
      option('mrkdwn_in', 'Mrkdwn fields', '应使用mrkdwn语法格式化的字段名数组.', {
        element: 'string_array',
      }),
      option('pretext', 'Pre-text', '', { placeholder: '{{ template "slack.default.pretext" . }}' }),
      option('short_fields', 'Short fields', '', { element: 'checkbox' }),
      option('text', 'Message body', '', { element: 'textarea', placeholder: '{{ template "slack.default.text" . }}' }),
      option('title', 'Title', '', { placeholder: '{{ template "slack.default.title" . }}' }),
      option('title_link', 'Title link', '', { placeholder: '{{ template "slack.default.titlelink" . }}' }),
      option('image_url', 'Image URL', ''),
      option('thumb_url', 'Thumbnail URL', ''),
      option('actions', 'Actions', '', {
        element: 'subform_array',
        subformOptions: [
          option('text', 'Text', '', { required: true }),
          option('type', 'Type', '', { required: true }),
          option('url', 'URL', 'url或名称和值都是必需的.'),
          option('name', 'Name', ''),
          option('value', 'Value', ''),
          option('confirm', 'Confirm', '', {
            element: 'subform',
            subformOptions: [
              option('text', 'Text', '', { required: true }),
              option('dismiss_text', 'Dismiss text', ''),
              option('ok_text', 'OK text', ''),
              option('title', 'Title', ''),
            ],
          }),
          option('style', 'Style', ''),
        ],
      }),
      option('fields', 'Fields', '', {
        element: 'subform_array',
        subformOptions: [
          option('title', 'Title', '', { required: true }),
          option('value', 'Value', '', { required: true }),
          option('short', 'Short', '', { element: 'checkbox' }),
        ],
      }),
      httpConfigOption,
    ],
  },
  {
    name: 'OpsGenie',
    description: 'Send notifications to OpsGenie',
    type: 'opsgenie',
    info: '',
    heading: 'OpsGenie settings',
    options: [
      option('api_key', 'API key', '与OpsGenie API对话时使用的API密钥.'),
      option('api_url', 'API URL', '向其发送OpsGenie API请求的主机.'),
      option('message', 'Message', '警报文本限制为130个字符.'),
      option('description', 'Description', '事件描述.', {
        placeholder: '{{ template "opsgenie.default.description" . }}',
      }),
      option('source', 'Source', '通知发送者的反向链接.', {
        placeholder: '{{ template "opsgenie.default.source" . }}',
      }),
      option(
        'details',
        'Details',
        '一组任意的键/值对，提供有关事件的进一步详细信息.',
        {
          element: 'key_value_map',
        }
      ),
      option('tags', 'Tags', '附加到通知的逗号分隔的标记列表.'),
      option('note', 'Note', '附加警告说明.'),
      option('priority', 'Priority', '警报的优先级。可能的值为 P1, P2, P3, P4, and P5.'),
      option('responders', 'Responders', '负责通知的响应者名单.', {
        element: 'subform_array',
        subformOptions: [
          option('type', 'Type', '"team", "user", "escalation" or schedule".', { required: true }),
          option('id', 'ID', '应该只定义其中一个字段.'),
          option('name', 'Name', '应该只定义其中一个字段.'),
          option('username', 'Username', '应该只定义其中一个字段.'),
        ],
      }),
      httpConfigOption,
    ],
  },
  {
    name: 'VictorOps',
    description: 'Send notifications to VictorOps',
    type: 'victorops',
    info: '',
    heading: 'VictorOps settings',
    options: [
      option('api_key', 'API key', '与VictorOps API对话时使用的API密钥.'),
      option('api_url', 'API URL', 'VictorOps API URL.'),
      option('routing_key', 'Routing key', '用于将警报映射到团队的密钥.', { required: true }),
      option('message_type', 'Message type', '描述警报的行为（关键、警告、信息）.'),
      option('entity_display_name', 'Entity display name', '包含警报问题的摘要.', {
        placeholder: '{{ template "victorops.default.entity_display_name" . }}',
      }),
      option('state_message', 'State message', '包含警报问题的详细说明.', {
        placeholder: '{{ template "victorops.default.state_message" . }}',
      }),
      option('monitoring_tool', 'Monitoring tool', '状态消息来自的监控工具.', {
        placeholder: '{{ template "victorops.default.monitoring_tool" . }}',
      }),
      httpConfigOption,
    ],
  },
  {
    name: 'Webhook',
    description: '向网络挂钩发送通知',
    type: 'webhook',
    info: '',
    heading: 'Webhook settings',
    options: [
      option('url', 'URL', '要向其发送HTTP POST请求的端点.', { required: true }),
      option(
        'max_alerts',
        'Max alerts',
        '要包含在单个webhook消息中的最大警报数。超过此阈值的警报将被截断。将其保留为默认值0时，将包括所有警报.',
        { placeholder: '0', validationRule: '(^\\d+$|^$)' }
      ),
      httpConfigOption,
    ],
  },
  {
    name: 'Discord',
    description: '向Discord发送通知',
    type: 'discord',
    info: '',
    heading: 'Discord settings',
    options: [
      option('title', 'Title', '模板配置消息的模板标题', {
        placeholder: '{{ template "discord.default.title" . }}',
      }),
      option(
        'message',
        'Message Content',
        '在频道中通知时提及使用@的组或使用<@ID>的用户',
        { placeholder: '{{ template "discord.default.message" . }}' }
      ),
      option('webhook_url', 'Webhook URL', '', { placeholder: 'Discord webhook URL', required: true }),
      httpConfigOption,
    ],
  },
  {
    name: 'Cisco Webex Teams',
    description: '向Cisco Webex Teams发送通知',
    type: 'webex',
    info: '',
    heading: 'Cisco Webex Teams settings',
    options: [
      option('api_url', 'API URL', 'Webex团队API URL', {
        placeholder: 'https://webexapis.com/v1/messages',
      }),
      option('room_id', 'Room ID', '发送消息的Webex Teams会议室的ID', {
        required: true,
      }),
      option('message', 'Message', '消息模板', {
        placeholder: '{{ template "webex.default.message" .}}',
      }),
      {
        ...httpConfigOption,
        required: true,
      },
    ],
  },
  {
    name: 'Telegram',
    description: '向Telegram发送通知',
    type: 'telegram',
    info: '',
    heading: 'Telegram settings',
    options: [
      option('api_url', 'API URL', '电报API URL', {
        placeholder: 'https://api.telegram.org',
      }),
      option('bot_token', 'Bot token', 'Telegram机器人令牌', {
        required: true,
      }),
      option('chat_id', 'Chat ID', '发送消息的聊天室ID', {
        required: true,
        setValueAs: (value) => (typeof value === 'string' ? parseInt(value, 10) : 0),
      }),
      option('message', 'Message', '消息模板', {
        placeholder: '{{ template "webex.default.message" .}}',
      }),
      option('disable_notifications', 'Disable notifications', '禁用电报通知', {
        element: 'checkbox',
      }),
      option('parse_mode', 'Parse mode', '电报消息的解析模式', {
        element: 'select',
        defaultValue: { label: 'MarkdownV2', value: 'MarkdownV2' },
        selectOptions: [
          { label: 'MarkdownV2', value: 'MarkdownV2' },
          { label: 'Markdown', value: 'Markdown' },
          { label: 'HTML', value: 'HTML' },
          { label: 'plain text', value: '' },
        ],
      }),
      httpConfigOption,
    ],
  },
  {
    name: 'Amazon SNS',
    description: '向亚马逊社交网络发送通知',
    type: 'sns',
    info: '',
    heading: 'Amazon SNS settings',
    options: [
      option('api_url', 'API URL', '亚马逊SNS API URL'),
      option(
        'sigv4',
        'SigV4 authentication',
        "配置AWS的Signature Verification 4签名过程以签署请求",
        {
          element: 'subform',
          subformOptions: [
            option(
              'region',
              'Region',
              'AWS地区。如果为空，则使用默认凭据链中的区域'
            ),
            option(
              'access_key',
              'Access key',
              'AWS API access_key。如果为空，则使用环境变量“AWS_ACCESS_KEY_ID”'
            ),
            option(
              'secret_key',
              'Secret key',
              'AWS API secret_key。如果为空，则使用环境变量“AWS_ACCESS_SECRET_ID”'
            ),
            option('profile', 'Profile', '用于身份验证的命名AWS配置文件'),
            option('role_arn', 'Rule ARN', 'AWS角色ARN，替代使用AWS API密钥'),
          ],
        }
      ),
      option(
        'topic_arn',
        'SNS topic ARN',
        "如果未指定此值，则必须为phone_number或target_arn指定一个值。如果使用FIFO SNS主题，则应将消息组间隔设置为大于5分钟，以防止具有相同组密钥的消息被SNS默认重复数据消除窗口进行重复数据消除"
      ),
      option(
        'phone_number',
        'Phone number',
        "如果信息通过E.164格式的短信发送，则为电话号码。如果未指定此值，则必须为topic_arn或target_arn指定一个值"
      ),
      option(
        'target_arn',
        'Target ARN',
        "如果消息是通过移动通知传递的，则移动平台端点ARN。如果未指定此值，则必须为topic_arn或phone_number指定一个值"
      ),

      option('subject', 'Subject', '邮件传递到电子邮件端点时的主题行', {
        placeholder: '{{ template "sns.default.subject" .}}',
      }),
      option('message', 'Message', 'SNS通知的消息内容', {
        placeholder: '{{ template "sns.default.message" .}}',
      }),
      option('attributes', 'Attributes', 'SNS消息属性', {
        element: 'key_value_map',
      }),
      httpConfigOption,
    ],
  },
  {
    name: 'WeChat',
    description: '向微信发送通知',
    type: 'wechat',
    info: '',
    heading: 'WeChat settings',
    options: [
      option('api_url', 'API URL', '微信API URL'),
      option('api_secret', 'API Secret', '与微信API对话时使用的API密钥'),
      option('corp_id', 'Corp ID', '用于身份验证的公司id'),
      option('message', 'Message', '微信API定义的API请求数据', {
        placeholder: '{{ template "wechat.default.message" . }}',
      }),
      option('message_type', 'Message type', '消息类型的类型', {
        element: 'select',
        defaultValue: { label: 'Text', value: 'text' },
        selectOptions: [
          { label: 'Text', value: 'text' },
          { label: 'Markdown', value: 'markdown' },
        ],
      }),
      option('agent_id', 'Agent ID', '', {
        placeholder: '{{ template "wechat.default.agent_id" . }}',
      }),
      option('to_user', 'to user', '', {
        placeholder: '{{ template "wechat.default.to_user" . }}',
      }),
      option('to_party', 'to party', '', {
        placeholder: '{{ template "wechat.default.to_party" . }}',
      }),
      option('to_tag', 'to tag', '', {
        placeholder: '{{ template "wechat.default.to_tag" . }}',
      }),
    ],
  },
];

export const globalConfigOptions: NotificationChannelOption[] = [
  // email
  option('smtp_from', 'SMTP from', '默认的SMTP发件人标头字段.'),
  option(
    'smtp_smarthost',
    'SMTP smarthost',
    '用于发送电子邮件的默认SMTP智能主机，包括端口号。端口号通常为25，对于TLS上的SMTP（有时称为STARTTLS）为587。示例：smtp.Example.org:587'
  ),
  option('smtp_hello', 'SMTP hello', '要标识给SMTP服务器的默认主机名.', {
    placeholder: 'localhost',
  }),
  option(
    'smtp_auth_username',
    'SMTP auth username',
    "使用CRAM-MD5、LOGIN和PLAIN的SMTP身份验证。如果为空，则Alertmanager不会对SMTP服务器进行身份验证."
  ),
  option('smtp_auth_password', 'SMTP auth password', '使用LOGIN和PLAIN的SMTP身份验证.'),
  option('smtp_auth_identity', 'SMTP auth identity', '使用PLAIN进行SMTP身份验证.'),
  option('smtp_auth_secret', 'SMTP auth secret', '使用CRAM-MD5的SMTP身份验证.'),
  option(
    'smtp_require_tls',
    'SMTP require TLS',
    '默认的SMTP TLS要求。请注意，Go不支持到远程SMTP终结点的未加密连接.',
    {
      element: 'checkbox',
    }
  ),

  // slack
  option('slack_api_url', 'Slack API URL', ''),
  option('victorops_api_key', 'VictorOps API key', ''),
  option('victorops_api_url', 'VictorOps API URL', '', {
    placeholder: 'https://alert.victorops.com/integrations/generic/20131114/alert/',
  }),
  option('pagerduty_url', 'PagerDuty URL', 'https://events.pagerduty.com/v2/enqueue'),
  option('opsgenie_api_key', 'OpsGenie API key', ''),
  option('opsgenie_api_url', 'OpsGenie API URL', '', { placeholder: 'https://api.opsgenie.com/' }),
  option('wechat_api_url', 'WeChat API URL', '', { placeholder: 'https://qyapi.weixin.qq.com/cgi-bin/' }),
  option('wechat_api_secret', 'WeChat API secret', ''),
  option('wechat_api_corp_id', 'WeChat API corp id', ''),
  option('webex_api_url', 'Cisco Webex Teams API URL', ''),
  option('telegram_api_url', 'The Telegram API URL', ''),
  httpConfigOption,
  option(
    'resolve_timeout',
    'Resolve timeout',
    '如果警报不包括EndsAt，则ResolveTimeout是alertmanager使用的默认值。经过此时间后，如果警报尚未更新，则可以将其声明为已解决。这对普罗米修斯的警报没有影响，因为它们总是包括EndsAt.',
    {
      placeholder: '5m',
    }
  ),
];
