import React from 'react';

import { config } from '@grafana/runtime';
import { Alert } from '@grafana/ui';

const EvaluationIntervalLimitExceeded = () => (
  <Alert severity="warning" title="超出全局评估间隔限制">
    最小评估间隔为 <strong>{config.unifiedAlerting.minInterval}</strong> 已在Grafana中配置.
    <br />
    请与管理员联系以配置较低的间隔.
  </Alert>
);

export { EvaluationIntervalLimitExceeded };
