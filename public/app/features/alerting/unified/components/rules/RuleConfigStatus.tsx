import { css } from '@emotion/css';
import React, { useMemo } from 'react';

import { GrafanaTheme2 } from '@grafana/data/src';
import { config } from '@grafana/runtime/src';
import { Icon, Tooltip, useStyles2 } from '@grafana/ui/src';

import { CombinedRule } from '../../../../../types/unified-alerting';
import { checkEvaluationIntervalGlobalLimit } from '../../utils/config';

interface RuleConfigStatusProps {
  rule: CombinedRule;
}

export function RuleConfigStatus({ rule }: RuleConfigStatusProps) {
  const styles = useStyles2(getStyles);

  const { exceedsLimit } = useMemo(
    () => checkEvaluationIntervalGlobalLimit(rule.group.interval),
    [rule.group.interval]
  );

  if (!exceedsLimit) {
    return null;
  }

  return (
    <Tooltip
      theme="error"
      content={
        <div>
          最小评估间隔为{' '}
          <span className={styles.globalLimitValue}>{config.unifiedAlerting.minInterval}</span> 已在中配置
          Grafana和将被使用，而不是 {rule.group.interval} 为规则组配置的间隔.
        </div>
      }
    >
      <Icon name="stopwatch-slash" className={styles.icon} />
    </Tooltip>
  );
}

function getStyles(theme: GrafanaTheme2) {
  return {
    globalLimitValue: css`
      font-weight: ${theme.typography.fontWeightBold};
    `,
    icon: css`
      fill: ${theme.colors.warning.text};
    `,
  };
}
