export interface TemplateDataItem {
  name: string;
  type: 'string' | '[]Alert' | 'KeyValue' | 'time.Time';
  notes: string;
}

interface TemplateFunctionItem {
  name: string;
  args?: '[]string';
  returns: 'KeyValue' | '[]string';
  notes?: string;
}

export const GlobalTemplateData: TemplateDataItem[] = [
  {
    name: 'Receiver',
    type: 'string',
    notes: '将通知发送到的联系人的名称.',
  },
  {
    name: 'Status',
    type: 'string',
    notes: '如果至少有一个警报正在启动，则启动，否则已解决',
  },
  {
    name: 'Alerts',
    type: '[]Alert',
    notes: '此通知中包含的警报对象的列表.',
  },
  {
    name: 'Alerts.Firing',
    type: '[]Alert',
    notes: '火灾警报列表',
  },
  {
    name: 'Alerts.Resolved',
    type: '[]Alert',
    notes: '已解决警报列表',
  },
  {
    name: 'GroupLabels',
    type: 'KeyValue',
    notes: '这些警报分组依据的标签.',
  },
  {
    name: 'CommonLabels',
    type: 'KeyValue',
    notes: '此通知中包含的所有警报的公用标签.',
  },
  {
    name: 'CommonAnnotations',
    type: 'KeyValue',
    notes: '此通知中包含的所有警报的公用注释.',
  },
  {
    name: 'ExternalURL',
    type: 'string',
    notes: '发送通知的Grafana的返回链接.',
  },
];

export const AlertTemplatePreviewData: TemplateDataItem[] = [
  {
    name: 'Labels',
    type: 'KeyValue',
    notes: '附加到警报的标签集.',
  },
  {
    name: 'Annotations',
    type: 'KeyValue',
    notes: '附加到警报的注释集.',
  },
  {
    name: 'StartsAt',
    type: 'time.Time',
    notes: '警报开始启动的时间.',
  },
  {
    name: 'EndsAt',
    type: 'time.Time',
    notes: '警报结束的时间.',
  },
];

export const AlertTemplateData: TemplateDataItem[] = [
  {
    name: 'Status',
    type: 'string',
    notes: 'firing or resolved.',
  },
  {
    name: 'Labels',
    type: 'KeyValue',
    notes: '附加到警报的标签集.',
  },
  {
    name: 'Annotations',
    type: 'KeyValue',
    notes: '附加到警报的注释集.',
  },
  {
    name: 'Values',
    type: 'KeyValue',
    notes:
      '所有即时查询、reduce和数学表达式的值，以及警报的经典条件。它不包含时间序列数据.',
  },
  {
    name: 'StartsAt',
    type: 'time.Time',
    notes: '警报开始启动的时间.',
  },
  {
    name: 'EndsAt',
    type: 'time.Time',
    notes:
      '仅当警报的结束时间已知时设置。否则，设置为自上次收到警报以来的可配置超时时间.',
  },
  {
    name: 'GeneratorURL',
    type: 'string',
    notes: 'Grafana或外部Alertmanager的反向链接.',
  },
  {
    name: 'SilenceURL',
    type: 'string',
    notes: '链接到的Grafana静默，该警报的标签已预先填充。仅适用于Grafana管理的警报.',
  },
  {
    name: 'DashboardURL',
    type: 'string',
    notes: '如果警报规则属于Grafana仪表板，则链接到该仪表板。仅适用于Grafana管理的警报.',
  },
  {
    name: 'PanelURL',
    type: 'string',
    notes: '链接到Grafana仪表板面板，如果警报规则属于其中一个。仅适用于Grafana管理的警报.',
  },
  {
    name: 'Fingerprint',
    type: 'string',
    notes: '可用于识别警报的指纹.',
  },
  {
    name: 'ValueString',
    type: 'string',
    notes: '包含警报中每个精简表达式的标签和值的字符串.',
  },
];

export const KeyValueTemplateFunctions: TemplateFunctionItem[] = [
  {
    name: 'SortedPairs',
    returns: 'KeyValue',
    notes: '返回键值字符串对的排序列表',
  },
  {
    name: 'Remove',
    args: '[]string',
    returns: 'KeyValue',
    notes: '返回没有给定键的键/值映射的副本.',
  },
  {
    name: 'Names',
    returns: '[]string',
    notes: '标签名称列表',
  },
  {
    name: 'Values',
    returns: '[]string',
    notes: '标签值列表',
  },
];

export const KeyValueCodeSnippet = `{
  "summary": "alert summary",
  "description": "警报说明"
}
`;
