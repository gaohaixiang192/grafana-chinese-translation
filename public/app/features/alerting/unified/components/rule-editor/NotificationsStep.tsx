import { css } from '@emotion/css';
import React from 'react';
import { useFormContext } from 'react-hook-form';

import { GrafanaTheme2 } from '@grafana/data';
import { Card, Link, useStyles2 } from '@grafana/ui';

import { RuleFormType, RuleFormValues } from '../../types/rule-form';
import { GRAFANA_RULES_SOURCE_NAME } from '../../utils/datasource';

import LabelsField from './LabelsField';
import { RuleEditorSection } from './RuleEditorSection';

export const NotificationsStep = () => {
  const styles = useStyles2(getStyles);
  const { watch, getValues } = useFormContext<RuleFormValues & { location?: string }>();

  const type = watch('type');

  const dataSourceName = watch('dataSourceName') ?? GRAFANA_RULES_SOURCE_NAME;
  const hasLabelsDefined = getNonEmptyLabels(getValues('labels')).length > 0;

  return (
    <RuleEditorSection
      stepNo={type === RuleFormType.cloudRecording ? 4 : 5}
      title={type === RuleFormType.cloudRecording ? 'Labels' : 'Notifications'}
      description={
        type === RuleFormType.cloudRecording
          ? '添加标签以帮助您更好地管理录制规则'
          : 'Grafana通过为警报分配标签来处理警报的通知。这些标签将警报连接到联系人，并使具有匹配标签的警报实例静音.'
      }
    >
      <div className={styles.contentWrapper}>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          {!hasLabelsDefined && type !== RuleFormType.cloudRecording && (
            <Card className={styles.card}>
              <Card.Heading>根路由–所有警报的默认值</Card.Heading>
              <Card.Description>
                如果没有自定义标签，您的警报将通过根路由进行路由。查看和编辑根
                路由，请转到<Link href="/alering/routes">通知策略</Link>或联系您的管理员以备不时之需
              您正在使用非Grafana警报管理.
              </Card.Description>
            </Card>
          )}
          <LabelsField dataSourceName={dataSourceName} />
        </div>
      </div>
    </RuleEditorSection>
  );
};

interface Label {
  key: string;
  value: string;
}

function getNonEmptyLabels(labels: Label[]) {
  return labels.filter((label) => label.key && label.value);
}

const getStyles = (theme: GrafanaTheme2) => ({
  contentWrapper: css`
    display: flex;
    align-items: center;
  `,
  hideButton: css`
    color: ${theme.colors.text.secondary};
    cursor: pointer;
    margin-bottom: ${theme.spacing(1)};
  `,
  card: css`
    max-width: 500px;
  `,
  flowChart: css`
    margin-right: ${theme.spacing(3)};
  `,
});
