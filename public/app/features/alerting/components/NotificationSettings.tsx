import React from 'react';

import { Checkbox, CollapsableSection, Field, InfoBox, Input } from '@grafana/ui';

import { NotificationSettingsProps } from './NotificationChannelForm';

interface Props extends NotificationSettingsProps {
  imageRendererAvailable: boolean;
}

export const NotificationSettings = ({ currentFormValues, imageRendererAvailable, register }: Props) => {
  return (
    <CollapsableSection label="Notification settings" isOpen={false}>
      <Field>
        <Checkbox {...register('isDefault')} label="Default" description="将此通知用于所有警报" />
      </Field>
      <Field>
        <Checkbox
          {...register('settings.uploadImage')}
          label="Include image"
          description="捕获图像并将其包含在通知中"
        />
      </Field>
      {currentFormValues.uploadImage && !imageRendererAvailable && (
        <InfoBox title="没有可用/安装的图像渲染器">
          Grafana cannot find an image renderer to capture an image for the notification. Please make sure the Grafana
          Image Renderer plugin is installed. Please contact your Grafana administrator to install the plugin.
        </InfoBox>
      )}
      <Field>
        <Checkbox
          {...register('disableResolveMessage')}
          label="禁用解析消息"
          description="禁用警报状态返回false时发送的解析消息[OK]"
        />
      </Field>
      <Field>
        <Checkbox
          {...register('sendReminder')}
          label="发送提醒"
          description="为触发的警报发送其他通知"
        />
      </Field>
      {currentFormValues.sendReminder && (
        <>
          <Field
            label="每隔发送提醒"
            description="指定提醒的发送频率，例如每30秒、1米、10米、30米或1小时等。
                        在评估规则后发送警报提醒。提醒的发送频率再高不过了
                        而不是配置的警报规则评估间隔。"
          >
            <Input {...register('frequency')} width={8} />
          </Field>
        </>
      )}
    </CollapsableSection>
  );
};
