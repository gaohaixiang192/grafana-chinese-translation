import {
  DataSourceApi,
  PluginMeta,
  DataTransformerConfig,
  DataSourceInstanceSettings,
  DataSourceRef,
} from '@grafana/data';
import { DataSourceSrv } from '@grafana/runtime';

import { ElasticsearchQuery } from '../../plugins/datasource/elasticsearch/types';

import { getAlertingValidationMessage } from './getAlertingValidationMessage';

describe('getAlertingValidationMessage', () => {
  describe('当与包含模板变量的某些目标一起调用时', () => {
    it('那么它应该返回false', async () => {
      let call = 0;
      const datasource: DataSourceApi = {
        meta: { alerting: true } as unknown as PluginMeta,
        targetContainsTemplate: () => {
          if (call === 0) {
            call++;
            return true;
          }
          return false;
        },
        name: 'some name',
        uid: 'some uid',
      } as unknown as DataSourceApi;
      const getMock = jest.fn().mockResolvedValue(datasource);
      const datasourceSrv: DataSourceSrv = {
        get: (ref: DataSourceRef) => {
          return getMock(ref.uid);
        },
        getList(): DataSourceInstanceSettings[] {
          return [];
        },
        getInstanceSettings: jest.fn(),
        reload: jest.fn(),
      };
      const targets: ElasticsearchQuery[] = [
        { refId: 'A', query: '@hostname:$hostname' },
        { refId: 'B', query: '@instance:instance' },
      ];
      const transformations: DataTransformerConfig[] = [];

      const result = await getAlertingValidationMessage(transformations, targets, datasourceSrv, {
        uid: datasource.uid,
      });

      expect(result).toBe('');
      expect(getMock).toHaveBeenCalledTimes(2);
      expect(getMock).toHaveBeenCalledWith(datasource.uid);
    });
  });

  describe('当使用不支持警报的数据源对某些目标进行调用时', () => {
    it('then it should return false', async () => {
      const alertingDatasource: DataSourceApi = {
        meta: { alerting: true } as unknown as PluginMeta,
        targetContainsTemplate: () => false,
        name: 'alertingDatasource',
      } as unknown as DataSourceApi;
      const datasource: DataSourceApi = {
        meta: { alerting: false } as unknown as PluginMeta,
        targetContainsTemplate: () => false,
        name: 'datasource',
      } as unknown as DataSourceApi;

      const datasourceSrv: DataSourceSrv = {
        get: (name: string) => {
          if (name === datasource.name) {
            return Promise.resolve(datasource);
          }

          return Promise.resolve(alertingDatasource);
        },
        getInstanceSettings: jest.fn(),
        getList(): DataSourceInstanceSettings[] {
          return [];
        },
        reload: jest.fn(),
      };
      const targets: any[] = [
        { refId: 'A', query: 'some query', datasource: 'alertingDatasource' },
        { refId: 'B', query: 'some query', datasource: 'datasource' },
      ];
      const transformations: DataTransformerConfig[] = [];

      const result = await getAlertingValidationMessage(transformations, targets, datasourceSrv, {
        uid: datasource.name,
      });

      expect(result).toBe('');
    });
  });

  describe('当与包含模板变量的所有目标一起调用时', () => {
    it('那么它应该返回false', async () => {
      const datasource: DataSourceApi = {
        meta: { alerting: true } as unknown as PluginMeta,
        targetContainsTemplate: () => true,
        name: 'some name',
      } as unknown as DataSourceApi;
      const getMock = jest.fn().mockResolvedValue(datasource);
      const datasourceSrv: DataSourceSrv = {
        get: (ref: DataSourceRef) => {
          return getMock(ref.uid);
        },
        getInstanceSettings: jest.fn(),
        getList(): DataSourceInstanceSettings[] {
          return [];
        },
        reload: jest.fn(),
      };
      const targets: ElasticsearchQuery[] = [
        { refId: 'A', query: '@hostname:$hostname' },
        { refId: 'B', query: '@instance:$instance' },
      ];
      const transformations: DataTransformerConfig[] = [];

      const result = await getAlertingValidationMessage(transformations, targets, datasourceSrv, {
        uid: datasource.name,
      });

      expect(result).toBe('警报查询中不支持模板变量');
      expect(getMock).toHaveBeenCalledTimes(2);
      expect(getMock).toHaveBeenCalledWith(datasource.name);
    });
  });

  describe('当使用不支持警报的数据源对所有目标调用时', () => {
    it('那么它应该返回false', async () => {
      const datasource: DataSourceApi = {
        meta: { alerting: false } as unknown as PluginMeta,
        targetContainsTemplate: () => false,
        name: 'some name',
        uid: 'theid',
      } as unknown as DataSourceApi;
      const getMock = jest.fn().mockResolvedValue(datasource);
      const datasourceSrv: DataSourceSrv = {
        get: (ref: DataSourceRef) => {
          return getMock(ref.uid);
        },
        getInstanceSettings: jest.fn(),
        getList(): DataSourceInstanceSettings[] {
          return [];
        },
        reload: jest.fn(),
      };
      const targets: ElasticsearchQuery[] = [
        { refId: 'A', query: '@hostname:hostname' },
        { refId: 'B', query: '@instance:instance' },
      ];
      const transformations: DataTransformerConfig[] = [];

      const result = await getAlertingValidationMessage(transformations, targets, datasourceSrv, {
        uid: datasource.uid,
      });

      expect(result).toBe('数据源不支持报警查询');
      expect(getMock).toHaveBeenCalledTimes(2);
      expect(getMock).toHaveBeenCalledWith(datasource.uid);
    });
  });

  describe('使用转换调用时', () => {
    it('那么它应该返回false', async () => {
      const datasource: DataSourceApi = {
        meta: { alerting: true } as unknown as PluginMeta,
        targetContainsTemplate: () => false,
        name: 'some name',
      } as unknown as DataSourceApi;
      const getMock = jest.fn().mockResolvedValue(datasource);
      const datasourceSrv: DataSourceSrv = {
        get: (ref: DataSourceRef) => {
          return getMock(ref.uid);
        },
        getInstanceSettings: jest.fn(),
        getList(): DataSourceInstanceSettings[] {
          return [];
        },
        reload: jest.fn(),
      };
      const targets: ElasticsearchQuery[] = [
        { refId: 'A', query: '@hostname:hostname' },
        { refId: 'B', query: '@instance:instance' },
      ];
      const transformations: DataTransformerConfig[] = [{ id: 'A', options: null }];

      const result = await getAlertingValidationMessage(transformations, targets, datasourceSrv, {
        uid: datasource.uid,
      });

      expect(result).toBe('警报查询中不支持转换');
      expect(getMock).toHaveBeenCalledTimes(0);
    });
  });
});
