import { PanelModel } from 'app/features/dashboard/state';

import { hiddenReducerTypes, ThresholdMapper } from './ThresholdMapper';
import alertDef from './alertDef';

const visibleReducerTypes = alertDef.reducerTypes
  .filter(({ value }) => hiddenReducerTypes.indexOf(value) === -1)
  .map(({ value }) => value);

describe('ThresholdMapper', () => {
  describe('具有大于评估者', () => {
    it('可以将查询条件映射到阈值', () => {
      const panel = {
        type: 'graph',
        options: { alertThresholds: true },
        alert: {
          conditions: [
            {
              type: 'query',
              evaluator: { type: 'gt', params: [100] },
            },
          ],
        },
      } as unknown as PanelModel;

      const updated = ThresholdMapper.alertToGraphThresholds(panel);
      expect(updated).toBe(true);
      expect(panel.thresholds[0].op).toBe('gt');
      expect(panel.thresholds[0].value).toBe(100);
    });
  });

  describe('带范围外评估器', () => {
    it('可以将查询条件映射到阈值', () => {
      const panel = {
        type: 'graph',
        options: { alertThresholds: true },
        alert: {
          conditions: [
            {
              type: 'query',
              evaluator: { type: 'outside_range', params: [100, 200] },
            },
          ],
        },
      } as unknown as PanelModel;

      const updated = ThresholdMapper.alertToGraphThresholds(panel);
      expect(updated).toBe(true);
      expect(panel.thresholds[0].op).toBe('lt');
      expect(panel.thresholds[0].value).toBe(100);

      expect(panel.thresholds[1].op).toBe('gt');
      expect(panel.thresholds[1].value).toBe(200);
    });
  });

  describe('带范围内评估器', () => {
    it('可以将查询条件映射到阈值, () => {
      const panel = {
        type: 'graph',
        options: { alertThresholds: true },
        alert: {
          conditions: [
            {
              type: 'query',
              evaluator: { type: 'within_range', params: [100, 200] },
            },
          ],
        },
      } as unknown as PanelModel;

      const updated = ThresholdMapper.alertToGraphThresholds(panel);
      expect(updated).toBe(true);
      expect(panel.thresholds[0].op).toBe('gt');
      expect(panel.thresholds[0].value).toBe(100);

      expect(panel.thresholds[1].op).toBe('lt');
      expect(panel.thresholds[1].value).toBe(200);
    });
  });

  visibleReducerTypes.forEach((type) => {
    describe(`with {${type}} reducer`, () => {
      it('可见应为真', () => {
        const panel = getPanel({ reducerType: type });

        const updated = ThresholdMapper.alertToGraphThresholds(panel);

        expect(updated).toBe(true);
        expect(panel.thresholds[0]).toEqual({
          value: 100,
          op: 'gt',
          fill: true,
          line: true,
          colorMode: 'critical',
          visible: true,
        });
      });
    });
  });

  hiddenReducerTypes.forEach((type) => {
    describe(`with {${type}} reducer`, () => {
      it('可见应为假', () => {
        const panel = getPanel({ reducerType: type });

        const updated = ThresholdMapper.alertToGraphThresholds(panel);

        expect(updated).toBe(true);
        expect(panel.thresholds[0]).toEqual({
          value: 100,
          op: 'gt',
          fill: true,
          line: true,
          colorMode: 'critical',
          visible: false,
        });
      });
    });
  });
});

function getPanel({ reducerType }: { reducerType?: string } = {}) {
  const panel = {
    type: 'graph',
    options: { alertThreshold: true },
    alert: {
      conditions: [
        {
          type: 'query',
          evaluator: { type: 'gt', params: [100] },
          reducer: { type: reducerType },
        },
      ],
    },
  } as unknown as PanelModel;

  return panel;
}
