import { AlertRulesState, StoreState } from 'app/types';

import { getSearchQuery, getAlertRuleItems } from './selectors';

describe('获取搜索查询', () => {
  it('应获取搜索查询', () => {
    const state: AlertRulesState = { searchQuery: 'dashboard', items: [], isLoading: false };
    const result = getSearchQuery(state);

    expect(result).toEqual(state.searchQuery);
  });
});

describe('获取警报规则项', () => {
  it('应获取警报规则项', () => {
    const state = {
      alertRules: {
        items: [
          {
            id: 1,
            dashboardId: 1,
            panelId: 1,
            name: '',
            state: '',
            stateText: '',
            stateIcon: '',
            stateClass: '',
            stateAge: '',
            url: '',
          },
        ],
        searchQuery: '',
      },
    } as unknown as StoreState;

    const result = getAlertRuleItems(state);
    expect(result.length).toEqual(1);
  });

  it('应根据搜索查询筛选规则项', () => {
    const state = {
      alertRules: {
        items: [
          {
            id: 1,
            dashboardId: 1,
            panelId: 1,
            name: 'dashboard',
            state: '',
            stateText: '',
            stateIcon: '',
            stateClass: '',
            stateAge: '',
            url: '',
          },
          {
            id: 2,
            dashboardId: 3,
            panelId: 1,
            name: 'dashboard2',
            state: '',
            stateText: '',
            stateIcon: '',
            stateClass: '',
            stateAge: '',
            url: '',
          },
          {
            id: 3,
            dashboardId: 5,
            panelId: 1,
            name: 'hello',
            state: '',
            stateText: '',
            stateIcon: '',
            stateClass: '',
            stateAge: '',
            url: '',
          },
          {
            id: 4,
            dashboardId: 7,
            panelId: 1,
            name: 'test',
            state: '',
            stateText: 'dashboard',
            stateIcon: '',
            stateClass: '',
            stateAge: '',
            url: '',
          },
        ],
        searchQuery: 'dashboard',
      },
    } as unknown as StoreState;

    const result = getAlertRuleItems(state);
    expect(result.length).toEqual(3);
  });
});
