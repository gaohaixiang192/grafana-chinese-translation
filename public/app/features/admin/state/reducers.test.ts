import { reducerTester } from 'test/core/redux/reducerTester';

import { LdapState, LdapUser, UserAdminState, UserDTO, UserListAdminState } from 'app/types';

import {
  clearUserMappingInfoAction,
  ldapConnectionInfoLoadedAction,
  ldapFailedAction,
  ldapReducer,
  ldapSyncStatusLoadedAction,
  userAdminReducer,
  userMappingInfoFailedAction,
  userMappingInfoLoadedAction,
  userProfileLoadedAction,
  userSessionsLoadedAction,
  userListAdminReducer,
  queryChanged,
  filterChanged,
} from './reducers';

const makeInitialLdapState = (): LdapState => ({
  connectionInfo: [],
});

const makeInitialUserAdminState = (): UserAdminState => ({
  sessions: [],
  orgs: [],
  isLoading: true,
});

const makeInitialUserListAdminState = (): UserListAdminState => ({
  users: [],
  query: '',
  page: 0,
  perPage: 50,
  totalPages: 1,
  showPaging: false,
  filters: [{ name: 'activeLast30Days', value: true }],
  isLoading: false,
});

const getTestUserMapping = (): LdapUser => ({
  info: {
    email: { cfgAttrValue: 'mail', ldapValue: 'user@localhost' },
    name: { cfgAttrValue: 'givenName', ldapValue: 'User' },
    surname: { cfgAttrValue: 'sn', ldapValue: '' },
    login: { cfgAttrValue: 'cn', ldapValue: 'user' },
  },
  permissions: {
    isGrafanaAdmin: false,
    isDisabled: false,
  },
  roles: [],
  teams: [],
});

const getTestUser = (): UserDTO => ({
  id: 1,
  email: 'user@localhost',
  login: 'user',
  name: 'User',
  avatarUrl: '',
  isGrafanaAdmin: false,
  isDisabled: false,
});

describe('LDAP页面缩减器', () => {
  describe('加载页面时', () => {
    describe('加载连接信息时', () => {
      it('应设置连接信息并清除错误', () => {
        const initialState = {
          ...makeInitialLdapState(),
        };

        reducerTester<LdapState>()
          .givenReducer(ldapReducer, initialState)
          .whenActionIsDispatched(
            ldapConnectionInfoLoadedAction([
              {
                available: true,
                host: 'localhost',
                port: 389,
                error: null as unknown as string,
              },
            ])
          )
          .thenStateShouldEqual({
            ...makeInitialLdapState(),
            connectionInfo: [
              {
                available: true,
                host: 'localhost',
                port: 389,
                error: null as unknown as string,
              },
            ],
            ldapError: undefined,
          });
      });
    });

    describe('连接失败时', () => {
      it('应该设置ldap错误', () => {
        const initialState = {
          ...makeInitialLdapState(),
        };

        reducerTester<LdapState>()
          .givenReducer(ldapReducer, initialState)
          .whenActionIsDispatched(
            ldapFailedAction({
              title: 'LDAP错误',
              body: '无法连接',
            })
          )
          .thenStateShouldEqual({
            ...makeInitialLdapState(),
            ldapError: {
              title: 'LDAP错误',
              body: '无法连接',
            },
          });
      });
    });

    describe('加载LDAP同步状态时', () => {
      it('应设置同步信息', () => {
        const initialState = {
          ...makeInitialLdapState(),
        };

        reducerTester<LdapState>()
          .givenReducer(ldapReducer, initialState)
          .whenActionIsDispatched(
            ldapSyncStatusLoadedAction({
              enabled: true,
              schedule: '0 0 * * * *',
              nextSync: '2019-01-01T12:00:00Z',
            })
          )
          .thenStateShouldEqual({
            ...makeInitialLdapState(),
            syncInfo: {
              enabled: true,
              schedule: '0 0 * * * *',
              nextSync: '2019-01-01T12:00:00Z',
            },
          });
      });
    });
  });

  describe('加载用户映射信息时', () => {
    it('应设置同步信息并清除用户错误', () => {
      const initialState = {
        ...makeInitialLdapState(),
        userError: {
          title: '未找到用户',
          body: '找不到用户',
        },
      };

      reducerTester<LdapState>()
        .givenReducer(ldapReducer, initialState)
        .whenActionIsDispatched(userMappingInfoLoadedAction(getTestUserMapping()))
        .thenStateShouldEqual({
          ...makeInitialLdapState(),
          user: getTestUserMapping(),
          userError: undefined,
        });
    });
  });

  describe('当找不到用户时', () => {
    it('应设置用户错误并清除用户信息', () => {
      const initialState = {
        ...makeInitialLdapState(),
        user: getTestUserMapping(),
      };

      reducerTester<LdapState>()
        .givenReducer(ldapReducer, initialState)
        .whenActionIsDispatched(
          userMappingInfoFailedAction({
            title: '未找到用户',
            body: '找不到用户',
          })
        )
        .thenStateShouldEqual({
          ...makeInitialLdapState(),
          user: undefined,
          userError: {
            title: '未找到用户',
            body: '找不到用户',
          },
        });
    });
  });

  describe('发送clearUserMappingInfoAction时', () => {
    it('那么状态应该是正确的', () => {
      reducerTester<LdapState>()
        .givenReducer(ldapReducer, {
          ...makeInitialLdapState(),
          user: getTestUserMapping(),
        })
        .whenActionIsDispatched(clearUserMappingInfoAction())
        .thenStateShouldEqual({
          ...makeInitialLdapState(),
          user: undefined,
        });
    });
  });
});

describe('编辑管理员用户页面缩减器', () => {
  describe('当用户加载时', () => {
    it('应设置用户并清除用户错误', () => {
      const initialState = {
        ...makeInitialUserAdminState(),
      };

      reducerTester<UserAdminState>()
        .givenReducer(userAdminReducer, initialState)
        .whenActionIsDispatched(userProfileLoadedAction(getTestUser()))
        .thenStateShouldEqual({
          ...makeInitialUserAdminState(),

          user: getTestUser(),
        });
    });
  });

  describe('当usersessionsloadedaction被调度时', () => {
    it('那么状态应该是正确的', () => {
      reducerTester<UserAdminState>()
        .givenReducer(userAdminReducer, { ...makeInitialUserAdminState() })
        .whenActionIsDispatched(
          userSessionsLoadedAction([
            {
              browser: 'Chrome',
              id: 1,
              browserVersion: '79',
              clientIp: '127.0.0.1',
              createdAt: '2020-01-01 00:00:00',
              device: 'a device',
              isActive: true,
              os: 'MacOS',
              osVersion: '15',
              seenAt: '2020-01-01 00:00:00',
            },
          ])
        )
        .thenStateShouldEqual({
          ...makeInitialUserAdminState(),
          sessions: [
            {
              browser: 'Chrome',
              id: 1,
              browserVersion: '79',
              clientIp: '127.0.0.1',
              createdAt: '2020-01-01 00:00:00',
              device: 'a device',
              isActive: true,
              os: 'MacOS',
              osVersion: '15',
              seenAt: '2020-01-01 00:00:00',
            },
          ],
        });
    });
  });
});

describe('用户列表管理缩减器', () => {
  describe('查询更改时', () => {
    it('应将页面重置为0', () => {
      const initialState = {
        ...makeInitialUserListAdminState(),
        page: 3,
      };

      reducerTester<UserListAdminState>()
        .givenReducer(userListAdminReducer, initialState)
        .whenActionIsDispatched(queryChanged('test'))
        .thenStateShouldEqual({
          ...makeInitialUserListAdminState(),
          query: 'test',
          page: 0,
        });
    });
  });

  describe('过滤器更换时', () => {
    it('应将页面重置为0', () => {
      const initialState = {
        ...makeInitialUserListAdminState(),
        page: 3,
      };

      reducerTester<UserListAdminState>()
        .givenReducer(userListAdminReducer, initialState)
        .whenActionIsDispatched(filterChanged({ test: true }))
        .thenStateShouldEqual({
          ...makeInitialUserListAdminState(),
          page: 0,
          filters: expect.arrayContaining([{ test: true }]),
        });
    });
  });
});
