import React from 'react';

import { PluginState } from '@grafana/data';
import { Badge, BadgeProps } from '@grafana/ui';

interface Props {
  state?: PluginState;
}

export const PluginStateInfo = (props: Props) => {
  const display = getFeatureStateInfo(props.state);

  if (!display) {
    return null;
  }

  return <Badge color={display.color} title={display.tooltip} text={display.text} icon={display.icon} />;
};

function getFeatureStateInfo(state?: PluginState): BadgeProps | null {
  switch (state) {
    case PluginState.deprecated:
      return {
        text: 'Deprecated',
        color: 'red',
        tooltip: `此功能已弃用，并将在将来的版本中删除`,
      };
    case PluginState.alpha:
      return {
        text: 'Alpha',
        color: 'blue',
        tooltip: `此功能是实验性的，将来的更新可能无法向后兼容`,
      };
    case PluginState.beta:
      return {
        text: 'Beta',
        color: 'blue',
        tooltip: `此功能已接近完成，但尚未完全测试`,
      };
    default:
      return null;
  }
}
