import type {
  PluginExtension,
  PluginExtensionConfig,
  PluginExtensionLink,
  PluginExtensionLinkConfig,
} from '@grafana/data';
import { isPluginExtensionLink } from '@grafana/runtime';

import { isPluginExtensionComponentConfig, isPluginExtensionLinkConfig, logWarning } from './utils';

export function assertPluginExtensionLink(
  extension: PluginExtension | undefined,
  errorMessage = '扩展名不是链接扩展名'
): asserts extension is PluginExtensionLink {
  if (!isPluginExtensionLink(extension)) {
    throw new Error(errorMessage);
  }
}

export function assertPluginExtensionLinkConfig(
  extension: PluginExtensionLinkConfig,
  errorMessage = '扩展名不是命令扩展名配置'
): asserts extension is PluginExtensionLinkConfig {
  if (!isPluginExtensionLinkConfig(extension)) {
    throw new Error(errorMessage);
  }
}

export function assertLinkPathIsValid(pluginId: string, path: string) {
  if (!isLinkPathValid(pluginId, path)) {
    throw new Error(
      `无效的链接扩展。“路径”是必需的，应以开头 "/a/${pluginId}/" (currently: "${path}"). 正在跳过扩展.`
    );
  }
}

export function assertIsReactComponent(component: React.ComponentType) {
  if (!isReactComponent(component)) {
    throw new Error(`无效的组件扩展，“component”属性需要是有效的React组件.`);
  }
}

export function assertExtensionPointIdIsValid(extension: PluginExtensionConfig) {
  if (!isExtensionPointIdValid(extension)) {
    throw new Error(
      `无效的扩展名 "${extension.title}". extensionPointId应以以下任一项开头 "grafana/" or "plugins/" (currently: "${extension.extensionPointId}"). 正在跳过扩展.`
    );
  }
}

export function assertConfigureIsValid(extension: PluginExtensionLinkConfig) {
  if (!isConfigureFnValid(extension)) {
    throw new Error(
      `无效的扩展名 "${extension.title}". “configure”属性必须是一个函数。正在跳过扩展.`
    );
  }
}

export function assertStringProps(extension: Record<string, unknown>, props: string[]) {
  for (const prop of props) {
    if (!isStringPropValid(extension[prop])) {
      throw new Error(
        `无效的扩展名 "${extension.title}". Property "${prop}" 必须是字符串并且不能为空。正在跳过扩展.`
      );
    }
  }
}

export function assertIsNotPromise(value: unknown, errorMessage = '提供的价值是承诺.'): void {
  if (isPromise(value)) {
    throw new Error(errorMessage);
  }
}

export function isLinkPathValid(pluginId: string, path: string) {
  return Boolean(typeof path === 'string' && path.length > 0 && path.startsWith(`/a/${pluginId}/`));
}

export function isExtensionPointIdValid(extension: PluginExtensionConfig) {
  return Boolean(
    extension.extensionPointId?.startsWith('grafana/') || extension.extensionPointId?.startsWith('plugins/')
  );
}

export function isConfigureFnValid(extension: PluginExtensionLinkConfig) {
  return extension.configure ? typeof extension.configure === 'function' : true;
}

export function isStringPropValid(prop: unknown) {
  return typeof prop === 'string' && prop.length > 0;
}

export function isPluginExtensionConfigValid(pluginId: string, extension: PluginExtensionConfig): boolean {
  try {
    assertStringProps(extension, ['title', 'description', 'extensionPointId']);
    assertExtensionPointIdIsValid(extension);

    if (isPluginExtensionLinkConfig(extension)) {
      assertConfigureIsValid(extension);

      if (!extension.path && !extension.onClick) {
        logWarning(`无效的扩展名 "${extension.title}". 需要“path”或“onClick”.`);
        return false;
      }

      if (extension.path) {
        assertLinkPathIsValid(pluginId, extension.path);
      }
    }

    if (isPluginExtensionComponentConfig(extension)) {
      assertIsReactComponent(extension.component);
    }

    return true;
  } catch (error) {
    if (error instanceof Error) {
      logWarning(error.message);
    }

    return false;
  }
}

export function isPromise(value: unknown): value is Promise<unknown> {
  return (
    value instanceof Promise || (typeof value === 'object' && value !== null && 'then' in value && 'catch' in value)
  );
}

export function isReactComponent(component: unknown): component is React.ComponentType {
  // We currently don't have any strict runtime-checking for this.
  // (The main reason is that we don't want to start depending on React implementation details.)
  return typeof component === 'function';
}
