import { render, waitFor } from '@testing-library/react';
import React from 'react';

import { config } from '@grafana/runtime';

import { DatasetSelector } from './DatasetSelector';
import { buildMockDatasetSelectorProps, buildMockTableSelectorProps } from './SqlComponents.testHelpers';
import { TableSelector } from './TableSelector';

beforeEach(() => {
  config.featureToggles.sqlDatasourceDatabaseSelection = true;
});

afterEach(() => {
  config.featureToggles.sqlDatasourceDatabaseSelection = false;
});

describe('DatasetSelector', () => {
  it('应仅在需要时查询数据库', async () => {
    const mockProps = buildMockDatasetSelectorProps();
    render(<DatasetSelector {...mockProps} />);

    await waitFor(() => {
      expect(mockProps.db.datasets).toHaveBeenCalled();
    });
  });

  it('如果Postgres实例不应该查询数据库，并且没有预配置的数据库', async () => {
    const mockProps = buildMockDatasetSelectorProps({ isPostgresInstance: true });
    render(<DatasetSelector {...mockProps} />);

    await waitFor(() => {
      expect(mockProps.db.datasets).not.toHaveBeenCalled();
    });
  });

  it('如果预先配置，则不应查询数据库', async () => {
    const mockProps = buildMockDatasetSelectorProps({ preconfiguredDataset: 'database 1' });
    render(<DatasetSelector {...mockProps} />);

    await waitFor(() => {
      expect(mockProps.db.datasets).not.toHaveBeenCalled();
    });
  });
});

describe('TableSelector', () => {
  it('应仅在需要时查询数据库', async () => {
    const mockProps = buildMockTableSelectorProps({ dataset: 'database 1' });
    render(<TableSelector {...mockProps} />);

    await waitFor(() => {
      expect(mockProps.db.tables).toHaveBeenCalled();
    });
  });

  it('如果没有数据集作为道具传递，则不应查询数据库', async () => {
    const mockProps = buildMockTableSelectorProps();
    render(<TableSelector {...mockProps} />);

    await waitFor(() => {
      expect(mockProps.db.tables).not.toHaveBeenCalled();
    });
  });
});
