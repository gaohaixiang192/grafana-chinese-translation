import { useEffect } from 'react';

import { DataSourcePluginOptionsEditorProps } from '@grafana/data';
import { logDebug, config } from '@grafana/runtime';

import { SQLOptions } from '../../types';

/**
 * 1. Moves the database field from the options object to jsonData.database and empties the database field.
 * 2. If max open connections, max idle connections, and auto idle are all undefined set these to default values.
 */
export function useMigrateDatabaseFields<T extends SQLOptions, S = {}>({
  onOptionsChange,
  options,
}: DataSourcePluginOptionsEditorProps<T, S>) {
  useEffect(() => {
    const jsonData = options.jsonData;
    let newOptions = { ...options };
    let optionsUpdated = false;

    // Migrate the database field from the column into the jsonData object
    if (options.database) {
      logDebug(`从具有值的options.database迁移 ${options.database} for ${options.name}`);
      newOptions.database = '';
      newOptions.jsonData = { ...jsonData, database: options.database };
      optionsUpdated = true;
    }

    // Set default values for max open connections, max idle connection,
    // and auto idle if they're all undefined
    if (
      jsonData.maxOpenConns === undefined &&
      jsonData.maxIdleConns === undefined &&
      jsonData.maxIdleConnsAuto === undefined
    ) {
      const { maxOpenConns, maxIdleConns } = config.sqlConnectionLimits;

      logDebug(
        `将默认的最大打开连接设置为 ${maxOpenConns} 并将最大空闲连接设置为 ${maxIdleConns}`
      );

      // Spread from the jsonData in new options in case
      // the database field was migrated as well
      newOptions.jsonData = {
        ...newOptions.jsonData,
        maxOpenConns: maxOpenConns,
        maxIdleConns: maxIdleConns,
        maxIdleConnsAuto: true,
      };

      // Make sure we issue an update if options changed
      optionsUpdated = true;
    }

    // If the maximum connection lifetime hasn't been
    // otherwise set fill in with the default from configuration
    if (jsonData.connMaxLifetime === undefined) {
      const { connMaxLifetime } = config.sqlConnectionLimits;

      // Spread new options and add our value
      newOptions.jsonData = {
        ...newOptions.jsonData,
        connMaxLifetime: connMaxLifetime,
      };

      // Note that we've updated the options
      optionsUpdated = true;
    }

    // Only issue an update if we changed options
    if (optionsUpdated) {
      onOptionsChange(newOptions);
    }
  }, [onOptionsChange, options]);
}
