import React from 'react';

import { DataSourceSettings } from '@grafana/data';
import { config } from '@grafana/runtime';
import { FieldSet, InlineField, InlineFieldRow, InlineSwitch } from '@grafana/ui';
import { NumberInput } from 'app/core/components/OptionsUI/NumberInput';

import { SQLConnectionLimits, SQLOptions } from '../../types';

interface Props<T> {
  onOptionsChange: Function;
  options: DataSourceSettings<SQLOptions>;
  labelWidth: number;
}

export const ConnectionLimits = <T extends SQLConnectionLimits>(props: Props<T>) => {
  const { onOptionsChange, options, labelWidth } = props;
  const jsonData = options.jsonData;
  const autoIdle = jsonData.maxIdleConnsAuto !== undefined ? jsonData.maxIdleConnsAuto : false;

  // Update JSON data with new values
  const updateJsonData = (values: {}) => {
    const newOpts = {
      ...options,
      jsonData: {
        ...jsonData,
        ...values,
      },
    };

    return onOptionsChange(newOpts);
  };

  // For the case of idle connections and connection lifetime
  // use a shared function to update respective properties
  const onJSONDataNumberChanged = (property: keyof SQLConnectionLimits) => {
    return (number?: number) => {
      updateJsonData({ [property]: number });
    };
  };

  // When the maximum number of connections is changed
  // see if we have the automatic idle option enabled
  const onMaxConnectionsChanged = (number?: number) => {
    if (autoIdle && number) {
      updateJsonData({
        maxOpenConns: number,
        maxIdleConns: number,
      });
    } else {
      updateJsonData({
        maxOpenConns: number,
      });
    }
  };

  // Update auto idle setting when control is toggled
  // and set minimum idle connections if automatic
  // is selected
  const onConnectionIdleAutoChanged = () => {
    let idleConns = undefined;
    let maxConns = undefined;

    // If the maximum number of open connections is undefined
    // and we're setting auto idle then set the default amount
    // otherwise take the numeric amount and get the value from that
    if (!autoIdle) {
      if (jsonData.maxOpenConns !== undefined) {
        maxConns = jsonData.maxOpenConns;
        idleConns = jsonData.maxOpenConns;
      }
    } else {
      maxConns = jsonData.maxOpenConns;
      idleConns = jsonData.maxIdleConns;
    }

    updateJsonData({
      maxIdleConnsAuto: !autoIdle,
      maxIdleConns: idleConns,
      maxOpenConns: maxConns,
    });
  };

  return (
    <FieldSet label="Connection limits">
      <InlineField
        tooltip={
          <span>
            打开的到数据库的最大连接数。如果 <i>最大空闲连接数</i> 大于0并且
            这个 <i>最大开放连接数</i> 小于 <i>最大空闲连接数</i>, 然后
            <i>最大空闲连接数</i> 将减少以匹配 <i>最大开放连接数</i> 限度如果设置为0,
            打开的连接数没有限制.
          </span>
        }
        labelWidth={labelWidth}
        label="Max open"
      >
        <NumberInput placeholder="unlimited" value={jsonData.maxOpenConns} onChange={onMaxConnectionsChanged} />
      </InlineField>
      <InlineFieldRow>
        <InlineField
          tooltip={
            <span>
              空闲连接池中的最大连接数。如果 <i>最大开放连接数</i> 更大
              小于0但小于 <i>最大空闲连接数</i>, 然后 <i>最大空闲连接数</i> 将减少
              以匹配 <i>最大开放连接数</i> 限度如果设置为0，则不会保留空闲连接.
            </span>
          }
          labelWidth={labelWidth}
          label="Max idle"
        >
          <NumberInput
            placeholder="2"
            value={jsonData.maxIdleConns}
            onChange={onJSONDataNumberChanged('maxIdleConns')}
            width={8}
            fieldDisabled={autoIdle}
          />
        </InlineField>
        <InlineField
          label="Auto"
          labelWidth={8}
          tooltip={
            <span>
              如果启用，则自动将<i>最大空闲连接数</i>设置为与
            <i> 最大开放连接数</i>。如果未设置最大打开连接数，则将其设置为默认值
              ({config.sqlConnectionLimits.maxIdleConns}).
            </span>
          }
        >
          <InlineSwitch value={autoIdle} onChange={onConnectionIdleAutoChanged} />
        </InlineField>
      </InlineFieldRow>
      <InlineField
        tooltip="连接可以重复使用的最长时间（以秒为单位）。如果设置为0，则连接将永远重复使用."
        labelWidth={labelWidth}
        label="Max lifetime"
      >
        <NumberInput
          placeholder="14400"
          value={jsonData.connMaxLifetime}
          onChange={onJSONDataNumberChanged('connMaxLifetime')}
        ></NumberInput>
      </InlineField>
    </FieldSet>
  );
};
