import React from 'react';

import {
  DataSourceJsonData,
  DataSourcePluginOptionsEditorProps,
  KeyValue,
  onUpdateDatasourceSecureJsonDataOption,
  updateDatasourcePluginResetOption,
} from '@grafana/data';
import { InlineField, SecretTextArea } from '@grafana/ui';

export interface Props<T extends DataSourceJsonData, S> {
  editorProps: DataSourcePluginOptionsEditorProps<T, S>;
  showCACert?: boolean;
  showKeyPair?: boolean;
  secureJsonFields?: KeyValue<Boolean>;
  labelWidth?: number;
}

export const TLSSecretsConfig = <T extends DataSourceJsonData, S extends {} = {}>(props: Props<T, S>) => {
  const { labelWidth, editorProps, showCACert, showKeyPair = true } = props;
  const { secureJsonFields } = editorProps.options;
  return (
    <>
      {showKeyPair ? (
        <InlineField
          tooltip={
            <span>要使用TLS/SSL客户端证书进行身份验证，请在此处提供客户端证书.</span>
          }
          labelWidth={labelWidth}
          label="TLS/SSL Client Certificate"
        >
          <SecretTextArea
            placeholder="Begins with -----BEGIN CERTIFICATE-----"
            cols={45}
            rows={7}
            isConfigured={secureJsonFields && secureJsonFields.tlsClientCert}
            onChange={onUpdateDatasourceSecureJsonDataOption(editorProps, 'tlsClientCert')}
            onReset={() => {
              updateDatasourcePluginResetOption(editorProps, 'tlsClientCert');
            }}
          ></SecretTextArea>
        </InlineField>
      ) : null}
      {showCACert ? (
        <InlineField
          tooltip={<span>如果所选的TLS/SSL模式需要服务器根证书，请在此处提供.</span>}
          labelWidth={labelWidth}
          label="TLS/SSL Root Certificate"
        >
          <SecretTextArea
            placeholder="Begins with -----BEGIN CERTIFICATE-----"
            cols={45}
            rows={7}
            isConfigured={secureJsonFields && secureJsonFields.tlsCACert}
            onChange={onUpdateDatasourceSecureJsonDataOption(editorProps, 'tlsCACert')}
            onReset={() => {
              updateDatasourcePluginResetOption(editorProps, 'tlsCACert');
            }}
          ></SecretTextArea>
        </InlineField>
      ) : null}
      {showKeyPair ? (
        <InlineField
          tooltip={<span>要使用客户端TLS/SSL证书进行身份验证，请在此处提供密钥.</span>}
          labelWidth={labelWidth}
          label="TLS/SSL Client Key"
        >
          <SecretTextArea
            placeholder="Begins with -----BEGIN RSA PRIVATE KEY-----"
            cols={45}
            rows={7}
            isConfigured={secureJsonFields && secureJsonFields.tlsClientKey}
            onChange={onUpdateDatasourceSecureJsonDataOption(editorProps, 'tlsClientKey')}
            onReset={() => {
              updateDatasourcePluginResetOption(editorProps, 'tlsClientKey');
            }}
          ></SecretTextArea>
        </InlineField>
      ) : null}
    </>
  );
};
