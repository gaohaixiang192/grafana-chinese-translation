import { css } from '@emotion/css';
import React, { useMemo } from 'react';
import { useAsync } from 'react-use';
import AutoSizer from 'react-virtualized-auto-sizer';
import { of } from 'rxjs';

import { GrafanaTheme2, PluginMeta } from '@grafana/data';
import { config } from '@grafana/runtime';
import { Alert, Spinner, useStyles2 } from '@grafana/ui';
import EmptyListCTA from 'app/core/components/EmptyListCTA/EmptyListCTA';
import { SearchResultsTable } from 'app/features/search/page/components/SearchResultsTable';
import { getGrafanaSearcher, SearchQuery } from 'app/features/search/service';

type Props = {
  plugin: PluginMeta;
};

export function PluginUsage({ plugin }: Props) {
  const styles = useStyles2(getStyles);

  const searchQuery = useMemo<SearchQuery>(() => {
    return {
      query: '*',
      panel_type: plugin.id,
      kind: ['panel'],
    };
  }, [plugin]);

  const results = useAsync(() => {
    return getGrafanaSearcher().search(searchQuery);
  }, [searchQuery]);

  const found = results.value;
  if (found?.totalRows) {
    return (
      <div className={styles.wrap}>
        <div className={styles.info}>
          {plugin.name} is used <b>{found.totalRows}</b> times.
        </div>
        <AutoSizer>
          {({ width, height }) => {
            return (
              <SearchResultsTable
                response={found}
                width={width}
                height={height}
                clearSelection={() => {}}
                keyboardEvents={of()}
                onTagSelected={() => {}}
              />
            );
          }}
        </AutoSizer>
      </div>
    );
  }

  if (results.loading) {
    return <Spinner />;
  }

  if (!config.featureToggles.panelTitleSearch) {
    return (
      <Alert title="缺少功能切换：面板标题搜索">
        插件使用情况需要新的搜索索引来查找仪表板中的使用情况
      </Alert>
    );
  }

  return (
    <EmptyListCTA
      title={`${plugin.name} 尚未在任何仪表板中使用`}
      buttonIcon="plus"
      buttonTitle="Create Dashboard"
      buttonLink={`dashboard/new?panelType=${plugin.id}&editPanel=1`}
    />
  );
}

export const getStyles = (theme: GrafanaTheme2) => {
  return {
    wrap: css`
      width: 100%;
      height: 100%;
    `,
    info: css`
      padding-bottom: 30px;
    `,
  };
};
