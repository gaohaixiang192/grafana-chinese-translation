import React from 'react';

import { PluginErrorCode } from '@grafana/data';
import { Badge } from '@grafana/ui';

type Props = { error?: PluginErrorCode };

export function PluginDisabledBadge({ error }: Props): React.ReactElement {
  const tooltip = errorCodeToTooltip(error);
  return <Badge icon="exclamation-triangle" text="Disabled" color="red" tooltip={tooltip} />;
}

function errorCodeToTooltip(error?: PluginErrorCode): string | undefined {
  switch (error) {
    case PluginErrorCode.modifiedSignature:
      return '插件因修改内容而被禁用';
    case PluginErrorCode.invalidSignature:
      return '由于插件签名无效，插件被禁用';
    case PluginErrorCode.missingSignature:
      return '由于缺少插件签名，插件被禁用';
    case null:
    case undefined:
      return '插件已禁用';
    default:
      return `由于未知错误，插件被禁用${error ? `: ${error}` : ''}`;
  }
}
