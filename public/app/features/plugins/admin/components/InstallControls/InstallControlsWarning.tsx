import { css } from '@emotion/css';
import React from 'react';

import { GrafanaTheme2, PluginType } from '@grafana/data';
import { config, featureEnabled } from '@grafana/runtime';
import { HorizontalGroup, Icon, LinkButton, useStyles2 } from '@grafana/ui';
import { contextSrv } from 'app/core/core';
import { AccessControlAction } from 'app/types';

import { getExternalManageLink } from '../../helpers';
import { isGrafanaAdmin } from '../../permissions';
import { useIsRemotePluginsAvailable } from '../../state/hooks';
import { CatalogPlugin, PluginStatus, Version } from '../../types';

interface Props {
  plugin: CatalogPlugin;
  pluginStatus: PluginStatus;
  latestCompatibleVersion?: Version;
}

export const InstallControlsWarning = ({ plugin, pluginStatus, latestCompatibleVersion }: Props) => {
  const styles = useStyles2(getStyles);
  const isExternallyManaged = config.pluginAdminExternalManageEnabled;
  const hasPermission = contextSrv.hasAccess(AccessControlAction.PluginsInstall, isGrafanaAdmin());
  const isRemotePluginsAvailable = useIsRemotePluginsAvailable();
  const isCompatible = Boolean(latestCompatibleVersion);

  if (plugin.type === PluginType.renderer) {
    return <div className={styles.message}>插件目录无法管理呈现器插件.</div>;
  }

  if (plugin.type === PluginType.secretsmanager) {
    return <div className={styles.message}>插件目录无法管理机密管理器插件.</div>;
  }

  if (plugin.isEnterprise && !featureEnabled('enterprise.plugins')) {
    return (
      <HorizontalGroup height="auto" align="center">
        <span className={styles.message}>未检测到有效的Grafana Enterprise许可证.</span>
        <LinkButton
          href={`${getExternalManageLink(plugin.id)}?utm_source=grafana_catalog_learn_more`}
          target="_blank"
          rel="noopener noreferrer"
          size="sm"
          fill="text"
          icon="external-link-alt"
        >
          Learn more
        </LinkButton>
      </HorizontalGroup>
    );
  }

  if (plugin.isDev) {
    return (
      <div className={styles.message}>这是插件的开发版本，可以&#39;无法卸载.</div>
    );
  }

  if (!hasPermission && !isExternallyManaged) {
    return <div className={styles.message}>{statusToMessage(pluginStatus)}</div>;
  }

  if (!plugin.isPublished) {
    return (
      <div className={styles.message}>
        <Icon name="exclamation-triangle" /> 此插件未发布到{' '}
        <a href="https://www.grafana.com/plugins" target="__blank" rel="noreferrer">
          grafana.com/plugins
        </a>{' '}
        and can&#39;不能通过目录进行管理.
      </div>
    );
  }

  if (!isCompatible) {
    return (
      <div className={styles.message}>
        <Icon name="exclamation-triangle" />
        &nbsp;这个插件没有&#39;不支持您的Grafana版本.
      </div>
    );
  }

  if (!isRemotePluginsAvailable) {
    return (
      <div className={styles.message}>
        由于Grafana服务器无法访问Grafana.com，安装控制已被禁用.
      </div>
    );
  }

  return null;
};

export const getStyles = (theme: GrafanaTheme2) => {
  return {
    message: css`
      color: ${theme.colors.text.secondary};
    `,
  };
};

function statusToMessage(status: PluginStatus): string {
  switch (status) {
    case PluginStatus.INSTALL:
    case PluginStatus.REINSTALL:
      return `您没有安装此插件的权限.`;
    case PluginStatus.UNINSTALL:
      return `您没有卸载此插件的权限.`;
    case PluginStatus.UPDATE:
      return `您没有更新此插件的权限.`;
    default:
      return `您没有管理此插件的权限.`;
  }
}
