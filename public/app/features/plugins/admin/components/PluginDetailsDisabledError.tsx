import React, { ReactElement } from 'react';

import { PluginErrorCode } from '@grafana/data';
import { selectors } from '@grafana/e2e-selectors';
import { Alert } from '@grafana/ui';

import { CatalogPlugin } from '../types';

type Props = {
  className?: string;
  plugin: CatalogPlugin;
};

export function PluginDetailsDisabledError({ className, plugin }: Props): ReactElement | null {
  if (!plugin.isDisabled) {
    return null;
  }

  return (
    <Alert
      severity="error"
      title="Plugin disabled"
      className={className}
      aria-label={selectors.pages.PluginPage.disabledInfo}
    >
      {renderDescriptionFromError(plugin.error)}
      <p>请与服务器管理员联系以解决此问题.</p>
      <a
        href="https://grafana.com/docs/grafana/latest/administration/cli/#plugins-commands"
        className="external-link"
        target="_blank"
        rel="noreferrer"
      >
        阅读有关管理插件的更多信息
      </a>
    </Alert>
  );
}

function renderDescriptionFromError(error?: PluginErrorCode): ReactElement {
  switch (error) {
    case PluginErrorCode.modifiedSignature:
      return (
        <p>
          Grafana实验室检查每个插件，以验证其是否具有有效的数字签名。在这样做的同时，我们
          发现此插件的内容与其签名不匹配。我们不能保证值得信赖
          的插件，因此已将其禁用。我们建议您重新安装该插件，以确保您
          运行此插件的已验证版本.
        </p>
      );
    case PluginErrorCode.invalidSignature:
      return (
        <p>
          Grafana实验室检查每个插件，以验证其是否具有有效的数字签名。在这样做的同时，我们
          发现它是无效的。我们不能保证这个插件的可信，因此
          已禁用它。我们建议您重新安装该插件，以确保您运行的是该插件的验证版本
          插件.
        </p>
      );
    case PluginErrorCode.missingSignature:
      return (
        <p>
          Grafana实验室检查每个插件，以验证其是否具有有效的数字签名。在这样做的同时，我们
          发现此插件没有签名。我们不能保证这个插件和
          因此已将其禁用。我们建议您重新安装该插件，以确保您运行的是经过验证的
          此插件的版本.
        </p>
      );
    default:
      return (
        <p>
          由于未知原因，我们未能运行此插件，因此已将其禁用。我们建议您
          重新安装插件以确保运行的是该插件的工作版本.
        </p>
      );
  }
}
