import React from 'react';

import { PluginErrorCode, PluginSignatureStatus } from '@grafana/data';
import { selectors } from '@grafana/e2e-selectors';
import { Alert } from '@grafana/ui';

import { CatalogPlugin } from '../types';

type Props = {
  className?: string;
  plugin: CatalogPlugin;
};

// Designed to show signature information inside the active tab on the plugin's details page
export function PluginDetailsSignature({ className, plugin }: Props): React.ReactElement | null {
  const isSignatureValid = plugin.signature === PluginSignatureStatus.valid;
  const isCore = plugin.signature === PluginSignatureStatus.internal;
  const isDisabled = plugin.isDisabled && isDisabledDueTooSignatureError(plugin.error);

  // The basic information is already available in the header
  if (isSignatureValid || isCore || isDisabled) {
    return null;
  }

  return (
    <Alert
      severity="warning"
      title="Invalid plugin signature"
      aria-label={selectors.pages.PluginPage.signatureInfo}
      className={className}
    >
      <p>
        Grafana实验室检查每个插件，以验证其是否具有有效的数字签名。插件签名验证
        是我们安全措施的一部分，以确保插件安全可靠。格拉法纳实验室不能保证
        这个未签名插件的完整性。请插件作者请求签名.
      </p>

      <a
        href="https://grafana.com/docs/grafana/latest/plugins/plugin-signatures/"
        className="external-link"
        target="_blank"
        rel="noreferrer"
      >
        阅读有关插件签名的更多信息.
      </a>
    </Alert>
  );
}

function isDisabledDueTooSignatureError(error: PluginErrorCode | undefined) {
  // If the plugin is disabled due to signature error we rely on the disabled
  // error message instad of the warning about the signature.

  switch (error) {
    case PluginErrorCode.invalidSignature:
    case PluginErrorCode.missingSignature:
    case PluginErrorCode.modifiedSignature:
      return true;

    default:
      return false;
  }
}
