// Temporary place to collect phrases we reuse between new and old browse/search
// TODO: remove this when new Browse Dashboards UI is no longer feature flagged

import { t } from 'app/core/internationalization';

export function getSearchPlaceholder(includePanels = false) {
  return includePanels
    ? t('search.search-input.include-panels-placeholder', '搜索仪表板、文件夹和面板')
    : t('search.search-input.placeholder', '搜索仪表板和文件夹');
}

export function getNewDashboardPhrase() {
  return t('search.dashboard-actions.new-dashboard', '新建仪表板');
}

export function getNewFolderPhrase() {
  return t('search.dashboard-actions.new-folder', '新增文件夹');
}

export function getImportPhrase() {
  return t('search.dashboard-actions.import', '导入');
}

export function getNewPhrase() {
  return t('search.dashboard-actions.new', '新');
}
