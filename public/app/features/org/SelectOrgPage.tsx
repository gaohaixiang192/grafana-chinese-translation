import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useEffectOnce } from 'react-use';

import { config } from '@grafana/runtime';
import { Button, HorizontalGroup } from '@grafana/ui';
import { Page } from 'app/core/components/Page/Page';
import { StoreState, UserOrg } from 'app/types';

import { getUserOrganizations, setUserOrganization } from './state/actions';

const navModel = {
  main: {
    icon: 'grafana' as const,
    subTitle: 'Preferences',
    text: '选择活动组织',
  },
  node: {
    text: '选择活动组织',
  },
};

const mapStateToProps = (state: StoreState) => {
  return {
    userOrgs: state.organization.userOrgs,
  };
};

const mapDispatchToProps = {
  setUserOrganization,
  getUserOrganizations,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type Props = ConnectedProps<typeof connector>;

export const SelectOrgPage = ({ setUserOrganization, getUserOrganizations, userOrgs }: Props) => {
  const setUserOrg = async (org: UserOrg) => {
    await setUserOrganization(org.orgId);
    window.location.href = config.appSubUrl + '/';
  };

  useEffectOnce(() => {
    getUserOrganizations();
  });

  return (
    <Page navModel={navModel}>
      <Page.Contents>
        <div>
          <p>
            您已被邀请加入其他组织！请选择当前要使用的组织。您可以稍后随时更改.
          </p>
          <HorizontalGroup wrap>
            {userOrgs &&
              userOrgs.map((org) => (
                <Button key={org.orgId} icon="signin" onClick={() => setUserOrg(org)}>
                  {org.name}
                </Button>
              ))}
          </HorizontalGroup>
        </div>
      </Page.Contents>
    </Page>
  );
};

export default connector(SelectOrgPage);
