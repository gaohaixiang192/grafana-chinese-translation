import { useEffect } from 'react';

import { config } from '@grafana/runtime';
import { useAppNotification } from 'app/core/copy/appNotification';
import { useCorrelations } from 'app/features/correlations/useCorrelations';
import { useDispatch } from 'app/types';

import { saveCorrelationsAction } from '../state/main';

export function useExploreCorrelations() {
  const { get } = useCorrelations();
  const { warning } = useAppNotification();

  const dispatch = useDispatch();
  useEffect(() => {
    if (!config.featureToggles.correlations) {
      dispatch(saveCorrelationsAction([]));
    } else {
      get.execute();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (get.value) {
      dispatch(saveCorrelationsAction(get.value));
    } else if (get.error) {
      dispatch(saveCorrelationsAction([]));
      warning(
        '无法加载相关性.',
        '无法加载相关性数据，DataLinks可能包含部分数据.'
      );
    }
  }, [get.value, get.error, dispatch, warning]);
}
