import { css } from '@emotion/css';
import React from 'react';

import { ConfirmModal } from '@grafana/ui';

interface Props {
  varName: string;
  isOpen: boolean;
  onConfirm: () => void;
  onDismiss: () => void;
}

export function ConfirmDeleteModal({ varName, isOpen = false, onConfirm, onDismiss }: Props) {
  return (
    <ConfirmModal
      title="Delete variable"
      isOpen={isOpen}
      onConfirm={onConfirm}
      onDismiss={onDismiss}
      body={`
      是否确实要删除变量 "${varName}"?
    `}
      modalClass={styles.modal}
      confirmText="Delete"
    />
  );
}

const styles = {
  modal: css({
    width: 'max-content',
    maxWidth: '80vw',
  }),
};
