import React, { PropsWithChildren, useMemo } from 'react';

import { VariableRefresh } from '@grafana/data';
import { Field, RadioButtonGroup } from '@grafana/ui';

interface Props {
  onChange: (option: VariableRefresh) => void;
  refresh: VariableRefresh;
}

const REFRESH_OPTIONS = [
  { label: '仪表板加载时', value: VariableRefresh.onDashboardLoad },
  { label: '随时间变化', value: VariableRefresh.onTimeRangeChanged },
];

export function QueryVariableRefreshSelect({ onChange, refresh }: PropsWithChildren<Props>) {
  const value = useMemo(
    () => REFRESH_OPTIONS.find((o) => o.value === refresh)?.value ?? REFRESH_OPTIONS[0].value,
    [refresh]
  );

  return (
    <Field label="Refresh" description="何时更新此变量的值">
      <RadioButtonGroup options={REFRESH_OPTIONS} onChange={onChange} value={value} />
    </Field>
  );
}
