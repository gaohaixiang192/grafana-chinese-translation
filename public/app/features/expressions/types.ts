import { DataQuery, ReducerID, SelectableValue } from '@grafana/data';

import { EvalFunction } from '../alerting/state/alertDef';

/**
 * MATCHES a constant in DataSourceWithBackend
 */
export const ExpressionDatasourceUID = '__expr__';

export enum ExpressionQueryType {
  math = 'math',
  reduce = 'reduce',
  resample = 'resample',
  classic = 'classic_conditions',
  threshold = 'threshold',
}

export const gelTypes: Array<SelectableValue<ExpressionQueryType>> = [
  {
    value: ExpressionQueryType.math,
    label: 'Math',
    description: '关于时间序列或数字数据的自由形式数学公式.',
  },
  {
    value: ExpressionQueryType.reduce,
    label: 'Reduce',
    description:
      '获取从查询或表达式返回的一个或多个时间序列，并将每个序列转换为单个数字.',
  },
  {
    value: ExpressionQueryType.resample,
    label: 'Resample',
    description: '更改每个时间序列中的时间戳，使其具有一致的时间间隔.',
  },
  {
    value: ExpressionQueryType.classic,
    label: 'Classic condition',
    description:
      '获取从查询或表达式返回的一个或多个时间序列，并检查这些序列中是否有符合条件的序列.',
  },
  {
    value: ExpressionQueryType.threshold,
    label: 'Threshold',
    description:
      '获取从查询或表达式返回的一个或多个时间序列，并检查是否有任何序列符合阈值条件.',
  },
];

export const reducerTypes: Array<SelectableValue<string>> = [
  { value: ReducerID.min, label: 'Min', description: '获取最小值' },
  { value: ReducerID.max, label: 'Max', description: '获取最大值' },
  { value: ReducerID.mean, label: 'Mean', description: '获取平均值' },
  { value: ReducerID.sum, label: 'Sum', description: '获取所有值的总和' },
  { value: ReducerID.count, label: 'Count', description: '获取值的数目' },
  { value: ReducerID.last, label: 'Last', description: '获取最后一个值' },
];

export enum ReducerMode {
  Strict = '', // backend API wants an empty string to support "strict" mode
  ReplaceNonNumbers = 'replaceNN',
  DropNonNumbers = 'dropNN',
}

export const reducerModes: Array<SelectableValue<ReducerMode>> = [
  {
    value: ReducerMode.Strict,
    label: 'Strict',
    description: '如果序列包含非数字数据，则结果可以是NaN',
  },
  {
    value: ReducerMode.DropNonNumbers,
    label: 'Drop Non-numeric Values',
    description: '减少前从输入序列中删除NaN、+/-Inf和null',
  },
  {
    value: ReducerMode.ReplaceNonNumbers,
    label: 'Replace Non-numeric Values',
    description: '在减少之前，将NaN、+/-Inf和null替换为常数值',
  },
];

export const downsamplingTypes: Array<SelectableValue<string>> = [
  { value: ReducerID.last, label: 'Last', description: '用最后一个值填充' },
  { value: ReducerID.min, label: 'Min', description: '填充最小值' },
  { value: ReducerID.max, label: 'Max', description: '填充最大值' },
  { value: ReducerID.mean, label: 'Mean', description: '用平均值填充' },
  { value: ReducerID.sum, label: 'Sum', description: '填充所有值的总和' },
];

export const upsamplingTypes: Array<SelectableValue<string>> = [
  { value: 'pad', label: 'pad', description: '用最后一个已知值填充' },
  { value: 'backfilling', label: 'backfilling', description: '填充下一个已知值' },
  { value: 'fillna', label: 'fillna', description: '填充NaN' },
];

export const thresholdFunctions: Array<SelectableValue<EvalFunction>> = [
  { value: EvalFunction.IsAbove, label: 'Is above' },
  { value: EvalFunction.IsBelow, label: 'Is below' },
  { value: EvalFunction.IsWithinRange, label: 'Is within range' },
  { value: EvalFunction.IsOutsideRange, label: 'Is outside range' },
];

/**
 * For now this is a single object to cover all the types.... would likely
 * want to split this up by type as the complexity increases
 */
export interface ExpressionQuery extends DataQuery {
  type: ExpressionQueryType;
  reducer?: string;
  expression?: string;
  window?: string;
  downsampler?: string;
  upsampler?: string;
  conditions?: ClassicCondition[];
  settings?: ExpressionQuerySettings;
}

export interface ExpressionQuerySettings {
  mode?: ReducerMode;
  replaceWithValue?: number;
}

export interface ClassicCondition {
  evaluator: {
    params: number[];
    type: EvalFunction;
  };
  operator?: {
    type: string;
  };
  query: {
    params: string[];
  };
  reducer: {
    params: [];
    type: ReducerType;
  };
  type: 'query';
}

export type ReducerType =
  | 'avg'
  | 'min'
  | 'max'
  | 'sum'
  | 'count'
  | 'last'
  | 'median'
  | 'diff'
  | 'diff_abs'
  | 'percent_diff'
  | 'percent_diff_abs'
  | 'count_non_null';
