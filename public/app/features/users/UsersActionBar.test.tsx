import { render, screen } from '@testing-library/react';
import React from 'react';
import { mockToolkitActionCreator } from 'test/core/redux/mocks';

import { config } from 'app/core/config';

import { Props, UsersActionBarUnconnected } from './UsersActionBar';
import { searchQueryChanged } from './state/reducers';

jest.mock('app/core/core', () => ({
  contextSrv: {
    hasPermission: () => true,
    hasAccess: () => true,
  },
}));

const setup = (propOverrides?: object) => {
  const props: Props = {
    searchQuery: '',
    changeSearchQuery: mockToolkitActionCreator(searchQueryChanged),
    onShowInvites: jest.fn(),
    pendingInvitesCount: 0,
    canInvite: false,
    externalUserMngLinkUrl: '',
    externalUserMngLinkName: '',
    showInvites: false,
  };

  Object.assign(props, propOverrides);

  const { rerender } = render(<UsersActionBarUnconnected {...props} />);

  return { rerender, props };
};

describe('Render', () => {
  it('应该呈现组件', () => {
    setup();

    expect(screen.getByTestId('users-action-bar')).toBeInTheDocument();
  });

  it('应呈现挂起的邀请按钮', () => {
    setup({
      pendingInvitesCount: 5,
    });

    expect(screen.getByRole('radio', { name: 'Pending Invites (5)' })).toBeInTheDocument();
  });

  it('应显示邀请按钮', () => {
    setup({
      canInvite: true,
    });

    expect(screen.getByRole('link', { name: 'Invite' })).toHaveAttribute('href', 'org/users/invite');
  });

  it('应显示外部用户管理按钮', () => {
    setup({
      externalUserMngLinkUrl: 'some/url',
      externalUserMngLinkName: 'someUrl',
    });

    expect(screen.getByRole('link', { name: 'someUrl' })).toHaveAttribute('href', 'some/url');
  });

  it('设置externalUserMngInfo时不应显示邀请按钮', () => {
    const originalExternalUserMngInfo = config.externalUserMngInfo;
    config.externalUserMngInfo = 'truthy';

    setup({
      canInvite: true,
    });

    expect(screen.queryByRole('link', { name: 'Invite' })).not.toBeInTheDocument();
    // Reset the disableLoginForm mock to its original value
    config.externalUserMngInfo = originalExternalUserMngInfo;
  });
});
