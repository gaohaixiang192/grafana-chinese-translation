import { css, cx } from '@emotion/css';
import React from 'react';

import { GrafanaTheme2 } from '@grafana/data';
import { Button, InfoBox, Portal, stylesFactory, useTheme2 } from '@grafana/ui';
import { getModalStyles } from '@grafana/ui/src/components/Modal/getModalStyles';

interface Props {
  maxConcurrentSessions?: number;
}

export const TokenRevokedModal = (props: Props) => {
  const theme = useTheme2();

  const styles = getStyles(theme);
  const modalStyles = getModalStyles(theme);

  const showMaxConcurrentSessions = Boolean(props.maxConcurrentSessions);

  const redirectToLogin = () => {
    window.location.reload();
  };

  return (
    <Portal>
      <div className={modalStyles.modal}>
        <InfoBox title="您已自动注销" severity="warning" className={styles.infobox}>
          <div className={styles.text}>
            <p>
              您的会话令牌已自动吊销，因为您已达到
              <strong>
                {` the maximum number of ${
                  showMaxConcurrentSessions ? props.maxConcurrentSessions : ''
                } concurrent sessions `}
              </strong>
              记在你方账上.
            </p>
            <p>
              <strong>若要继续会话，请重新登录.</strong>
              如果您重复自动注销，请与管理员联系或访问许可证页面以查看您的配额.
            </p>
          </div>
          <Button size="md" variant="primary" onClick={redirectToLogin}>
            Sign in
          </Button>
        </InfoBox>
      </div>
      <div className={cx(modalStyles.modalBackdrop, styles.backdrop)} />
    </Portal>
  );
};

const getStyles = stylesFactory((theme: GrafanaTheme2) => {
  return {
    infobox: css`
      margin-bottom: 0;
    `,
    text: css`
      margin: ${theme.spacing(1, 0, 2)};
    `,
    backdrop: css`
      background-color: ${theme.colors.background.canvas};
      opacity: 0.8;
    `,
  };
});
