import { useEffect } from 'react';
import { useAsyncFn } from 'react-use';

import { locationService } from '@grafana/runtime';
import { useAppNotification } from 'app/core/copy/appNotification';
import { deleteDashboard } from 'app/features/manage-dashboards/state/actions';

export const useDashboardDelete = (uid: string, cleanUpDashboardAndVariables: () => void) => {
  const [state, onDeleteDashboard] = useAsyncFn(() => deleteDashboard(uid, false), []);
  const notifyApp = useAppNotification();

  useEffect(() => {
    if (state.value) {
      cleanUpDashboardAndVariables();
      locationService.replace('/');
      notifyApp.success('仪表板已删除', `${state.value.title} 已被删除`);
    }
  }, [state, notifyApp, cleanUpDashboardAndVariables]);

  return { state, onDeleteDashboard };
};
