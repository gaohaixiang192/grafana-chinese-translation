import React from 'react';

import { Button, ModalsController } from '@grafana/ui';

import { DashboardModel } from '../../state';

import { DeleteDashboardModal } from './DeleteDashboardModal';

type Props = {
  dashboard: DashboardModel;
};

export const DeleteDashboardButton = ({ dashboard }: Props) => (
  <ModalsController>
    {({ showModal, hideModal }) => (
      <Button
        variant="destructive"
        onClick={() => {
          showModal(DeleteDashboardModal, {
            dashboard,
            hideModal,
          });
        }}
        aria-label="仪表板设置页面删除仪表板按钮"
      >
        Delete Dashboard
      </Button>
    )}
  </ModalsController>
);
