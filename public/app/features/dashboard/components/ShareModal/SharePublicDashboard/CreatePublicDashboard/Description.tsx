import { css } from '@emotion/css';
import cx from 'classnames';
import React from 'react';

import { GrafanaTheme2 } from '@grafana/data/src';
import { useStyles2 } from '@grafana/ui/src';

export const Description = () => {
  const styles = useStyles2(getStyles);

  return (
    <div className={styles.container}>
      <p className={styles.description}>目前，我们不支持模板变量或前端数据源</p>
      <p className={styles.description}>
        我们&apos;我喜欢你的反馈。要分享，请对此发表评论{' '}
        <a
          href="https://github.com/grafana/grafana/discussions/49253"
          target="_blank"
          rel="noreferrer"
          className={cx('text-link', styles.description)}
        >
          GitHub讨论
        </a>
        .
      </p>
    </div>
  );
};

const getStyles = (theme: GrafanaTheme2) => ({
  container: css`
    margin-bottom: ${theme.spacing(3)};
  `,
  description: css`
    color: ${theme.colors.text.secondary};
    margin-bottom: ${theme.spacing(1)};
  `,
});
