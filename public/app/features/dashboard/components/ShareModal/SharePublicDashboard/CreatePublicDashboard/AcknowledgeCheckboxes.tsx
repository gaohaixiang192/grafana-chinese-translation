import { css } from '@emotion/css';
import React from 'react';
import { UseFormRegister } from 'react-hook-form';

import { GrafanaTheme2 } from '@grafana/data/src';
import { selectors as e2eSelectors } from '@grafana/e2e-selectors/src';
import { Checkbox, FieldSet, HorizontalGroup, LinkButton, useStyles2, VerticalGroup } from '@grafana/ui/src';

import { SharePublicDashboardAcknowledgmentInputs } from './CreatePublicDashboard';

type Acknowledge = {
  type: keyof SharePublicDashboardAcknowledgmentInputs;
  description: string;
  testId: string;
  info: {
    href: string;
    tooltip: string;
  };
};
const selectors = e2eSelectors.pages.ShareDashboardModal.PublicDashboard;

const ACKNOWLEDGES: Acknowledge[] = [
  {
    type: 'publicAcknowledgment',
    description: '您的整个仪表板将是公开的*',
    testId: selectors.WillBePublicCheckbox,
    info: {
      href: 'https://grafana.com/docs/grafana/latest/dashboards/dashboard-public/',
      tooltip: '了解有关公用仪表板的详细信息',
    },
  },
  {
    type: 'dataSourcesAcknowledgment',
    description: '发布当前仅适用于数据源的子集*',
    testId: selectors.LimitedDSCheckbox,
    info: {
      href: 'https://grafana.com/docs/grafana/latest/datasources/',
      tooltip: '了解有关公共数据源的详细信息',
    },
  },
  {
    type: 'usageAcknowledgment',
    description: '公开仪表板将导致每次查看时都会运行查询，这可能会增加成本*',
    testId: selectors.CostIncreaseCheckbox,
    info: {
      href: 'https://grafana.com/docs/grafana/latest/enterprise/query-caching/',
      tooltip: '了解有关查询缓存的详细信息',
    },
  },
];

export const AcknowledgeCheckboxes = ({
  disabled,
  register,
}: {
  disabled: boolean;
  register: UseFormRegister<SharePublicDashboardAcknowledgmentInputs>;
}) => {
  const styles = useStyles2(getStyles);

  return (
    <>
      <p className={styles.title}>Before you make the dashboard public, acknowledge the following:</p>
      <FieldSet disabled={disabled}>
        <VerticalGroup spacing="md">
          {ACKNOWLEDGES.map((acknowledge) => (
            <HorizontalGroup key={acknowledge.type} spacing="none" align="center">
              <Checkbox
                {...register(acknowledge.type, { required: true })}
                label={acknowledge.description}
                data-testid={acknowledge.testId}
              />
              <LinkButton
                variant="primary"
                href={acknowledge.info.href}
                target="_blank"
                fill="text"
                icon="info-circle"
                rel="noopener noreferrer"
                tooltip={acknowledge.info.tooltip}
              />
            </HorizontalGroup>
          ))}
        </VerticalGroup>
      </FieldSet>
    </>
  );
};

const getStyles = (theme: GrafanaTheme2) => ({
  title: css`
    font-weight: ${theme.typography.fontWeightBold};
  `,
});
