import React from 'react';

import { selectors as e2eSelectors } from '@grafana/e2e-selectors/src';
import { Alert } from '@grafana/ui/src';

const selectors = e2eSelectors.pages.ShareDashboardModal.PublicDashboard;

export const UnsupportedTemplateVariablesAlert = () => (
  <Alert
    severity="warning"
    title="不支持模板变量"
    data-testid={selectors.TemplateVariablesWarningAlert}
  >
    此公用仪表板可能无法工作，因为它使用了模板变量
  </Alert>
);
