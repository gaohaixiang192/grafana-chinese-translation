import React from 'react';

import { selectors as e2eSelectors } from '@grafana/e2e-selectors/src';
import { Alert } from '@grafana/ui/src';

const selectors = e2eSelectors.pages.ShareDashboardModal.PublicDashboard;

export const NoUpsertPermissionsAlert = ({ mode }: { mode: 'create' | 'edit' }) => (
  <Alert
    severity="info"
    title={`您没有权限 ${mode} 公共仪表板`}
    data-testid={selectors.NoUpsertPermissionsWarningAlert}
  >
    请与管理员联系以获得{mode}创建公共仪表板的权限
  </Alert>
);
