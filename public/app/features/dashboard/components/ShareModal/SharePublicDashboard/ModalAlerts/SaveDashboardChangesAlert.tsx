import React from 'react';

import { Alert } from '@grafana/ui/src';

export const SaveDashboardChangesAlert = () => (
  <Alert title="请在更新公用配置之前保存您的仪表板更改" severity="warning" />
);
