import { css } from '@emotion/css';
import React from 'react';
import { useAsync } from 'react-use';

import { GrafanaTheme2 } from '@grafana/data';
import { FetchError } from '@grafana/runtime';
import { Alert, useStyles2 } from '@grafana/ui';
import { backendSrv } from 'app/core/services/backend_srv';

import { DashboardModel } from '../../state';

interface DashboardValidationProps {
  dashboard: DashboardModel;
}

type ValidationResponse = Awaited<ReturnType<typeof backendSrv.validateDashboard>>;

function DashboardValidation({ dashboard }: DashboardValidationProps) {
  const styles = useStyles2(getStyles);
  const { loading, value, error } = useAsync(async () => {
    const saveModel = dashboard.getSaveModelClone();
    const respPromise = backendSrv
      .validateDashboard(saveModel)
      // API returns schema validation errors in 4xx range, so resolve them rather than throwing
      .catch((err: FetchError<ValidationResponse>) => {
        if (err.status >= 500) {
          throw err;
        }

        return err.data;
      });

    return respPromise;
  }, [dashboard]);

  let alert: React.ReactNode;

  if (loading) {
    alert = <Alert severity="info" title="检查仪表板的有效性" />;
  } else if (value) {
    if (!value.isValid) {
      alert = (
        <Alert severity="warning" title="仪表板架构验证失败">
          <p>
            验证是为了开发目的而提供的，应该可以安全地忽略。如果你是格拉法纳人
            开发人员，考虑检查和更新仪表板模式
          </p>
          <div className={styles.error}>{value.message}</div>
        </Alert>
      );
    }
  } else {
    const errorMessage = error?.message ?? 'Unknown error';
    alert = (
      <Alert severity="info" title="检查仪表板有效性时出错">
        <p className={styles.error}>{errorMessage}</p>
      </Alert>
    );
  }

  if (alert) {
    return <div className={styles.root}>{alert}</div>;
  }

  return null;
}

const getStyles = (theme: GrafanaTheme2) => ({
  root: css({
    marginTop: theme.spacing(1),
  }),
  error: css({
    fontFamily: theme.typography.fontFamilyMonospace,
    whiteSpace: 'pre-wrap',
    overflowX: 'auto',
    maxWidth: '100%',
  }),
});

export default DashboardValidation;
