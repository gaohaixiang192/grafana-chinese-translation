import { css } from '@emotion/css';
import { saveAs } from 'file-saver';
import React, { useCallback, useState } from 'react';

import { Stack } from '@grafana/experimental';
import { Button, ClipboardButton, HorizontalGroup, TextArea } from '@grafana/ui';

import { SaveDashboardFormProps } from '../types';

export const SaveProvisionedDashboardForm = ({ dashboard, onCancel }: SaveDashboardFormProps) => {
  const [dashboardJSON, setDashboardJson] = useState(() => {
    const clone = dashboard.getSaveModelClone();
    delete clone.id;
    return JSON.stringify(clone, null, 2);
  });

  const saveToFile = useCallback(() => {
    const blob = new Blob([dashboardJSON], {
      type: 'application/json;charset=utf-8',
    });
    saveAs(blob, dashboard.title + '-' + new Date().getTime() + '.json');
  }, [dashboard.title, dashboardJSON]);

  return (
    <>
      <Stack direction="column" gap={2}>
        <div>
          无法从Grafana UI保存此仪表板，因为它是从另一个源提供的。复制
          JSON或将其保存到下面的文件中，然后您可以在供应源中更新仪表板.
          <br />
          <i>
            See{' '}
            <a
              className="external-link"
              href="https://grafana.com/docs/grafana/latest/administration/provisioning/#dashboards"
              target="_blank"
              rel="noreferrer"
            >
              documentation
            </a>{' '}
            for more information about provisioning.
          </i>
          <br /> <br />
          <strong>File path: </strong> {dashboard.meta.provisionedExternalId}
        </div>
        <TextArea
          spellCheck={false}
          value={dashboardJSON}
          onChange={(e) => {
            setDashboardJson(e.currentTarget.value);
          }}
          className={styles.json}
        />
        <HorizontalGroup>
          <Button variant="secondary" onClick={onCancel} fill="outline">
            Cancel
          </Button>
          <ClipboardButton icon="copy" getText={() => dashboardJSON}>
            Copy JSON to clipboard
          </ClipboardButton>
          <Button type="submit" onClick={saveToFile}>
            Save JSON to file
          </Button>
        </HorizontalGroup>
      </Stack>
    </>
  );
};

const styles = {
  json: css`
    height: 400px;
    width: 100%;
    overflow: auto;
    resize: none;
    font-family: monospace;
  `,
};
