import {
  VizPanel,
  SceneGridRow,
  SceneTimePicker,
  SceneGridLayout,
  SceneTimeRange,
  SceneRefreshPicker,
  SceneGridItem,
} from '@grafana/scenes';
import { TestDataQueryType } from 'app/plugins/datasource/testdata/dataquery.gen';

import { DashboardScene } from '../dashboard/DashboardScene';

import { getQueryRunnerWithRandomWalkQuery } from './queries';

export function getGridWithMultipleData(): DashboardScene {
  return new DashboardScene({
    title: '具有行和不同查询的网格',
    body: new SceneGridLayout({
      children: [
        new SceneGridRow({
          $timeRange: new SceneTimeRange(),
          $data: getQueryRunnerWithRandomWalkQuery({ scenarioId: TestDataQueryType.RandomWalkTable }),
          title: 'A行-有自己的查询',
          key: 'Row A',
          isCollapsed: true,
          y: 0,
          children: [
            new SceneGridItem({
              x: 0,
              y: 1,
              width: 12,
              height: 5,
              isResizable: true,
              isDraggable: true,
              body: new VizPanel({
                pluginId: 'timeseries',
                title: 'Row A Child1',
                key: 'Row A Child1',
              }),
            }),
            new SceneGridItem({
              x: 0,
              y: 5,
              width: 6,
              height: 5,
              isResizable: true,
              isDraggable: true,
              body: new VizPanel({
                pluginId: 'timeseries',
                title: 'Row A Child2',
                key: 'Row A Child2',
              }),
            }),
          ],
        }),
        new SceneGridRow({
          title: 'Row B - uses global query',
          key: 'Row B',
          isCollapsed: true,
          y: 1,
          children: [
            new SceneGridItem({
              x: 0,
              y: 2,
              width: 12,
              height: 5,
              isResizable: false,
              isDraggable: true,
              body: new VizPanel({
                pluginId: 'timeseries',
                title: 'Row B Child1',
                key: 'Row B Child1',
              }),
            }),
            new SceneGridItem({
              x: 0,
              y: 7,
              width: 6,
              height: 5,
              isResizable: false,
              isDraggable: true,
              body: new VizPanel({
                $data: getQueryRunnerWithRandomWalkQuery({ seriesCount: 10 }),
                pluginId: 'timeseries',
                title: 'Row B Child2 with data',
                key: 'Row B Child2',
              }),
            }),
          ],
        }),
        new SceneGridItem({
          x: 0,
          y: 12,
          width: 6,
          height: 10,
          isResizable: true,
          isDraggable: true,
          body: new VizPanel({
            $data: getQueryRunnerWithRandomWalkQuery({ seriesCount: 10 }),
            pluginId: 'timeseries',
            title: 'Outsider, has its own query',
            key: 'Outsider-own-query',
          }),
        }),
        new SceneGridItem({
          x: 6,
          y: 12,
          width: 12,
          height: 10,
          isResizable: true,
          isDraggable: true,
          body: new VizPanel({
            pluginId: 'timeseries',
            title: '局外人，使用全局查询',
            key: 'Outsider-global-query',
          }),
        }),
      ],
    }),
    $timeRange: new SceneTimeRange(),
    $data: getQueryRunnerWithRandomWalkQuery(),
    actions: [new SceneTimePicker({}), new SceneRefreshPicker({})],
  });
}
