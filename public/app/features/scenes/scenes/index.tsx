import { DashboardScene } from '../dashboard/DashboardScene';

import { getGridWithMultipleTimeRanges } from './gridMultiTimeRange';
import { getMultipleGridLayoutTest } from './gridMultiple';
import { getGridWithMultipleData } from './gridWithMultipleData';
import { getQueryVariableDemo } from './queryVariableDemo';
import { getSceneWithRows } from './sceneWithRows';
import { getTransformationsDemo } from './transformations';
import { getVariablesDemo, getVariablesDemoWithAll } from './variablesDemo';

interface SceneDef {
  title: string;
  getScene: () => DashboardScene;
}
export function getScenes(): SceneDef[] {
  return [
    { title: '带行的场景', getScene: getSceneWithRows },
    { title: '具有行和不同查询的网格', getScene: getGridWithMultipleData },
    { title: '具有行、不同查询和时间范围的网格', getScene: getGridWithMultipleTimeRanges },
    { title: '多重网格布局测试', getScene: getMultipleGridLayoutTest },
    { title: '变量', getScene: getVariablesDemo },
    { title: '具有所有值的变量', getScene: getVariablesDemoWithAll },
    { title: '查询变量', getScene: getQueryVariableDemo },
    { title: '转换演示', getScene: getTransformationsDemo },
  ];
}

const cache: Record<string, DashboardScene> = {};

export function getSceneByTitle(title: string) {
  if (cache[title]) {
    return cache[title];
  }

  const scene = getScenes().find((x) => x.title === title);

  if (scene) {
    cache[title] = scene.getScene();
  }

  return cache[title];
}
