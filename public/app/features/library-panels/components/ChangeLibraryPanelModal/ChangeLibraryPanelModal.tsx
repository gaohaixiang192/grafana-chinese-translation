import React from 'react';

import { ConfirmModal } from '@grafana/ui';

import { PanelModel } from '../../../dashboard/state';
import { isPanelModelLibraryPanel } from '../../guard';

export interface ChangeLibraryPanelModalProps {
  panel: PanelModel;
  onConfirm: () => void;
  onDismiss: () => void;
}

export const ChangeLibraryPanelModal = ({ onConfirm, onDismiss, panel }: ChangeLibraryPanelModalProps): JSX.Element => {
  const isLibraryPanel = isPanelModelLibraryPanel(panel);
  const title = `${isLibraryPanel ? '变化' : '替换为'} “库”面板`;
  const body = `${
    isLibraryPanel ? '变化' : '替换为'
  } 库面板将删除自上次保存以来的所有更改.`;
  return (
    <ConfirmModal
      onConfirm={onConfirm}
      onDismiss={onDismiss}
      confirmText={isLibraryPanel ? '变化' : '替换'}
      title={title}
      body={body}
      dismissText="Cancel"
      isOpen={true}
    />
  );
};
