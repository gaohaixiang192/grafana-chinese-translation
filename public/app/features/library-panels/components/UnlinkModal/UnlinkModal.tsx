import React from 'react';

import { ConfirmModal } from '@grafana/ui';

interface Props {
  isOpen: boolean;
  onConfirm: () => void;
  onDismiss: () => void;
}

export const UnlinkModal = ({ isOpen, onConfirm, onDismiss }: Props) => {
  return (
    <ConfirmModal
      title="是否确实要取消此面板的链接?"
      icon="question-circle"
      body="如果取消链接此面板，则可以在不影响任何其他面板的情况下对其进行编辑.
            然而，一旦您进行了更改，您将无法恢复到其原始的可重复使用面板."
      confirmText="是，取消链接"
      onConfirm={() => {
        onConfirm();
        onDismiss();
      }}
      onDismiss={onDismiss}
      isOpen={isOpen}
    />
  );
};
