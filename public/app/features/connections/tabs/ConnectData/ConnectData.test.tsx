import { fireEvent, render, RenderResult, screen } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';

import { PluginType } from '@grafana/data';
import { contextSrv } from 'app/core/core';
import { getCatalogPluginMock, getPluginsStateMock } from 'app/features/plugins/admin/__mocks__';
import { CatalogPlugin } from 'app/features/plugins/admin/types';
import { configureStore } from 'app/store/configureStore';
import { AccessControlAction } from 'app/types';

import { AddNewConnection } from './ConnectData';

jest.mock('app/features/datasources/api');

const renderPage = (plugins: CatalogPlugin[] = []): RenderResult => {
  // @ts-ignore
  const store = configureStore({ plugins: getPluginsStateMock(plugins) });

  return render(
    <Provider store={store}>
      <AddNewConnection />
    </Provider>
  );
};

const mockCatalogDataSourcePlugin = getCatalogPluginMock({
  type: PluginType.datasource,
  name: 'Sample data source',
  id: 'sample-data-source',
});

const originalHasPermission = contextSrv.hasPermission;

describe('添加新连接', () => {
  beforeEach(() => {
    contextSrv.hasPermission = originalHasPermission;
  });

  test('如果插件列表为空，则不显示任何结果', async () => {
    renderPage();

    expect(screen.queryByText('找不到与您的查询匹配的结果.')).toBeInTheDocument();
  });

  test('如果列表中没有数据源插件，则不显示任何结果', async () => {
    renderPage([getCatalogPluginMock()]);

    expect(screen.queryByText('找不到与您的查询匹配的结果.')).toBeInTheDocument();
  });

  test('填充列表时仅呈现数据源插件', async () => {
    renderPage([getCatalogPluginMock(), mockCatalogDataSourcePlugin]);

    expect(await screen.findByText('Sample data source')).toBeVisible();
  });

  test('renders card if search term matches', async () => {
    renderPage([getCatalogPluginMock(), mockCatalogDataSourcePlugin]);
    const searchField = await screen.findByRole('textbox');

    fireEvent.change(searchField, { target: { value: 'ampl' } });
    expect(await screen.findByText('Sample data source')).toBeVisible();

    fireEvent.change(searchField, { target: { value: 'cramp' } });
    expect(screen.queryByText('No results matching your query were found.')).toBeInTheDocument();
  });

  test('shows a "No access" modal if the user does not have permissions to create datasources', async () => {
    (contextSrv.hasPermission as jest.Mock) = jest.fn().mockImplementation((permission: string) => {
      if (permission === AccessControlAction.DataSourcesCreate) {
        return false;
      }

      return true;
    });

    renderPage([getCatalogPluginMock(), mockCatalogDataSourcePlugin]);
    const exampleSentenceInModal = 'Editors cannot add new connections.';

    // Should not show the modal by default
    expect(screen.queryByText(new RegExp(exampleSentenceInModal))).not.toBeInTheDocument();

    // Should show the modal if the user has no permissions
    fireEvent.click(await screen.findByText('Sample data source'));
    expect(screen.queryByText(new RegExp(exampleSentenceInModal))).toBeInTheDocument();
  });

  test('does not show a "No access" modal but displays the details page if the user has the right permissions', async () => {
    (contextSrv.hasPermission as jest.Mock) = jest.fn().mockReturnValue(true);

    renderPage([getCatalogPluginMock(), mockCatalogDataSourcePlugin]);
    const exampleSentenceInModal = 'Editors cannot add new connections.';

    // Should not show the modal by default
    expect(screen.queryByText(new RegExp(exampleSentenceInModal))).not.toBeInTheDocument();

    // Should not show the modal when clicking a card
    fireEvent.click(await screen.findByText('Sample data source'));
    expect(screen.queryByText(new RegExp(exampleSentenceInModal))).not.toBeInTheDocument();
  });
});
