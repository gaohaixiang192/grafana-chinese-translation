﻿import { reducerTester } from '../../../../test/core/redux/reducerTester';
import { ApiKeysState } from '../../../types';
import { getMultipleMockKeys } from '../__mocks__/apiKeysMock';

import {
  apiKeysLoaded,
  apiKeysReducer,
  includeExpiredToggled,
  initialApiKeysState,
  isFetching,
  setSearchQuery,
} from './reducers';

describe('API键减速机', () => {
  it('应设置关键点', () => {
    reducerTester<ApiKeysState>()
      .givenReducer(apiKeysReducer, { ...initialApiKeysState })
      .whenActionIsDispatched(
        apiKeysLoaded({ keys: getMultipleMockKeys(4), keysIncludingExpired: getMultipleMockKeys(6) })
      )
      .thenStateShouldEqual({
        ...initialApiKeysState,
        keys: getMultipleMockKeys(4),
        keysIncludingExpired: getMultipleMockKeys(6),
        hasFetched: true,
      });
  });

  it('应设置搜索查询', () => {
    reducerTester<ApiKeysState>()
      .givenReducer(apiKeysReducer, { ...initialApiKeysState })
      .whenActionIsDispatched(setSearchQuery('test query'))
      .thenStateShouldEqual({
        ...initialApiKeysState,
        searchQuery: 'test query',
      });
  });

  it('应切换includeExpired状态', () => {
    reducerTester<ApiKeysState>()
      .givenReducer(apiKeysReducer, { ...initialApiKeysState })
      .whenActionIsDispatched(includeExpiredToggled())
      .thenStateShouldEqual({
        ...initialApiKeysState,
        includeExpired: true,
      });
  });

  it('应在获取时设置状态', () => {
    reducerTester<ApiKeysState>()
      .givenReducer(apiKeysReducer, { ...initialApiKeysState })
      .whenActionIsDispatched(isFetching())
      .thenStateShouldEqual({
        ...initialApiKeysState,
        hasFetched: false,
      });
  });
});
