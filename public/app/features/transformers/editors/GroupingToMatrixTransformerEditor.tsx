import React, { useCallback } from 'react';

import {
  DataTransformerID,
  SelectableValue,
  standardTransformers,
  TransformerRegistryItem,
  TransformerUIProps,
  GroupingToMatrixTransformerOptions,
  SpecialValue,
} from '@grafana/data';
import { InlineField, InlineFieldRow, Select } from '@grafana/ui';

import { useAllFieldNamesFromDataFrames } from '../utils';

export const GroupingToMatrixTransformerEditor = ({
  input,
  options,
  onChange,
}: TransformerUIProps<GroupingToMatrixTransformerOptions>) => {
  const fieldNames = useAllFieldNamesFromDataFrames(input).map((item: string) => ({ label: item, value: item }));

  const onSelectColumn = useCallback(
    (value: SelectableValue<string>) => {
      onChange({
        ...options,
        columnField: value?.value,
      });
    },
    [onChange, options]
  );

  const onSelectRow = useCallback(
    (value: SelectableValue<string>) => {
      onChange({
        ...options,
        rowField: value?.value,
      });
    },
    [onChange, options]
  );

  const onSelectValue = useCallback(
    (value: SelectableValue<string>) => {
      onChange({
        ...options,
        valueField: value?.value,
      });
    },
    [onChange, options]
  );

  const specialValueOptions: Array<SelectableValue<SpecialValue>> = [
    { label: 'Null', value: SpecialValue.Null, description: '空值' },
    { label: 'True', value: SpecialValue.True, description: '布尔真值' },
    { label: 'False', value: SpecialValue.False, description: '布尔错误值' },
    { label: 'Empty', value: SpecialValue.Empty, description: '空字符串' },
  ];

  const onSelectEmptyValue = useCallback(
    (value: SelectableValue<SpecialValue>) => {
      onChange({
        ...options,
        emptyValue: value?.value,
      });
    },
    [onChange, options]
  );

  return (
    <>
      <InlineFieldRow>
        <InlineField label="Column" labelWidth={8}>
          <Select options={fieldNames} value={options.columnField} onChange={onSelectColumn} isClearable />
        </InlineField>
        <InlineField label="Row" labelWidth={8}>
          <Select options={fieldNames} value={options.rowField} onChange={onSelectRow} isClearable />
        </InlineField>
        <InlineField label="Cell Value" labelWidth={10}>
          <Select options={fieldNames} value={options.valueField} onChange={onSelectValue} isClearable />
        </InlineField>
        <InlineField label="Empty Value">
          <Select options={specialValueOptions} value={options.emptyValue} onChange={onSelectEmptyValue} isClearable />
        </InlineField>
      </InlineFieldRow>
    </>
  );
};

export const groupingToMatrixTransformRegistryItem: TransformerRegistryItem<GroupingToMatrixTransformerOptions> = {
  id: DataTransformerID.groupingToMatrix,
  editor: GroupingToMatrixTransformerEditor,
  transformation: standardTransformers.groupingToMatrixTransformer,
  name: '分组到矩阵',
  description: `采用三个字段的组合并生成矩阵`,
};
