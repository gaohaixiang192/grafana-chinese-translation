import React from 'react';

import { DataTransformerID, standardTransformers, TransformerRegistryItem, TransformerUIProps } from '@grafana/data';
import { MergeTransformerOptions } from '@grafana/data/src/transformations/transformers/merge';
import { FieldValidationMessage } from '@grafana/ui';

export const MergeTransformerEditor = ({ input, options, onChange }: TransformerUIProps<MergeTransformerOptions>) => {
  if (input.length <= 1) {
    // Show warning that merge is useless only apply on a single frame
    return <FieldValidationMessage>Merge has no effect when applied on a single frame.</FieldValidationMessage>;
  }
  return null;
};

export const mergeTransformerRegistryItem: TransformerRegistryItem<MergeTransformerOptions> = {
  id: DataTransformerID.merge,
  editor: MergeTransformerEditor,
  transformation: standardTransformers.mergeTransformer,
  name: '合并',
  description: `合并多个系列/表并返回一个表，其中可合并的值将合并到同一行.
                用于显示多个系列、表格或两者在表格中的组合.`,
};
