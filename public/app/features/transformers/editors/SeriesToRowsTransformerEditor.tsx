import { DataTransformerID, standardTransformers, TransformerRegistryItem, TransformerUIProps } from '@grafana/data';
import { SeriesToRowsTransformerOptions } from '@grafana/data/src/transformations/transformers/seriesToRows';

export const SeriesToRowsTransformerEditor = ({
  input,
  options,
  onChange,
}: TransformerUIProps<SeriesToRowsTransformerOptions>) => {
  return null;
};

export const seriesToRowsTransformerRegistryItem: TransformerRegistryItem<SeriesToRowsTransformerOptions> = {
  id: DataTransformerID.seriesToRows,
  editor: SeriesToRowsTransformerEditor,
  transformation: standardTransformers.seriesToRowsTransformer,
  name: '系列到行',
  description: `合并多个序列并返回一个以时间、度量和值为列的序列.
                用于显示表格中可视化的多个时间序列.`,
};
