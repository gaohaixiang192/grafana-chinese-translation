import React, { useCallback } from 'react';

import {
  DataTransformerID,
  ReducerID,
  SelectableValue,
  standardTransformers,
  TransformerRegistryItem,
  TransformerUIProps,
} from '@grafana/data';
import { ReduceTransformerMode, ReduceTransformerOptions } from '@grafana/data/src/transformations/transformers/reduce';
import { selectors } from '@grafana/e2e-selectors';
import { LegacyForms, Select, StatsPicker } from '@grafana/ui';

// TODO:  Minimal implementation, needs some <3
export const ReduceTransformerEditor = ({ options, onChange }: TransformerUIProps<ReduceTransformerOptions>) => {
  const modes: Array<SelectableValue<ReduceTransformerMode>> = [
    {
      label: '系列到行',
      value: ReduceTransformerMode.SeriesToRows,
      description: '为每个系列值创建一个包含一行的表',
    },
    {
      label: '减少字段',
      value: ReduceTransformerMode.ReduceFields,
      description: '将每个字段折叠为一个值',
    },
  ];

  const onSelectMode = useCallback(
    (value: SelectableValue<ReduceTransformerMode>) => {
      const mode = value.value!;
      onChange({
        ...options,
        mode,
        includeTimeField: mode === ReduceTransformerMode.ReduceFields ? !!options.includeTimeField : false,
      });
    },
    [onChange, options]
  );

  const onToggleTime = useCallback(() => {
    onChange({
      ...options,
      includeTimeField: !options.includeTimeField,
    });
  }, [onChange, options]);

  const onToggleLabels = useCallback(() => {
    onChange({
      ...options,
      labelsToFields: !options.labelsToFields,
    });
  }, [onChange, options]);

  return (
    <>
      <div>
        <div className="gf-form gf-form--grow">
          <div className="gf-form-label width-8" aria-label={selectors.components.Transforms.Reduce.modeLabel}>
            Mode
          </div>
          <Select
            options={modes}
            value={modes.find((v) => v.value === options.mode) || modes[0]}
            onChange={onSelectMode}
            className="flex-grow-1"
          />
        </div>
      </div>
      <div className="gf-form-inline">
        <div className="gf-form gf-form--grow">
          <div className="gf-form-label width-8" aria-label={selectors.components.Transforms.Reduce.calculationsLabel}>
            Calculations
          </div>
          <StatsPicker
            className="flex-grow-1"
            placeholder="Choose Stat"
            allowMultiple
            stats={options.reducers || []}
            onChange={(stats) => {
              onChange({
                ...options,
                reducers: stats as ReducerID[],
              });
            }}
          />
        </div>
      </div>
      {options.mode === ReduceTransformerMode.ReduceFields && (
        <div className="gf-form-inline">
          <div className="gf-form">
            <LegacyForms.Switch
              label="Include time"
              labelClass="width-8"
              checked={!!options.includeTimeField}
              onChange={onToggleTime}
            />
          </div>
        </div>
      )}
      {options.mode !== ReduceTransformerMode.ReduceFields && (
        <div className="gf-form-inline">
          <div className="gf-form">
            <LegacyForms.Switch
              label="Labels to fields"
              labelClass="width-8"
              checked={!!options.labelsToFields}
              onChange={onToggleLabels}
            />
          </div>
        </div>
      )}
    </>
  );
};

export const reduceTransformRegistryItem: TransformerRegistryItem<ReduceTransformerOptions> = {
  id: DataTransformerID.reduce,
  editor: ReduceTransformerEditor,
  transformation: standardTransformers.reduceTransformer,
  name: standardTransformers.reduceTransformer.name,
  description: standardTransformers.reduceTransformer.description,
};
