import React, { useCallback, useState } from 'react';

import { ValueMatcherID, BasicValueMatcherOptions } from '@grafana/data';
import { Input } from '@grafana/ui';

import { ValueMatcherEditorConfig, ValueMatcherUIProps, ValueMatcherUIRegistryItem } from './types';
import { convertToType } from './utils';

export function basicMatcherEditor<T = any>(
  config: ValueMatcherEditorConfig
): React.FC<ValueMatcherUIProps<BasicValueMatcherOptions<T>>> {
  return function Render({ options, onChange, field }) {
    const { validator, converter = convertToType } = config;
    const { value } = options;
    const [isInvalid, setInvalid] = useState(!validator(value));

    const onChangeValue = useCallback(
      (event: React.FormEvent<HTMLInputElement>) => {
        setInvalid(!validator(event.currentTarget.value));
      },
      [setInvalid, validator]
    );

    const onChangeOptions = useCallback(
      (event: React.FocusEvent<HTMLInputElement>) => {
        if (isInvalid) {
          return;
        }

        const { value } = event.currentTarget;

        onChange({
          ...options,
          value: converter(value, field),
        });
      },
      [options, onChange, isInvalid, field, converter]
    );

    return (
      <Input
        className="flex-grow-1"
        invalid={isInvalid}
        defaultValue={String(options.value)}
        placeholder="Value"
        onChange={onChangeValue}
        onBlur={onChangeOptions}
      />
    );
  };
}

export const getBasicValueMatchersUI = (): Array<ValueMatcherUIRegistryItem<BasicValueMatcherOptions>> => {
  return [
    {
      name: '更大',
      id: ValueMatcherID.greater,
      component: basicMatcherEditor<number>({
        validator: (value) => !isNaN(value),
      }),
    },
    {
      name: '大于或等于',
      id: ValueMatcherID.greaterOrEqual,
      component: basicMatcherEditor<number>({
        validator: (value) => !isNaN(value),
      }),
    },
    {
      name: '更低',
      id: ValueMatcherID.lower,
      component: basicMatcherEditor<number>({
        validator: (value) => !isNaN(value),
      }),
    },
    {
      name: '低于或等于',
      id: ValueMatcherID.lowerOrEqual,
      component: basicMatcherEditor<number>({
        validator: (value) => !isNaN(value),
      }),
    },
    {
      name: '等于',
      id: ValueMatcherID.equal,
      component: basicMatcherEditor<any>({
        validator: () => true,
      }),
    },
    {
      name: '不相等',
      id: ValueMatcherID.notEqual,
      component: basicMatcherEditor<any>({
        validator: () => true,
      }),
    },
    {
      name: '正则',
      id: ValueMatcherID.regex,
      component: basicMatcherEditor<string>({
        validator: () => true,
        converter: (value) => String(value),
      }),
    },
  ];
};
