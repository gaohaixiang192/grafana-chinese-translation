export function assertInstanceOf<T extends { new (...args: unknown[]): InstanceType<T> }>(
  value: unknown,
  type: T
): InstanceType<T> {
  if (!(value instanceof type)) {
    throw new Error(`期望值为的instanceof ${typeof type} but got ${typeof value}`);
  }

  return value;
}

export function assertIsDefined<T>(value: T | null | undefined): T {
  if (value == null) {
    throw new Error(`期望值不能为null，但得到 ${typeof value}`);
  }

  return value;
}
