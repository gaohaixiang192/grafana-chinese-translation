package navtreeimpl

import (
	ac "github.com/grafana/grafana/pkg/services/accesscontrol"
	contextmodel "github.com/grafana/grafana/pkg/services/contexthandler/model"
	"github.com/grafana/grafana/pkg/services/correlations"
	"github.com/grafana/grafana/pkg/services/datasources"
	"github.com/grafana/grafana/pkg/services/featuremgmt"
	"github.com/grafana/grafana/pkg/services/navtree"
	"github.com/grafana/grafana/pkg/services/pluginsintegration/pluginaccesscontrol"
	"github.com/grafana/grafana/pkg/services/serviceaccounts"
)

func (s *ServiceImpl) getAdminNode(c *contextmodel.ReqContext) (*navtree.NavLink, error) {
	var configNodes []*navtree.NavLink
	hasAccess := ac.HasAccess(s.accessControl, c)
	hasGlobalAccess := ac.HasGlobalAccess(s.accessControl, s.accesscontrolService, c)
	orgsAccessEvaluator := ac.EvalPermission(ac.ActionOrgsRead)
	authConfigUIAvailable := s.license.FeatureEnabled("saml")

	if hasAccess(datasources.ConfigurationPageAccess) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text:     "数据源",
			Icon:     "database",
			SubTitle: "添加和配置数据源",
			Id:       "datasources",
			Url:      s.cfg.AppSubURL + "/datasources",
		})
	}

	// FIXME: while we don't have a permissions for listing plugins the legacy check has to stay as a default
	if pluginaccesscontrol.ReqCanAdminPlugins(s.cfg)(c) || hasAccess(pluginaccesscontrol.AdminAccessEvaluator) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text:     "插件",
			Id:       "plugins",
			SubTitle: "使用插件扩展Grafana体验",
			Icon:     "plug",
			Url:      s.cfg.AppSubURL + "/plugins",
		})
	}

	if hasAccess(ac.EvalAny(ac.EvalPermission(ac.ActionOrgUsersRead), ac.EvalPermission(ac.ActionUsersRead, ac.ScopeGlobalUsersAll))) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text: "用户", SubTitle: "管理Grafana中的用户", Id: "global-users", Url: s.cfg.AppSubURL + "/admin/users", Icon: "user",
		})
	}

	if hasAccess(ac.TeamsAccessEvaluator) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text:     "团队",
			Id:       "teams",
			SubTitle: "具有通用仪表板和权限需求的用户组",
			Icon:     "users-alt",
			Url:      s.cfg.AppSubURL + "/org/teams",
		})
	}

	if enableServiceAccount(s, c) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text:     "服务帐户",
			Id:       "serviceaccounts",
			SubTitle: "使用服务帐户在Grafana中运行自动化工作负载",
			Icon:     "gf-service-account",
			Url:      s.cfg.AppSubURL + "/org/serviceaccounts",
		})
	}

	disabled, err := s.apiKeyService.IsDisabled(c.Req.Context(), c.OrgID)
	if err != nil {
		return nil, err
	}
	if hasAccess(ac.ApiKeyAccessEvaluator) && !disabled {
		configNodes = append(configNodes, &navtree.NavLink{
			Text:     "API密钥",
			Id:       "apikeys",
			SubTitle: "管理和创建用于与Grafana HTTP API交互的API密钥",
			Icon:     "key-skeleton-alt",
			Url:      s.cfg.AppSubURL + "/org/apikeys",
		})
	}

	if hasAccess(ac.OrgPreferencesAccessEvaluator) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text:     "默认首选项",
			Id:       "org-settings",
			SubTitle: "管理整个组织的首选项",
			Icon:     "sliders-v-alt",
			Url:      s.cfg.AppSubURL + "/org",
		})
	}

	if authConfigUIAvailable && hasAccess(evalAuthenticationSettings()) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text:     "身份验证",
			Id:       "authentication",
			SubTitle: "管理您的身份验证设置并配置单一登录",
			Icon:     "signin",
			Url:      s.cfg.AppSubURL + "/admin/authentication",
		})
	}

	if hasAccess(ac.EvalPermission(ac.ActionSettingsRead, ac.ScopeSettingsAll)) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text: "设置", SubTitle: "查看Grafana配置中定义的设置", Id: "server-settings", Url: s.cfg.AppSubURL + "/admin/settings", Icon: "sliders-v-alt",
		})
	}

	if hasGlobalAccess(orgsAccessEvaluator) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text: "组织机构", SubTitle: "Grafana在同一服务器上运行的独立实例", Id: "global-orgs", Url: s.cfg.AppSubURL + "/admin/orgs", Icon: "building",
		})
	}

	if s.features.IsEnabled(featuremgmt.FlagCorrelations) && hasAccess(correlations.ConfigurationPageAccess) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text:     "相关性",
			Icon:     "gf-glue",
			SubTitle: "添加和配置相关性",
			Id:       "correlations",
			Url:      s.cfg.AppSubURL + "/datasources/correlations",
		})
	}

	if s.cfg.LDAPAuthEnabled && hasAccess(ac.EvalPermission(ac.ActionLDAPStatusRead)) {
		configNodes = append(configNodes, &navtree.NavLink{
			Text: "LDAP", Id: "ldap", Url: s.cfg.AppSubURL + "/admin/ldap", Icon: "book",
		})
	}

	if hasAccess(ac.EvalPermission(ac.ActionSettingsRead, ac.ScopeSettingsAll)) && s.features.IsEnabled(featuremgmt.FlagStorage) {
		storage := &navtree.NavLink{
			Text:     "存储",
			Id:       "storage",
			SubTitle: "管理文件存储",
			Icon:     "cube",
			Url:      s.cfg.AppSubURL + "/admin/storage",
		}
		configNodes = append(configNodes, storage)
	}

	configNode := &navtree.NavLink{
		Id:         navtree.NavIDCfg,
		Text:       "Administration",
		SubTitle:   "Organization: " + c.OrgName,
		Icon:       "cog",
		SortWeight: navtree.WeightConfig,
		Children:   configNodes,
		Url:        "/admin",
	}

	return configNode, nil
}

func enableServiceAccount(s *ServiceImpl, c *contextmodel.ReqContext) bool {
	hasAccess := ac.HasAccess(s.accessControl, c)
	return hasAccess(serviceaccounts.AccessEvaluator)
}

func evalAuthenticationSettings() ac.Evaluator {
	return ac.EvalAll(
		ac.EvalPermission(ac.ActionSettingsWrite, ac.ScopeSettingsSAML),
		ac.EvalPermission(ac.ActionSettingsRead, ac.ScopeSettingsSAML),
	)
}
